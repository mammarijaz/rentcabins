// usage: log('inside coolFunc', this, arguments);
// paulirish.com/2009/log-a-lightweight-wrapper-for-consolelog/
window.log = function f(){ log.history = log.history || []; log.history.push(arguments); if(this.console) { var args = arguments, newarr; args.callee = args.callee.caller; newarr = [].slice.call(args); if (typeof console.log === 'object') log.apply.call(console.log, console, newarr); else console.log.apply(console, newarr);}};

// make it safe to use console.log always
(function(a){function b(){}for(var c="assert,count,debug,dir,dirxml,error,exception,group,groupCollapsed,groupEnd,info,log,markTimeline,profile,profileEnd,time,timeEnd,trace,warn".split(","),d;!!(d=c.pop());){a[d]=a[d]||b;}})
(function(){try{console.log();return window.console;}catch(a){return (window.console={});}}());


// place any jQuery/helper plugins in here, instead of separate, slower script files.

jQuery(document).ready(function($){
/**
  * Select2 - jQuery form plugin - http://ivaynberg.github.com/select2/
  */
	$("#destination").select2({
		placeholder: "select one or more towns",
		allowClear: true
	});
	$("#region").select2({
		placeholder: "or select one or more regions",
		allowClear: true
	});
	$("#setting").select2({
		placeholder: "select one or more settings",
		allowClear: true
	});
	$("#style").select2({
		placeholder: "select one or more styles",
		allowClear: true
	});
	$("#bedrooms").select2({
		placeholder: "bedrooms",
		allowClear: true
	});
	$("#sleeps").select2({
		placeholder: "sleeps",
		allowClear: true
	});
	$("select#amenities").select2({
		placeholder: "Select pet preference & other needs",
		allowClear: true
	});
	$("#state").select2({
		placeholder: "state *",
		allowClear: true
	});

/**
  * jQuery UI
  */
	// Datepicker
	$('#arrival_date').datepicker({});
	$('#departure_date').datepicker({});
	$('#arrival_date_2').datepicker({});
	$('#departure_date_2').datepicker({});

	$('.datepicker-icon').on('click', '.btn', function(e) {
	  $(e.delegateTarget).find('.datepicker').focus();
	});

	if($('#arrival_date').length || $('#arrival_date_2').length){
		$( "#arrival_date, #arrival_date_2" ).datepicker({
			defaultDate: "+0d",
			minDate:+0,
			changeMonth: true,
			onSelect: function() {
				var date = $(this).datepicker('getDate');
				if (date){
					date.setDate(date.getDate() + 1);
					$( "#departure_date" ).datepicker( "option", "minDate", date );
				}
			}
		});
	}
	if($('#departure_date').length || $('#departure_date_2').length){
		$( "#departure_date, #departure_date_2" ).datepicker({
			defaultDate: "+0d",
			minDate:+1,
			changeMonth: true
		});
	}

	$( ".calendar-display" ).datepicker({
			numberOfMonths: 3,
			showButtonPanel: true
		});

	// Slider
	$("#slider").slider({
		range: true,
		values: [17, 67]
	});

	$("#daily-rate").slider({
		range: true,
		min: 0,
		max: 450,
		values: [0, 450],
		step: 50,
		slide: function( event, ui ) {
			$( "#amount-daily" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] + ((ui.values[ 1 ] === 450)?'+':'') );
		}
	});
	$("#amount-daily").val( "$" + $( "#daily-rate" ).slider( "values", 0 ) +
		" - $" + $( "#daily-rate" ).slider( "values", 1 ) + '+' );

	$("#weekly-rate").slider({
		range: true,
		min: 0,
		max: 3000,
		values: [0, 3000],
		step: 250,
		slide: function( event, ui ) {
			$("#amount-weekly").val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] + ((ui.values[ 1 ] === 3000)?'+':'') );
		}
	});
	$("#amount-weekly").val( "$" + $( "#weekly-rate" ).slider( "values", 0 ) +
		" - $" + $( "#weekly-rate" ).slider( "values", 1 ) + '+' );

	//hover states on the static widgets
	$('#dialog_link, ul#icons li').hover(
		function() { $(this).addClass('ui-state-hover'); },
		function() { $(this).removeClass('ui-state-hover'); }
	);

/**
  * jQuery Placeholder plugin - https://github.com/mathiasbynens/jquery-placeholder
  */
	$('input, textarea').placeholder();

/**
  * jQuery Validity plugin - http://validity.thatscaptaintoyou.com/
  */
	$.validity.setup({ outputMode:"label" });

	$("#listrentalform").validity(function() {
		$("#first_name")
			.require();
		$("#last_name")
			.require();
		$("#email")
			.require()
			.match('email');
	});

	$("#subscribeform").validity(function() {
		$("#first_name_subscribe")
			.require();
		$("#last_name_subscribe")
			.require();
		$("#email_subscribe")
			.require()
			.match('email');
	});

	$("#inquiryform").validity(function() {
		$("#first_name")
			.require();
		$("#email")
			.require()
			.match('email');
		$("#state")
			.require();
	});

	window.validateAjaxInquiryForm = function() {

		$.validity.start();

		$("#first_name")
			.require();
		$("#email")
			.require()
			.match('email');
		$("#state")
			.require();

		var result = $.validity.end();

		return result.valid;
	};

});

jQuery(document).ready(function($){
  /**
  * ShareThis buttons - http://sharethis.com/
  */
	stLight.options({
		publisher: "1f74148d-87f2-4950-8ee6-113ebd3059fc",
		tracking:'google',
	});

/**
  * Closing of map - property details container
  */
	$('.close').click(function() {
		$('.property-detail').hide('slow');
	});

/**
  * Fade in back to top button - http://agyuku.net/2009/05/back-to-top-link-using-jquery/
  */
	$(function() {
		$(window).scroll(function() {
			if ( $(this).scrollTop() >= 700) {
				$('#toTop').fadeIn();
			} else {
				$('#toTop').fadeOut();
			}
		});

		$('#toTop').click(function() {
			$('body,html').animate({scrollTop:0},800);
		});
	});

	if($('#inquiryform').length){
		$('#inquiryform').submit(function(e){
			$('#btn-inquiry').prop('disabled', true);

			if ($('#is_search').length) {
				let userValue = [];
				$.each($('input[name="select_rental[]"]:checked'), function(){
					userValue.push($(this).val());
				});
				$('#selected_properties').val(userValue);
			}
		});
	}

/**
  * Random div (Featured Property)
  */
	// var myRandom = Math.floor(Math.random() * $("#featured-property .box").length);
	// $("#featured-property .box:eq(" + myRandom + ")").show();

	if($('#logo').length){
		$('#logo').on('click',function(event){
			event.preventDefault();
			window.close();
		});
	}

/**
  * Tooltip for "select" box on property results containers
  */

   $('.btn-tooltip').tooltip();

/**
  * Video modal on Property Details page
  */

	$('.popup-video').magnificPopup({
		type: 'iframe',
		removalDelay: 160,
		preloader: false,
		fixedContentPos: false
	});

/**
  * Amenities modal on Property Details page
  */
	$('.popup-modal').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#username',
		modal: true
	});
	$(document).on('click', '.popup-modal-dismiss', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});

	//Homepage
    let flexslider = $('.flexslider');
  	if(flexslider.length){
  		flexslider.flexslider({
				animation: "slide",
				slideshow: false,
				pauseOnAction: false,
				video: false,
				smoothHeight: true
			});
  	}
  	alert('got here');
  	if($('#page-search').length){
  		$.ajax({
			type: "GET",
			url: "/api/all-properties",
			data: {
				csrf: $('[name="csrf_token"]').attr('content')
			},
			dataType: 'json',
			success: function(data) {
				alert('success');
				console.log(data);
				$.each(data,function(i, rlocation){
					if(rlocation.address_latitude != null) {
						mapPD[rlocation.public_id] = rlocation;
					}
				});
				googleMapUpdate(data);
			},
			error: function(jqXhr, status) {
				alert('error');
				console.log(jqXhr);
				console.log(status);
			}
		});

		$('#searchform').submit(function(e){
			e.preventDefault();
			$('#btn-search').val('Searching...');
			getResults();
		});

		$('#show-listings').click(function(e){
			e.preventDefault();
			$('#btn-search').val('Searching...');
			getResults();
			$(document.body).scrollTop($('#list').offset());
		});

		//Moved event listeners
		$('#results').on('change', 'input[name="select_rental[]"]' ,function(){
			//if($('input[name="select_rental[]"]:checked').length) {$('#inquiry').fadeIn();} else {$('#inquiry').fadeOut();}
		});
  	} else {
  		alert('Page search is empty.');
  	}

  	function getResults(){
		$('.result-number').hide(0);
		$('#results').html('<h2 class="searching"><i class="fa fa-spinner fa-spin fa-lg"></i> Searching</h2>');
		googleMapInit();
	
	$.get('/api/properties', $('#searchform').serialize(), function(result) {
			$('#btn-search').val('Update Map');
			$('#results').html('');
			console.log(result);
			$('.result-number span').html(result.length);
			$('.result-number').show(0);
			for(var i=0;i<result.length;i++) {
				var template = $('#result-template').html();
				template = $(template).clone();
				if(result[i]['photos'] != null && result[i]['photos'][0] != '') {$('.result_photo',template).attr('src','/assets/admin_photos/'+result[i]['photos'][0].replace('.jpg','_thumb.jpg'));} else {$('.result_photo',template).attr('src','/assets/images/photo_placeholder.png');}
				if(result[i]['resort']!='') {$('.result_resort',template).html(result[i]['resort']);} else {$('.result_resort',template).remove();}
				if(result[i]['name']!='') {$('.result_name',template).html(result[i]['name']);} else {$('.result_name',template).remove();}
				if(result[i]['description']!='') {$('.result_description',template).html(result[i]['description']);} else {$('.result_description',template).remove();}
				if(result[i]['bedrooms']!='') {$('.result_bedrooms',template).html(result[i]['bedrooms']+' - bedrooms,');} else {$('.result_bedrooms',template).remove();}
				if(result[i]['bathrooms']!='') {$('.result_bathrooms',template).html(result[i]['bathrooms']+' - bathrooms,');} else {$('.result_bathrooms',template).remove();}
				if(result[i]['max_capacity']!='') {$('.result_max_capacity',template).html('sleeps - '+result[i]['max_capacity']);} else {$('.result_max_capacity',template).remove();}
				// if(result[i]['features'].indexOf('NO Pets Allowed') !== -1) {$('.result_no_pets_allowed',template).html('NO Pets Allowed');}
				// else if(result[i]['features'].indexOf('Pets Allowed') !== -1) {$('.result_no_pets_allowed',template).remove();}
				// else {$('.result_no_pets_allowed',template).remove();}
				if(result[i]['on_waters']!='') {$('.result_on_waters',template).html('On '+result[i]['on_waters']);} else {$('.result_on_waters',template).remove();}
				if(result[i]['near_locations']!='') {$('.result_near_locations',template).html('Near '+result[i]['near_locations']);} else {$('.result_near_locations',template).remove();}
				if(result[i]['city']!='') {$('.result_city',template).html(result[i]['city']);} else {$('.result_city',template).remove();}
				if(result[i]['max_rates_daily']!='' && result[i]['max_rates_daily']!='0') {
							$('.result_rates_daily',template).html('$'+result[i]['min_rates_daily']+' - $'+result[i]['max_rates_daily']+' nt<br>').show();
							$('.result_rates_daily_strike',template).html('<span style="text-decoration: line-through;">$'+(parseFloat(result[i]['min_rates_daily']) * 1.3).toFixed(0) +' - $'+(parseFloat(result[i]['max_rates_daily']) * 1.3).toFixed(0)+' nt</span>').show();
						}
						else {
							if(result[i]['max_rates_weekly']=='' && result[i]['max_rates_weekly']=='0') {
								$('.result_rates_daily',template).html("").hide();
								$('.result_rates_daily_strike',template).html("").hide();
							} else {
								$('.result_rates_daily',template).html("Inquire for daily rates").show();
								$('.result_rates_daily_strike',template).html("Not available").show();
							}
						}
						if(result[i]['max_rates_weekly']!='' && result[i]['max_rates_weekly']!='0') {
							$('.result_rates_weekly',template).html('$'+result[i]['min_rates_weekly']+' - $'+result[i]['max_rates_weekly']+' wk<br>').show();
							$('.result_rates_weekly_strike',template).html('<span style="text-decoration: line-through;">$'+(parseFloat(result[i]['min_rates_weekly']) * 1.3).toFixed(0) +' - $'+(parseFloat(result[i]['max_rates_weekly']) * 1.3).toFixed(0)+' wk</span>').show();
						}
						else {
							if(result[i]['max_rates_daily']=='' && result[i]['max_rates_daily']=='0') { 
								$('.result_rates_weekly',template).html("Inquire for details").show();
								$('.result_rates_weekly_strike',template).html("Not available").show();
							} else {
								$('.result_rates_weekly',template).html("Inquire for weekly rates").show();
								$('.result_rates_weekly_strike',template).html("Not available").show();
							}
						}
				$('input[name="select_rental[]"]',template).val(result[i]['pkid']);
				$('.result_details_link',template).attr('data-href','detail.php?id='+result[i]['pkid']);
				$('#results').append(template);
			}
			$('.btn-tooltip').tooltip();
			googleMapUpdate(result);
		});
	}
});

//# sourceMappingURL=all.js.map
