<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyRegionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('property_region', function (Blueprint $table) {
            //
            // $table->increments('id');
            $table->integer('property_id')->unsigned()->nullable();
            $table->integer('region_id')->unsigned()->nullable();

            $table->foreign('property_id')->references('id')->on('properties')->onDelete('cascade');
            $table->foreign('region_id')->references('id')->on('regions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('property_region');
    }
}
