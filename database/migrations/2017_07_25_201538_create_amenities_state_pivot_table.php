<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmenitiesStatePivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amenities_states', function($table){
            $table->increments('id');
            $table->integer('amenities_id')->unsigned();
            $table->integer('state_id')->unsigned();
            $table->foreign('state_id')->references('id')->on('states');
            $table->foreign('amenities_id')->references('id')->on('amenities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('amenities_states', function($table){
            $table->dropForeign('amenities_states_state_id_foreign');
            $table->dropForeign('amenities_states_amenities_id_foreign');
        });
        Schema::drop('amenities_states');
    }
}
