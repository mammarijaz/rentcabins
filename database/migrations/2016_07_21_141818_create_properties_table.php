<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('public_id');
            $table->string('status');
            $table->integer('user_id')->unsigned();
            $table->string('name');
            // $table->string('slug')->unique();
            // $table->text('description_brief')->nullable();
            $table->text('description');
            $table->integer('old_id')->unsigned()->nullable();
            $table->text('seo_description')->nullable();
            $table->text('owner_bios')->nullable();
            $table->string('email');
            $table->string('phone');
            $table->string('phone_cell')->nullable();
            $table->string('phone_cell_carrier')->nullable();
            $table->string('address');
            $table->string('city');
            $table->string('zip');
            $table->integer('state_id')->unsigned();
            $table->text('photos')->nullable();
            // $table->text('photo_descriptions')->nullable();
            $table->string('listing_url')->nullable();
            // $table->string('blocked_dates')->nullable();
            // $table->integer('region_id')->unsigned();
            // $table->integer('near_locations')->unsigned();
            $table->integer('location_id')->unsigned()->nullable();
            $table->string('on_waters')->nullable();
            // $table->text('settings')->nullable();
            // $table->text('styles')->nullable();
            $table->boolean('featured')->default(0);
            $table->boolean('allow_pets')->default(0);
            $table->boolean('allow_accessible')->default(0);
            $table->boolean('accepts_credit_cards')->default(0);
            $table->integer('min_rates_daily')->unsigned();
            $table->integer('min_rates_weekly')->unsigned();
            $table->integer('max_rates_daily')->unsigned();
            $table->integer('max_rates_weekly')->unsigned();
            $table->string('rates')->nullable();
            $table->integer('max_capacity');
            $table->integer('bedrooms');
            $table->integer('bathrooms');
            // $table->string('keywords')->nullable();
            $table->string('address_longitude');
            $table->string('address_latitude');
            $table->string('resort')->nullable();
            $table->string('unique_features')->nullable();
            $table->string('things_to_do')->nullable();
            $table->string('special_offers')->nullable();
            $table->text('features')->nullable();
            $table->text('testimonials')->nullable();
            $table->text('videos')->nullable();
            $table->string('weekly_only')->nullable();
            $table->text('social_media_links')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('state_id')->references('id')->on('states');
            $table->foreign('location_id')->references('id')->on('locations');
            // $table->foreign('region_id')->references('id')->on('regions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('properties');
    }
}
