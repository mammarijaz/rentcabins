<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('state_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('state_id')->unsigned()->unique();
            $table->string('from_email');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('google_analytics');
            $table->string('seo_title')->nullable();
            $table->string('seo_description')->nullable();
            $table->text('social_media')->nullable();
            $table->text('home_header')->nullable();
            $table->string('home_header_image')->nullable();
            $table->text('home_sidebar')->nullable();
            $table->text('home_content')->nullable();
            $table->text('home_bottom_content')->nullable();
            $table->text('footer_links')->nullable();
            $table->text('footer_sidebar_links')->nullable();
            // $table->timestamps();

            $table->foreign('state_id')->references('id')->on('states');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('state_settings');
    }
}
