<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatePropertySettingsPivot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('state_property_settings', function($table){
            $table->integer('property_settings_id')->unsigned();
            $table->integer('state_id')->unsigned();
            $table->foreign('state_id')->references('id')->on('states');
            $table->foreign('property_settings_id')->references('id')->on('property_settings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('state_property_settings', function($table){
            $table->dropForeign('state_property_settings_state_id_foreign');
            $table->dropForeign('state_property_settings_property_settings_id_foreign');
        });
        Schema::drop('state_property_settings');
    }
}
