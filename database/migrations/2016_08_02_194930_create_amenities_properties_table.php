<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmenitiesPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('amenity_property', function (Blueprint $table) {
            //
            // $table->increments('id');
            $table->integer('property_id')->unsigned()->nullable();
            $table->integer('amenity_id')->unsigned()->nullable();

            $table->foreign('property_id')->references('id')->on('properties');
            $table->foreign('amenity_id')->references('id')->on('amenities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('amenity_property');
    }
}
