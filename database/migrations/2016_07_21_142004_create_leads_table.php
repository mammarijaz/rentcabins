<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('state');
            // $table->integer('property_id')->unsigned();
            $table->string('phone')->nullable();
            $table->string('email');
            $table->string('email_optin')->default('no');
            $table->text('personalization');
            // $table->text('selected_results');
            $table->text('search_info');
            $table->timestamps();
            $table->softDeletes();

            // $table->foreign('property_id')->references('id')->on('properties');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('leads');
    }
}
