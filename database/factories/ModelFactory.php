<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\State::class, function( Faker\Generator $faker){
	return [
		'name' => $faker->state,
		'abbr' => $faker->stateAbbr,
		'url' => $faker->url,
	];
});

$factory->define(App\Models\Location::class, function( Faker\Generator $faker){
    $city = $faker->city;
    $regions = DB::table('regions')->get();
    $region = $faker->randomElement($regions);
    $state = DB::table('states')->where('id', $region->state_id)->get();
    return [
        'name' => $city,
        'slug' => str_slug($city.'-'.$state[0]->abbr),
        'region_id' => $region->id,
        'state_id' => $state[0]->id,
    ];
});
