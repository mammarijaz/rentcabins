<?php

use Illuminate\Database\Seeder;

use App\Models\Region;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Region::truncate();
        Region::create([
            'state_id' => 1,
            'name' => 'Northeast',
            'slug' => 'northeast',
        ]);
        Region::create([
            'state_id' => 1,
            'name' => 'Northwest',
            'slug' => 'northwest',
        ]);
        Region::create([
            'state_id' => 1,
            'name' => 'West Central',
            'slug' => 'west-central',
        ]);
        Region::create([
            'state_id' => 1,
            'name' => 'East Central',
            'slug' => 'east-central',
        ]);
        Region::create([
            'state_id' => 1,
            'name' => 'Southwest',
            'slug' => 'southwest',
        ]);
        Region::create([
            'state_id' => 1,
            'name' => 'Southeast',
            'slug' => 'southeast',
        ]);
        Region::create([
            'state_id' => 2,
            'name' => 'Northwest Kansas',
            'slug' => 'northwest',
        ]);
        Region::create([
            'state_id' => 2,
            'name' => 'West Central Kansas',
            'slug' => 'west-central',
        ]);
        Region::create([
            'state_id' => 2,
            'name' => 'East Central Kansas',
            'slug' => 'east-central',
        ]);
        Region::create([
            'state_id' => 2,
            'name' => 'Southwest Kansas',
            'slug' => 'southwest',
        ]);
        Region::create([
            'state_id' => 2,
            'name' => 'Southeast Kansas',
            'slug' => 'southeast',
        ]);
    }
}
