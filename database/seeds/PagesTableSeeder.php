<?php

use Illuminate\Database\Seeder;
use App\Models\Page;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Page::truncate();
        Page::create([
            'state_id' => 1,
            'title' => 'Advertising Opportunities',
            'slug' => 'advertising-opportunities',
            'content' => "<p class='p1' style='box-sizing: border-box; margin: 0px 0px 12px; padding: 0px; font-size: 16px; direction: ltr; font-family: Verdana; font-stretch: normal; color: #222222;'><strong style='box-sizing: border-box; line-height: inherit;'>BOOST IT WITH AN EMAIL ADVERTISING CAMPAIGN</strong><br style='box-sizing: border-box;' />6,500+ opt-in vacationers want to know about your rental special, or announcement.<br style='box-sizing: border-box;' />Why wait for area renters to find your special -- for just $250, we'll set up an email campaign and send it to OUR growing opt-in list of 6,500 area vacationers ...TWICE. This is OUR list, not a purchased list. These are vacationers who have given us their email and WANT to know about your rental specials or announcements.</p>
<p class='p1' style='box-sizing: border-box; margin: 0px 0px 12px; padding: 0px; font-size: 16px; direction: ltr; font-family: Verdana; font-stretch: normal; color: #222222;'><span class='s1' style='box-sizing: border-box; text-decoration: underline; color: #0000ff;'><a style='box-sizing: border-box; color: #2ba6cb; text-decoration: none; line-height: inherit;' href='https://www.robly.com/archive?id=7a283a1353192f9dc29bb16040d452b7'>CLICK HERE</a></span>&nbsp;TO SEE A SAMPLE of a cancellation that was filled in 30 minutes.</p>
<p class='p1' style='box-sizing: border-box; margin: 0px 0px 12px; padding: 0px; font-size: 16px; direction: ltr; font-family: Verdana; font-stretch: normal; color: #222222;'>Advertising to our opt-in list is a great compliment to any campaign or announcement!<br style='box-sizing: border-box;' />1.&nbsp;<a style='box-sizing: border-box; color: #2ba6cb; text-decoration: none; line-height: inherit;' href='mailto:updates@rentwisconsincabins.com'><span class='s2' style='box-sizing: border-box; text-decoration: underline; color: #0000ee;'>Email us.</span></a>&nbsp;Tell us what your campaign is about. YOUR campaign will be emailed&nbsp;<strong style='box-sizing: border-box; line-height: inherit;'>TWICE</strong>&nbsp;(2 days apart &ndash; the second eblast only goes to renters that did not open the first one). We'll design it for you! All you need to do is proof your campaign. Your campaign report shows all the activity.<br style='box-sizing: border-box;' />2. To get started, call 608-850-4242. Your email campaign is sent twice and is just $250</p>
<p class='p2' style='box-sizing: border-box; margin: 0px 0px 6px; padding: 0px; font-size: 16px; direction: ltr; font-family: Verdana; font-stretch: normal; color: #222222;'>&nbsp;</p>
<p class='p1' style='box-sizing: border-box; margin: 0px 0px 12px; padding: 0px; font-size: 16px; direction: ltr; font-family: Verdana; font-stretch: normal; color: #222222;'><strong style='box-sizing: border-box; line-height: inherit;'>FEATURE YOUR PROPERTY ON OUR WEBSITE</strong><br style='box-sizing: border-box;' />BOOST awareness and get hundreds of clicks to your business website by 'Featuring Your Property' on our website - we'll lay out the banner for you. Starts at $155 for 3 months; 6 months $295; or $555 for the entire year!<br style='box-sizing: border-box;' /><br style='box-sizing: border-box;' />'As always, I love your website for ease of making our yearly changes! You and your team have done a fantastic job. We wish to retain our place as a featured Property Manager for 2014.' The Cottage Keeper.<a style='box-sizing: border-box; color: #2ba6cb; text-decoration: none; line-height: inherit;' href='http://www.rentwisconsincabins.com/advertise.php'><span class='s2' style='box-sizing: border-box; text-decoration: underline; color: #0000ee;'>&nbsp;Learn how you can Feature Your Rentals.</span></a></p>
<p class='p2' style='box-sizing: border-box; margin: 0px 0px 6px; padding: 0px; font-size: 16px; direction: ltr; font-family: Verdana; font-stretch: normal; color: #222222;'>&nbsp;</p>
<p class='p1' style='box-sizing: border-box; margin: 0px 0px 12px; padding: 0px; font-size: 16px; direction: ltr; font-family: Verdana; font-stretch: normal; color: #222222;'><strong style='box-sizing: border-box; line-height: inherit;'>SEARCH ENGINE OPTIMIZATION (SEO) AND SOCIAL MEDIA COACH</strong><br style='box-sizing: border-box;' />You already know WE are professional online marketing nerds. But, did you know you can hire us to COACH YOU through the wild world of online marketing and search engine optimization? Success on the Internet includes advertising with us, but your business website and social media efforts need some LOVE too. Call us to talk about the current state of your online marketing efforts. We can work with you to improve the search-ranking of your business website and kick-start your social media. SEO Marketing Review &amp; Coaching Packages starts at $300. Call 608-850-4242, or&nbsp;<a style='box-sizing: border-box; color: #2ba6cb; text-decoration: none; line-height: inherit;' href='mailto:updates@rentwisconsincabins.com'><span class='s2' style='box-sizing: border-box; text-decoration: underline; color: #0000ee;'>e-mail us</span></a>&nbsp;if you are interested in learning more.</p>
<p class='p2' style='box-sizing: border-box; margin: 0px 0px 6px; padding: 0px; font-size: 16px; direction: ltr; font-family: Verdana; font-stretch: normal; color: #222222;'>&nbsp;</p>
<p class='p1' style='box-sizing: border-box; margin: 0px 0px 12px; padding: 0px; font-size: 16px; direction: ltr; font-family: Verdana; font-stretch: normal; color: #222222;'>Though check is preferred (to avoid fees); you can now use PayPal to pay fee's USING your credit card.</p>
<p class='p1' style='box-sizing: border-box; margin: 0px 0px 12px; padding: 0px; font-size: 16px; direction: ltr; font-family: Verdana; font-stretch: normal; color: #222222;'><strong style='box-sizing: border-box; line-height: inherit;'>1. Pay your annual fee.<br style='box-sizing: border-box;' />2. Pay any joint advertising fees.</strong></p>
<p class='p3' style='box-sizing: border-box; margin: 0px; padding: 0px; font-size: 16px; direction: ltr; font-family: Verdana; font-stretch: normal; color: #222222;'>Call us at 608-850-4242. We will send you a PayPal electronic invoice based on your advertising needs.</p>",
        ]);
        Page::create([
            'state_id' => 1,
            'title' => 'FAQ',
            'slug' => 'faq',
            'content' => "Frequently Asked Questions
<br>
QUESTION:
How come pictures on my computer are not uploading in my Property Manager?
ANSWER:
Chances are the picture file is not a .jpg and/or not under 10MB. Before uploading pictures, check to make sure that EACH picture is under 10MB. If you are uploading several pictures at the same time, the upload may take a couple of minutes. If you can't determine the picture file size, email us your picture(s). We will review, resize, and upload them for you.
<br>
QUESTION:
How come changes that I make in my Property Manager do not get saved?
ANSWER:
You MUST click the green Save button as you make changes in your Property Manager.
<br>
QUESTION:
Once I log in to my Property Manager, I see a 5 tabs. What are these?
ANSWER:
The Home Tab is an introduction.
The Property Tab is where you manage your property(s) criteria.
The Inquiry Tab is where all of your inquiries are backed up (click on the blue name to see the entire inquiry).
The Advertising Opportunities Tab is where you'll find low-cost opportunities to participate in some of our advertising. Scroll down to the PayPal button. NOTE: You can pay ANY fees (including your annual fee) using a credit card.
The FAQ tab is for frequently asked questions.
<br>
QUESTION:
Once I log in to my Property Manager, I noticed my rental name(s) are light blue. Why?
ANSWER:
Your property name links to your full details page. After you edit and SAVE your property criteria, click on the light blue property name to review your changes.
<br>
QUESTION:
I know very little about computers. Is there a step-by-step guide on how to use my Property Manager?
ANSWER:
Yes! Download our Property Manager User Guide and keep it by your computer.
<br>
DOWNLOAD GUIDE
<br><br><br>
QUESTION:
How do I retrieve a lost Login or Password?
ANSWER:
Call us at 608-850-4240, or email us. We have your log in information archived.
<br>
QUESTION:
Can I change my username and password?
ANSWER:
Yes. However, you need to first tell us your new username and/or password so we can update our Main Admin.
<br>
QUESTION:
Can I change my pin placement on the Google Map?
ANSWER:
Yes, but it does require a few steps in your Property Manager. If you would rather have us do it, just call or email us. If you're feeling BRAVE, log in and click on the Location Tab. To the right you will see Latitude and Longitude.
To move the pin slightly RIGHT, change the numbers like this: Example -90.9667274 to -90.9657274
To move the pin slightly LEFT, change Longitude numbers like this: Example -90.9667274 to -90.9677274
To move the pin slightly UP, change Latitude numbers like this: Example 45.8235069 to 45.8245069
To move the pin slightly DOWN, change Latitude numbers like this: Example 45.8235069 to 45.8225069
Click Save. Go to our home page and refresh your browser. Zoom in to your pin(s) to see new position.

The Property Manager is a new product for our members. If you have any questions or discover any issue -- please call us at 608-850-4240, or email us at info@rentwisconsincabins.com.
<br>
QUESTION:
Email inquiries are NOT showing up in my business email. Why?
ANSWER:
Check your Spam inbox. Inquiries come directly from vacationers, and might end up in your spam/bulk mail. Make sure you log in to your Property Manager and click on the Inquiries Tab. We archive ALL YOUR INQUIRIES here as a backup.
<br>
QUESTION:
I'm not getting many inquiries. Why?
ANSWER:
The first thing to review is the traffic to your details page(s). This number will soon be available in your Property Manager, but for now you need to call or email us to get your DETAIL LISTING REPORT. If you are getting lots of visits to your details page(s), you should review your property descriptions. Are they exciting and enticing? If your photo gallery only has 4-5 pictures, that's a problem. Photo galleries that show 12-15 good quality pictures lead to more inquiries. We introduce your property to vacationers DIALY. FIRST IMPRESSIONS ARE IMPORTANT.
<br><br>
QUESTION:
How do I update my Availability Calendar(s)?
ANSWER:
Easy. Log in to your Property Manager and click on the Availability Tab. Click on each day you are booked - the days will turn RED. If you have a cancellation, click on each Red day - the days will turn White. Click the SAVE button to save your updates.",
        ]);
        Page::create([
        	'state_id' => 2,
        	'title' => 'Advertising Opportunities',
        	'slug' => 'advertising-opportunities',
        	'content' => 'Second Site Content',
    	]);
    	Page::create([
            'state_id' => 2,
            'title' => 'FAQ',
            'slug' => 'faq',
            'content' => 'Second Site Content',
        ]);
    }
}
