<?php

use Illuminate\Database\Seeder;
use App\Models\Amenity;

class AmenitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Amenity::truncate();
        Amenity::create([
            
            'title' => 'Pets Allowed',
        ]);
        Amenity::create([
            
            'title' => 'Handicap Access',
        ]);
        Amenity::create([
            
            'title' => 'Air Conditioning',
        ]);
        Amenity::create([
            
            'title' => 'Linens Provided',
        ]);
        Amenity::create([
            
            'title' => 'Washer &amp; Dryer',
        ]);
        Amenity::create([
            
            'title' => 'Furnished Kitchen',
        ]);
    }
}
