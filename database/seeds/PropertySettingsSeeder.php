<?php

use Illuminate\Database\Seeder;
use App\Models\PropertySettings;

class PropertySettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        PropertySettings::truncate();
        PropertySettings::create(['setting' => 'On large recreation lake']);
        PropertySettings::create(['setting' => 'On medium recreation lake']);
        PropertySettings::create(['setting' => 'On small quiet no-wake lake']);
        PropertySettings::create(['setting' => 'On a river']);
        PropertySettings::create(['setting' => 'Walking distance to water']);
        PropertySettings::create(['setting' => 'Off water']);
    }
}
