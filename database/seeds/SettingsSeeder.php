<?php

use Illuminate\Database\Seeder;

use App\Models\Settings;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Settings::truncate();
    	Settings::create([
            'state_id' => 1,
            'google_analytics' => '123',
            'from_email' => 'info@rentwisconsincabins.com',
            'social_media' => '{"facebook":"https:\/\/www.facebook.com\/RentWisconsinCabins","twitter":"https:\/\/twitter.com\/#!\/RentWisCabins","google-plus":"https:\/\/plus.google.com\/u\/0\/b\/107299725286010525076\/107299725286010525076\/posts\/p\/pub","youtube":"http:\/\/www.youtube.com\/user\/RentWisconsinCabins\/","pinterest":"http:\/\/pinterest.com\/rentwiscabins\/"}',
            'latitude' => '44.755514',
            'longitude' => '-90.175781',
            'home_header' => "<div id='seo' class='text-center'>
<h1>Wisconsin cabin rentals. Compare 674 vacation cabins, cottages, lodges and homes. Rustic to lakefront upscale. Rent direct, skip those booking website traveler fees.</h1>
</div>
<div id='use' class='hidden-xs text-center'>
<h2>Rental pins are stacked. Zoom in and watch them separate.</h2>
</div>",
            'home_header_image' => '/images/bg-photoset-02.jpg',
            'home_sidebar' => "<div id='subscribe' class='mb40'>
	<h2><span>Sign Up for</span> Vacation Rental Specials</h2>

	<p>Monthly newsletter includes other surprises.</p>

	<div class='createsend-button' data-listid='t/81/D34/B16/B41820B8E9ABD1D3'></div>

	<script type='text/javascript'>
		(function () {
			var e = document.createElement('script');
			e.type = 'text/javascript';
			e.async = true;
			e.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://btn.createsend1.com/js/sb.min.js?v=2' e.className = 'createsend-script';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(e, s);
		})();
	</script>
</div><div class='fb-page' data-href='https://www.facebook.com/RentWisconsinCabins' data-tabs='timeline' data-width='300' data-height='325' data-small-header='true' data-adapt-container-width='true' data-hide-cover='false' data-show-facepile='false'><div class='fb-xfbml-parse-ignore'><blockquote cite='https://www.facebook.com/RentWisconsinCabins'><a href='https://www.facebook.com/RentWisconsinCabins'>RentWisconsinCabins.com</a></blockquote></div></div>
<br/><br/>

<div id='fb-root'></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = '//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>",
'home_content' => "<h2 style='box-sizing: border-box; font-family: Arvo, serif; font-weight: 400; line-height: 1.1; color: #595148; margin-top: 20px; margin-bottom: 10px; font-size: 20px; background-color: #f7f7f3;'>Wisconsin cabin rentals</h2>
<p style='box-sizing: border-box; margin: 0px 0px 10px; color: #444444; font-family: Roboto, sans-serif; font-size: 14px; line-height: 20px; background-color: #f7f7f3;'><span style='box-sizing: border-box; font-weight: bold;'>At RentWisconsinCabins.com&reg; you always connect and book DIRECTLY with Wisconsin vacation rental owners or local property managers.</span>&nbsp;Avoid middleman booking websites and you&rsquo;ll avoid paying those hidden traveler fees.</p>",
'home_bottom_content' => "<p style='box-sizing: border-box; margin: 0px 0px 10px; color: #444444; font-family: Roboto, sans-serif; font-size: 14px; line-height: 20px; background-color: #f7f7f3;'>Watch our selection of Wisconsin cabin rentals grow every month. Comparing popular&nbsp;<a style='box-sizing: border-box; color: #a83828; text-decoration: none; transition: all 0.2s ease; border-bottom-width: 0.08em; border-bottom-style: solid; border-bottom-color: rgba(0, 0, 0, 0.0980392); padding-bottom: 0.08em; background: 0px 0px;' href='http://www.rentwisconsincabins.com/location.php?id=Eagle%20River' target='_blank'>Eagle River cabins</a>&nbsp;and&nbsp;<a style='box-sizing: border-box; color: #a83828; text-decoration: none; transition: all 0.2s ease; border-bottom-width: 0.08em; border-bottom-style: solid; border-bottom-color: rgba(0, 0, 0, 0.0980392); padding-bottom: 0.08em; background: 0px 0px;' href='http://www.rentwisconsincabins.com/location.php?id=Wisconsin%20Dells' target='_blank'>Wisconsin Dells cabins</a>&nbsp;is easy and fun. But don&rsquo;t overlook the other 1,264 towns in Wisconsin, some of the best Cabin Time memories for couples, groups of friends, family reunions, or weddings are off-the-beaten-path. July and August are popular months to rent cabins in Wisconsin (that&rsquo;s your hint to send inquiries to rental owners months in advance), but don&rsquo;t let the cold and snow keep you inside. Half of the properties listed here are renter-ready year round.</p>
<p style='box-sizing: border-box; margin: 0px 0px 10px; color: #444444; font-family: Roboto, sans-serif; font-size: 14px; line-height: 20px; background-color: #f7f7f3;'>Sorry, no hotels or condo apartments here. Our perfectly normal obsession with hooking you up with Wisconsin vacation rentals has resulted in the Internet&rsquo;s largest selection of Wisconsin cabins, cottages, lodges and homes.</p>
<p style='box-sizing: border-box; margin: 0px 0px 10px; color: #444444; font-family: Roboto, sans-serif; font-size: 14px; line-height: 20px; background-color: #f7f7f3;'><span style='box-sizing: border-box; font-weight: bold;'>Relax. Enjoy your search. It&rsquo;s cabin time in WI.</span></p>",
'footer_links' => "<li><a href='/about.php' title='About'>About</a></li>,
						<li><a href='/contact.php' title='Contact'>Contact</a></li>
						<li><a href='/testimonials.php' title='Testimonials'>Testimonials</a></li>
						<li><a href='/links.php' title='Links'>Links</a></li>
						<li><a href='/community.php' title='Community'>Community</a></li>
						<li><a href='/advertise.php' title='Advertise'>Advertise</a></li>
						<li><a href='/jobs.php' title='Employment'>Employment</a></li>
						<li><a href='/privacy.php' title='Privacy Policy'>Privacy Policy</a></li>
						<li><a href='/terms.php' title='Terms of Use'>Terms of Use</a></li>
						<li><a href='/feedback.php' title='Feedback?'>Feedback?</a></li>
						<li><a href='/sitemap.php' title='Sitemap'>Sitemap</a></li>",
'footer_sidebar_links' => "<a href='/romantic-getaways-in-wisconsin.php' title='Romantic cabin getaways'>Romantic Getaways in Wisconsin</a><br><br>
					<a href='/wisconsin-snowmobile-trails.php' title='Wisconsin Snowmobile Trails and Cabins'>Wisconsin Snowmobile Trails</a><br><br>
					<a href='/large-group-accommodation.php' title='Large Group Vacation Rentals'>Large Group Accommodations</a><br><br>
					<a href='/cabin-rental-videos.php' title='Wisconsin Vacation Rental Drone Videos'>Drone Videos of Wisconsin Cabins</a><br><br>
					<a href='/things-to-do-in-wi.php' title='Things to do in WI'>Things To Do in Wisconsin</a><br><br>",
        ]);
    }
}
