<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::truncate();
        // User::create([
        //     'name' => 'Super Admin',
        //     'email' => 'jmogensen@jmcreativegroup.com',
        //     'password' => bcrypt('relaxenjoy!'),
        //     'role' => 'admin',
        //     'status' => 'active',
        // ]);
        // User::create([
        //     'name' => 'e-Media Admin',
        //     'email' => 'support@e-mediaresources.com',
        //     'password' => bcrypt('68JZ8b6XLbGT20J'),
        //     'role' => 'admin',
        //     'status' => 'active',
        // ]);
        User::create([
            'name' => 'Roger Admin',
            'email' => 'admin@e-mediaresources.com',
            'password' => bcrypt('abc123'),
            'role_id' => '2',
            'status' => 'active',
        ]);
        User::create([
            'name' => 'Roger Owner',
            'email' => 'owner@e-mediaresources.com',
            'password' => bcrypt('abc123'),
            'state_id' => 1,
            'role_id' => '3',
            'status' => 'active',
        ]);
        User::create([
            'name' => 'Test Owner1',
            'state_id' => 1,
            'email' => 'test1@test.com',
            'password' => bcrypt('abc123'),
            'role_id' => '3',
            'status' => 'active',
        ]);
        User::create([
            'name' => 'Test Owner2',
            'state_id' => 1,
            'email' => 'test2@test.com',
            'password' => bcrypt('abc123'),
            'role_id' => '3',
            'status' => 'active',
        ]);
        User::create([
        	'name' => 'Test Owner3',
            'state_id' => 2,
        	'email' => 'test3@test.com',
        	'password' => bcrypt('abc123'),
            'role_id' => '3',
            'status' => 'active',
    	]);
    }
}
