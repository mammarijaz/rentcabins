<?php

use Illuminate\Database\Seeder;
use App\Models\State;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	State::truncate();
    	State::create([
            'name' => 'Wisconsin',
            'seo_name' => 'Rent Wisconsin Cabins',
            'abbr' => 'WI',
            'domain' => 'rentwisconsincabins2.local',
            'main_logo' => 'http://rentwisconsincabins2.local/images/logo.png',
        ]);
        State::create([
            'name' => 'Kansas',
            'seo_name' => 'Rent Kansas Cabins',
            'abbr' => 'KS',
            'domain' => 'rentkansascabins.local',
            'main_logo' => 'http://rentkansascabins.local/images/logo2.png',
        ]);
        State::create([
            'name' => 'Idaho',
            'seo_name' => 'Rent Idaho Cabins',
            'abbr' => 'ID',
            'domain' => 'rentidahocabins.local',
            'main_logo' => 'http://rentidahocabins.local/images/logo2.png',
        ]);
        State::create([
    		'name' => 'Iowa',
            'seo_name' => 'Rent Iowa Cabins',
    		'abbr' => 'IO',
    		'domain' => 'rentiowacabins.local',
            'main_logo' => 'http://rentiowacabins.local/images/logo.png',
		]);
    }
}
