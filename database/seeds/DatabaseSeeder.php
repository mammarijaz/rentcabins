<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->call(UsersTableSeeder::class);
        $this->call(StatesTableSeeder::class);
        $this->call(PagesTableSeeder::class);
        $this->call(RegionSeeder::class);
        $this->call(LocationSeeder::class);
        $this->call(SettingsSeeder::class);
        $this->call(AmenitySeeder::class);
        $this->call(PropertySettingsSeeder::class);
        $this->call(StylesSeeder::class);
        $this->call(UserRoleSeeder::class);
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
