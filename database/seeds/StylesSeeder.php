<?php

use Illuminate\Database\Seeder;
use App\Models\PropertyStyles;

class StylesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
    	PropertyStyles::truncate();
        PropertyStyles::create(['style' => 'Single cabin/cottage']);
        PropertyStyles::create(['style' => 'Multiple cabin/cottage resort/lodge']);
        PropertyStyles::create(['style' => 'Private home']);
    }
}
