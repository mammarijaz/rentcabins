@extends('layouts.app', ['two_pack' => 'false'])
@section('content')
<div class="container main-container">

	<div class="row">
		<div class="col-sm-8 col-sm-push-4">
			@include('partials.featured-property', ['featuredProperty' => $featuredProperty])

			<h1>You&rsquo;re one step closer to a great {{$state->abbr}} cabin getaway.</h1>
				<p>Remember, you book directly with the rental owner or local manager and skip those booking website traveler fees.</p>
				<h1>Now learn about our (4) $50 GAS CARD GIVAWAYS.</h1>
			<p>It&rsquo;s easy. If you end up booking your cabin, simply Reply to the courtesy email we just sent you and you&rsquo;ll be eligible to WIN one of 4 $100 GAS CARDS. Drawings are April 1, July 1, October 1, January 1. You will be eligible for the 4 drawings after your check in date. This is a {{$state->seo_name}} special promotion.</p>

			<br><p>Rental owner(s) will likely contact you by email, so watch your inbox and your spam folder. If you provided a phone number, you could get a call. We encourage owners to respond quickly, but it may take a day or two. If you need answers right away, we recommend calling rental owners.</p>

			<p>As mentioned, we just sent you a courtesy email that includes the contact information for property(s) you just inquired about, plus information about the $100 GAS CARD GIVAWAY – it might be in your spam folder.</p>

			<p>Thank you for choosing {{$state->seo_name}} to find for your perfect vacation rental. Please tell your family and friends about our book-direct website. Avoid third-party booking websites and avoid paying more for the same rental.</p>
		</div>
		<div class="col-sm-4 col-sm-pull-8">
			{{$settings->sidebar}}
		</div>
	</div>

</div>
@endsection
