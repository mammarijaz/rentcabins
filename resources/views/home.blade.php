@extends('layouts.app', ['two_pack' => 'false'])
@push('styles')
<link rel="stylesheet" href="/js/libs/datepicker/jquery.datepick.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/js/libs/datepicker/ui-cupertino.datepick.css" type="text/css" media="screen" />
@endpush
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">

            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    {!! $settings->home_header !!}
                </div>
            </div>

        </div>
    </div>
</div>
<a href="https://plus.google.com/u/0/b/107299725286010525076/107299725286010525076/posts/p/pub" rel="publisher"></a>

<div class="container main-container">
    <!-- search & map -->
    <div class="row">
        <div class="col-sm-5 col-md-4">
            <div id="search-container" class="fancyframe">
                @include('includes.search-form')
            </div>
        </div>
        <div class="col-sm-7 col-md-8 hidden-xs">
            <div id="map-container" class="fancyframe">
                @include('includes.map', ['locations' => $locations, 'amenities' => $amenities, 'region' => $regions, 'isResort' => 'false'])
            </div>
        </div>
    </div>
    <!-- /search & map -->

    <!-- results, social & seo -->
    <div class="row">
        <div class="col-sm-8 col-sm-push-4">
            <a name="list"></a>
            <a name="show_results"></a>
            @if($featuredProperty)
                @include('partials.featured-property', ['featuredProperty', $featuredProperty])
            @endif

            <p class="result-number noshow"><span>0</span> area vacation rentals match criteria</p>

            <!-- results container -->
            <div id="form-search-results" onsubmit="return false;">
                <div id="results"></div>
            </div>

            <script type="text/template" id="result-template">
            <div class="row">
                <div class="col-sm-12">

                    <div class="result-container">
                        <div class="inquiry-select">
                            <label>
                                <input class="result_select_rental" type="checkbox" name="select_rental[]" value=""> select
                            </label>
                            <button type="button" class="btn btn-icon btn-xs btn-tooltip" data-toggle="tooltip" data-placement="auto left" title="Send 1 inquiry to multiple rental owners. SELECT this listing, PLUS your other favorite listings. An Inquiry form will appear below the last listing. Otherwise, send inquiries from the details pages."><span class="glyphicon glyphicon-question-sign"></span></button>
                        </div>

                        <div class="row no-gutter clickable_hotspot">
                            <div class="col-sm-5 col-md-4 text-center">
                                <div class="photo fancyframe">
                                    <img src="" alt="" class="result_photo img-responsive">
                                </div>
                            </div>
                            <div class="col-sm-7 col-md-8 pt30 pb15">
                                <h2 class="result_resort mt0"></h2>
                                <h2 class="result_name mt0"></h2>
                                <h3 class="result_city mt0"></h3>

                                <ul class="list-inline">
                                    <li class="result_bedrooms">- bedrooms,</span>
                                    <li class="result_bathrooms">- bathrooms,</span>
                                    <li class="result_max_capacity">sleeps -</span>
                                </ul>

                                <div class="row"> 
                                    <div class="col-sm-6">
                                    <span style="font-weight: bold;">BOOK DIRECT</span>
                                    <span class="result_rates_daily">- daily<br></span>
                                    <span class="result_rates_weekly">- weekly</span>
                                    </div>
                                    <div style="font-style: italic; opacity: 0.4" class="col-sm-6">
                                    3rd party booking
                                    <span class="result_rates_daily_strike">- daily<br></span>
                                    <span class="result_rates_weekly_strike">- weekly</span>
                                    </div>
                                </div>
                                <br>
                                <p class="result_description"><small></small></p>

                                <p><a class="result_details_link" href="javascript:" data-href="/rental/">VIEW DETAILS and rent direct. No booking website fees. &raquo;</a></p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            </script>
            <!-- /result container -->

            <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "Organization",
      "url": "{{$state->domain}}",
      "logo": "/images/{{$state->logo}}"
    }
    </script>

            <script type="application/ld+json">
{
  "@context" : "http://schema.org",
  "@type" : "Organization",
  "name" : "{{$state->seo_name}}",
  "url" : "{{$state->domain}}",
  "sameAs" : [
    "{{$settings->social_media['facebook']}}",
    "{{$settings->social_media['twitter']}}",
    "{{$settings->social_media['pinterest']}}"
  ]
}
</script>

        <!-- inquiry form -->
        <div class="row">
            <div class="col-sm-12 inquiry-form-container">
                @include('includes.inquiry-form', ['is_search' => true, 'inquiryHtml' =>
                    '<h2>Inquire and book directly with owners/managers.</h2>
                    <p>Select your favorite properties above. Send one inquiry to multiple rental owners.</p>'])
            </div>
        </div>
        <!-- /inquiry form -->

        <!-- seo & events -->
        <div class="row">
            <div class="col-md-7">
                {!! $settings->home_content !!}
                @foreach($regions as $region)
                <h3>Cabin rentals in {{$region->name}} {{$state->name}}</h3>
                <ul style="margin:0px 0px 25px;padding:0px;">
                    @foreach($region->locations()->get() as $location)
                        <a href="/location/{{$location->slug}}" target="_blank">{{$location->name}}</a>&nbsp;|
                    @endforeach
                </ul>
                @endforeach
                {!! $settings->home_bottom_content !!}
            </div>
            <div class="col-md-5">
            @foreach($featuredPages as $featuredPage)
            <p>
                <a href="/{{$featuredPage->slug}}" target="_blank">
                    @if($featuredPage->thumb_image)
                        <img src="{{$featuredPage->thumb_image}}" alt="{{$featuredPage->title}}" class="img-responsive">
                    @else
                        <p style="font-size:20px;height: 150px;">{{$featuredPage->title}}</p>
                    @endif
                </a>
            </p>
            @endforeach
            </div>
        </div>
        <!-- /seo & events -->

    </div>
    <div class="col-sm-4 col-sm-pull-8">
        <hr class="dashed tall visible-xs">
        {!! $settings->home_sidebar !!}
    </div>
</div>
<!-- /results, social & seo -->

</div>
@endsection
