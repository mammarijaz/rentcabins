@extends('layouts.app', ['two_pack' => 'false'])
@section('content')
<div style="padding-bottom: 40px" class ="container">
	<div class="row">
        <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">

            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <div id="seo" class="text-center">
						<h1>List your {{$state->name}} vacation rentals. We offer a more relaxed approach to promoting rentals that pays off for everyone. No booking commissions or traveler fees.</h1>

						<h2>We deliver inquiries, you complete the booking</h2>
					</div>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="container main-container">

	<div class="row">
		<div class="col-sm-8 col-sm-push-4">
			@include('partials.featured-property', ['featuredProeprty' => $featuredProperty])

			<h1>Thank you for listing your {{$state->name}} rental(s)</h1>
			<p>Getting your vacation rental(s) found on the Internet is what we’re good at. We will call you within the next 24 hours to say "thanks" then build your listing(s). Once your listing(s) are Live, we will send you our Welcome New Member email which includes a Username and Password to your Property Manager. Then we will get your payment preference (check or credit card). Once payment is received we activate your inquiry form(s).</p>

			<p>Keep your photo gallery updated. Use quality pictures of interiors, exteriors, and lifestyle. CAREFULLY review all criteria and try using your Availability Calendar. Vacationers are often seeing your rentals for the first time, so make a great first impression.</p>

			<p>Note: If you are listing a multi-cabin resort, where several cabins share the same street address, we will need a map of the resort grounds so we can position the pins on each cabin. If a resort map is on your property website, we will find it.</p>

			<p>Interested in FEATURING your rentals on our website (like the Featured Property banner above)? It’s only $555 per year. We offer value around every corner.</p>
			<p>If you have any questions, call us. We’re here to help you maximize your exposure and inquiries every year. 608-850-4242.</p>
			<h3>The {{$state->seo_name}} Team</h3>

		</div>
		<div class="col-sm-4 col-sm-pull-8">
			{{$settings->sidebar}}
		</div>
	</div>

</div>
@endsection
