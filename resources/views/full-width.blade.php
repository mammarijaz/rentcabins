@extends('layouts.app', ['two_pack' => 'false'])
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">

            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <div id="seo" class="text-center">
					<h1>{!!$page->header_text !!}</h1>
					</div>
					<div id="use" class="hidden-xs text-center">
					<h2></h2>
					</div>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="container main-container">
	<div class="row">
		<div class="col-sm-8 col-sm-push-4">
			@if($featuredProperty)
                @include('partials.featured-property', ['featuredProperty', $featuredProperty])
            @endif
			<h3>{{ $page->title }}</h3>
			<hr>
			{!! $page->content !!}
		</div>
		<div class="col-sm-4 col-sm-pull-8">
		{!! $settings->home_sidebar !!}
		</div>
	</div>
</div>
@endsection
