<h2 class="title">Narrow Your Search for {{ $state->abbr }} Vacation Rentals</h2>
<p>Select some criteria and click Update Map or Show Listings. Or, dive right into the map.</p>
<form action="/api/properties/" method="get" id="searchform" class="mb30" role="form">
	{{ csrf_field() }}
	<div class="form-group">
		<select id="destination" class="form-control select2" name="locations[]" multiple="multiple">
			@foreach($locations as $id => $location)
				<option value="{{$id}}">{{$location}}</option>
			@endforeach
		</select>
	</div>

	<div class="form-group">
		<select id="region" class="form-control select2" name="regions[]" multiple="multiple">
			@foreach($regions->lists('name', 'id') as $id => $region)
				<option value="{{$id}}">{{$region}}</option>
			@endforeach
		</select>
	</div>

	<div class="row">
		<div class="col-sm-6">
			<div class="form-group input-group datepicker-icon">
				<input type="text" id="arrival_date" class="form-control datepicker" name="arrival_date" placeholder="arrival">
				<span class="input-group-btn">
			      <button class="btn btn-default" type="button">
			        <span class="glyphicon glyphicon-calendar date-button" ></span>
			      </button>
			    </span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group input-group datepicker-icon">
				<input type="text" id="departure_date" class="form-control datepicker" name="departure_date" placeholder="departure">
				<span class="input-group-btn">
			      <button class="btn btn-default" type="button">
			        <span class="glyphicon glyphicon-calendar date-button" ></span>
			      </button>
			    </span>
			</div>
		</div>
	</div>

	<div class="form-group">
		<select id="setting" class="form-control select2" name="settings[]" multiple="multiple">
			@foreach($propertySettings as $id => $setting)
				<option value="{{$id}}">{{$setting}}</option>
			@endforeach
		</select>
	</div>

	<div class="form-group">
		<select id="style" class="form-control select2" name="styles[]" multiple="multiple">
			@foreach($propertyStyles as $id => $style)
				<option value="{{$id}}">{{$style}}</option>
			@endforeach
		</select>
	</div>

	<div class="form-group rate-container">
		<label for="amount-daily">Adjust nightly rates</label>
		<input type="text" id="amount-daily" name="max_rates_daily" />
		<div id="daily-rate" class="slider"></div>
	</div>

	<div class="form-group rate-container">
		<label for="amount-weekly">Adjust weekly rates</label>
		<input type="text" id="amount-weekly" name="max_rates_weekly" />
		<div id="weekly-rate" class="slider"></div>
	</div>

	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<select id="bedrooms" class="form-control select2" name="bedrooms">
					<option value=""></option>
					<option value="1">1+</option>
					<option value="2">2+</option>
					<option value="3">3+</option>
					<option value="4">4+</option>
					<option value="5">5+</option>
					<option value="6">6+</option>
					<option value="7">7+</option>
					<option value="8">8+</option>
					<option value="9">9+</option>
					<option value="10">10+</option>
				</select>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<select id="sleeps" class="form-control select2" name="max_capacity">
					<option value=""></option>
					<option value="2">2+</option>
					<option value="3">3+</option>
					<option value="4">4+</option>
					<option value="5">5+</option>
					<option value="6">6+</option>
					<option value="7">7+</option>
					<option value="8">8+</option>
					<option value="9">9+</option>
					<option value="10">10+</option>
					<option value="11">11+</option>
					<option value="12">12+</option>
					<option value="13">13+</option>
					<option value="14">14+</option>
					<option value="15">15+</option>
					<option value="16">16+</option>
					<option value="17">17+</option>
					<option value="18">18+</option>
					<option value="19">19+</option>
					<option value="20">20+</option>
				</select>
			</div>
		</div>
	</div>

	<div class="form-group">
		<select id="amenities" class="form-control select2" name="amenities[]" multiple="multiple">
			@foreach($amenities as $id => $name)
				<option value="{{$id}}">{{$name}}</option>
			@endforeach
		</select>
	</div>

	<input type="submit" id="btn-search" value="Update Map" class="btn btn-contrast btn-search" />
	<a href="#list" id="show-listings" title="Show Listings" class="btn btn-contrast btn-search">Show Listings</a>
</form>

<div class="tape"></div>
