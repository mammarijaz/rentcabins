<div class="map">
	<div class="property-detail" style="display:none; z-index: 99999999999999;">
		<h3 class="result_pd_resort"></h3>
		<h2 class="result_pd_name"></h2>
		<h3 class="result_pd_city_state"></h3>

		<img class="result_pd_photo mb15" src="" alt="" width="196" height="106" style="-ms-interpolation-mode:bicubic;">

		<p>
			<span>
				<span class="result_pd_bedrooms">- bedrooms</span>
	            <span class="result_pd_bathrooms">- bathrooms<br></span>
				<span class="result_pd_max_capacity">sleeps -<br></span>
			</span>
				<span class="result_pd_on_waters">On -</span>
		</p>

		<div class="row"> 
			<div class="col-sm-6">
			<span style="font-weight: bold;">BOOK DIRECT</span>
			<span class="result_pd_max_rates_daily">- daily<br></span>
			<span class="result_pd_max_rates_weekly">- weekly</span>
			</div>
			<div style="font-style: italic; opacity: 0.4" class="col-sm-6">
			<span>3rd party booking</span>
			<span class="result_pd_max_rates_daily_strike">- daily</span><br>
			<span class="result_pd_max_rates_weekly_strike">- weekly</span>
			</div>
		</div>
		<br>
		<a href="/rental/" title="View Details" class="btn btn-contrast btn-sm pull-right result_pd_details" target="_blank">View details. Book direct &amp; save!</a><br><br><br>
		<a href="" title="resortpage" class = "result_pd_res_page" target="_blank"></a><br>
		<a href="/location/" title="townlisting" class = "result_pd_city" target="_blank"></a>
		<button class="close"></button>
	</div>
	@if($isResort == 'true')
		<div id="map_canvas" class="resort-map"></div>
	@else
		<div id="map_canvas"></div>
	@endif
	<div class="tape"></div>
</div>
<script type="text/template" id="result-template">
<div class="row">
    <div class="col-sm-12">

        <div class="result-container">
            <div class="inquiry-select">
                <label>
                    <input class="result_select_rental" type="checkbox" name="select_rental[]" value=""> select
                </label>
                <button type="button" class="btn btn-icon btn-xs btn-tooltip" data-toggle="tooltip" data-placement="auto left" title="Send 1 inquiry to multiple rental owners. SELECT this listing, PLUS your other favorite listings. An Inquiry form will appear below the last listing. Otherwise, send inquiries from the details pages."><span class="glyphicon glyphicon-question-sign"></span></button>
            </div>

            <div class="row no-gutter clickable_hotspot">
                <div class="col-sm-5 col-md-4 text-center">
                    <div class="photo fancyframe">
                        <img src="" alt="" class="result_photo img-responsive">
                    </div>
                </div>
                <div class="col-sm-7 col-md-8 pt30 pb15">
                    <h2 class="result_resort mt0"></h2>
                    <h2 class="result_name mt0"></h2>
                    <h3 class="result_city mt0"></h3>

                    <ul class="list-inline">
                        <li class="result_bedrooms">- bedrooms,</span>
                        <li class="result_bathrooms">- bathrooms,</span>
                        <li class="result_max_capacity">sleeps -</span>
                    </ul>

                    <div class="row"> 
                        <div class="col-sm-6">
                        <span style="font-weight: bold;">BOOK DIRECT<br></span>
                        <span class="result_rates_daily">- daily<br></span>
                        <span class="result_rates_weekly">- weekly</span>
                        </div>
                        <div style="font-style: italic; opacity: 0.4" class="col-sm-6">
                        3rd party booking<br>
                        <span class="result_rates_daily_strike">- daily<br></span>
                        <span class="result_rates_weekly_strike">- weekly</span>
                        </div>
                    </div>
                    <br>
                    <p class="result_description"><small></small></p>

                    <p><a class="result_details_link" href="javascript:" data-href="/rental/">VIEW DETAILS and rent direct. No booking website fees. &raquo;</a></p>
                </div>
            </div>
        </div>

    </div>
</div>
</script>
<!-- /result container -->
{{-- <script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key={{$state->map_key}}&sensor=false"></script> --}}
<script type="text/javascript">
	var googleMap = null;
	var mapPD = {};
	function googleMapInit() {
		var styleArray = [
			{
				featureType: "poi",
				elementType: "labels",
				stylers: [
					{ visibility: "off" }
				]
			}
		];
		// Initialize Map
		var mapOptions = {
			center: new google.maps.LatLng({{$settings->latitude}}, {{$settings->longitude}}),
			zoom: 7,
			maxZoom: 19,
			mapTypeId: google.maps.MapTypeId.TERRAIN,
			disableDefaultUI: true,
			zoomControl: true,
			mapTypeControl: true,
			zoomControlOptions: {
				style: google.maps.ZoomControlStyle.LARGE,
				position: google.maps.ControlPosition.LEFT_CENTER
			},
			styles: styleArray
		};
		googleMap = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
		bounds = new google.maps.LatLngBounds();
		google.maps.event.addListener(googleMap, 'bounds_changed', function() {
  			google.maps.event.trigger(googleMap, 'resize');
		});
		google.maps.event.addListener(googleMap, 'zoom_changed', function() {
			if(googleMap.getZoom(+5) >= 12)
			{
				googleMap.setMapTypeId(google.maps.MapTypeId.HYBRID);
			}
			else
			{
				googleMap.setMapTypeId(google.maps.MapTypeId.TERRAIN);
			}
		});
	}
	function googleMapUpdate(result) {
		// Drop Markers
		$.each(result, function(i, rlocation){
			var myLatlng = null;
			if(rlocation.address_longitude != '' && rlocation.address_latitude != '' && rlocation.address_longitude != null && rlocation.address_latitude != null) {
				myLatlng = new google.maps.LatLng(rlocation.address_latitude,rlocation.address_longitude);
				if(typeof $('#resort_id').attr('value') === 'undefined' || $('#resort_id').attr('value') == rlocation.resort_id) {
					var marker = new google.maps.Marker({
						position: myLatlng,
						map: googleMap,
						title: rlocation.name+' :: '+rlocation.public_id
					});
					bounds.extend(marker.getPosition());
				}

				google.maps.event.addListener(marker, 'click', function() {
					var pkid = marker.title.split(' :: ').pop();

					if(typeof(mapPD[pkid]) != "undefined") {
						var pdlocation = mapPD[pkid];
						if(pdlocation['resort'] !== null) {$('.result_pd_resort').html(pdlocation.resort['name']).show();} else {$('.result_pd_resort').html("").hide();}
						$('.result_pd_name').html(pdlocation.name);
						if(pdlocation['bedrooms']!='') {$('.result_pd_city_state').html(pdlocation.city+', {{$state->abbr}}');} else {$('.result_pd_city_state').html("").hide();}
						if(pdlocation.photos !== null && pdlocation.photos[0] != null)
						{
							$('.result_pd_photo').attr('src', pdlocation.photos[0].image_path);
						}
						else
						{
							$('.result_pd_photo').attr('src','/images/photo_placeholder.png');
						}
						$('.result_pd_photo').attr('alt',pdlocation.name);
						if(pdlocation['bedrooms']!='') {$('.result_pd_bedrooms').html(pdlocation.bedrooms+' bedroom').show();} else {$('.result_pd_bedrooms').html("").hide();}
						if(pdlocation['bathrooms']!='') {$('.result_pd_bathrooms').html(' | ' + pdlocation.bathrooms+' bathroom').show();} else {$('.result_pd_bathrooms').html("").hide();}
						if(pdlocation['max_capacity']!='') {$('.result_pd_max_capacity').html(' | Sleeps '+pdlocation.max_capacity+'').show();} else {$('.result_pd_max_capacity').html("").hide();}
						if(pdlocation['amenities'].indexOf('NO Pets Allowed') !== -1) {$('.result_pd_no_pets_allowed').html('NO Pets Allowed<br><br>').show();}
						else if(pdlocation['amenities'].indexOf('Pets Allowed') !== -1) {$('.result_pd_no_pets_allowed').html('Pets Allowed<br><br>').show();}
						else {$('.result_pd_no_pets_allowed').html("").hide();}
						if(pdlocation['on_waters']!='') {$('.result_pd_on_waters').html('<br><b>Zoom in to see exact location</b><br>On '+pdlocation.on_waters).show();} else {$('.result_pd_on_waters').html("").hide();}
						if(pdlocation['max_rates_daily']!='' && pdlocation['max_rates_daily']!='0') {
							$('.result_pd_max_rates_daily').html('$'+pdlocation['min_rates_daily']+' - $'+pdlocation['max_rates_daily']+' per night<br>').show();
							$('.result_pd_max_rates_daily_strike').html('<span style="text-decoration: line-through;">$'+(parseFloat(pdlocation['min_rates_daily']) * 1.3).toFixed(0) +' - $'+(parseFloat(pdlocation['max_rates_daily']) * 1.3).toFixed(0)+' per night</span>').show();
						}
						else {
							if(pdlocation['max_rates_weekly']=='' && pdlocation['max_rates_weekly']=='0') {
								$('.result_pd_max_rates_daily').html("").hide();
								$('.result_pd_max_rates_daily_strike').html("").hide();
							} else {
								$('.result_pd_max_rates_daily').html("Inquire for daily rates").show();
								$('.result_pd_max_rates_daily_strike').html("Not available").show();
							}
						}
						if(pdlocation['max_rates_weekly']!='' && pdlocation['max_rates_weekly']!='0') {
							$('.result_pd_max_rates_weekly').html('$'+pdlocation['min_rates_weekly']+' - $'+pdlocation['max_rates_weekly']+' per week<br>').show();
							$('.result_pd_max_rates_weekly_strike').html('<span style="text-decoration: line-through;">$'+(parseFloat(pdlocation['min_rates_weekly']) * 1.3).toFixed(0) +' - $'+(parseFloat(pdlocation['max_rates_weekly']) * 1.3).toFixed(0)+' per week</span>').show();
						}
						else {
							if(pdlocation['max_rates_daily']=='' && pdlocation['max_rates_daily']=='0') { 
								$('.result_pd_max_rates_weekly').html("Inquire for details").show();
								$('.result_pd_max_rates_weekly_strike').html("Not available").show();
							} else {
								$('.result_pd_max_rates_weekly').html("Inquire for weekly rates").show();
								$('.result_pd_max_rates_weekly_strike').html("Not available").show();
							}
						}
						if(pdlocation['resort']!= null) {$('.result_pd_res_page').html('View more cabins at ' + pdlocation['resort']['name']).show();} else {$('.result_pd_city').html("").hide();}
						if(pdlocation['resort']!=null) {$('.result_pd_res_page').attr('href', '/resort/'+pdlocation['resort']['public_id']);}
						if(pdlocation['location']!= null) {$('.result_pd_city').html('View ALL ' + pdlocation['location']['name'] + ', {{$state->abbr}} ' + ' area rentals').show();} else {$('.result_pd_city').html("").hide();}
						$('.result_pd_details').attr('href','/rental/'+pdlocation['public_id']);
						if(pdlocation['location']!= null) {$('.result_pd_city').attr('href','/location/'+pdlocation['location']['slug']);}	
						setTimeout(function(){$('.property-detail').show(0);},300);
					}
				});
			}
		});
		googleMap.fitBounds(bounds);
	}
  //googleMapInit();
</script>
<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key={{$state->map_key}}&callback=googleMapInit" async defer></script>
