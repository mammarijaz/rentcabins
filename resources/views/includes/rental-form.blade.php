<h2>Join our team by filling out the form below!</h2>
<div id="optin">
	{!! $inquiryHtml or '' !!}
	{!! Form::open(['url' => action('PagesController@postRentalInquiry'), 'method' => 'POST', 'id' => 'inquiryform']) !!}
		<div class="row">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="{{$classContainer or 'col-sm-6'}}">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" id="first_name" name="name" placeholder="first name *" class="form-control" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" id="last_name" name="Lname" placeholder="last name" class="form-control" />
						</div>
					</div>
				</div>
				<div class="form-group">
					<input type="text" id="email" name="email" placeholder="email *" class="form-control" />
				</div>
				<div class="form-group">
					<input type="text" id="phone" name="phone" placeholder="phone" class="form-control" />
				</div>
				<div class="form-group">
					<input type="text" id="mobile" name="mobile" placeholder="Mobile Phone and Carrier Name (for free Text Alerts)" class="form-control" />
				</div>
				<div class="form-group">
					<input type="text" id="website" name="website" placeholder="Your property website" class="form-control" />
				</div>
				<div class="form-group">
					<input type="numRentals" id="numRentals" name="numRentals" placeholder="Number of rental listings. See prices below" class="form-control" />
				</div>
				<div class="checkbox">
					<label>
						<input type="checkbox" name="avroa" value="yes" />
						Are you an AVROA member?
					</label>
				</div>
			</div>
			<div class="{{$classContainer or 'col-sm-6'}}">
				<div class="form-group">
					<textarea id="personalization" class="form-control" rows="7" name="personalization" placeholder="Let us know more about your properties!"></textarea>
					<span class="help-block"><small>(Give us a few details about your {{$state->abbr}} getaway)</small></span>
				</div>

				{!! Form::submit('Send Listing Request', ['class' => 'btn btn-contrast mt30', 'id' => 'btn-inquiry']) !!}
			</div>
		</div>
	{!! Form::close() !!}
</div>
