<div id="optin">
	{!! $inquiryHtml or '' !!}
	{!! Form::open(['url' => action('PropertyController@postInquiry'), 'method' => 'POST', 'id' => 'inquiryform']) !!}

		{!! Form::hidden('selected_properties',
			old('selected_properties', (isset($property->id)? $property->id: null)),
			['id' => 'selected_properties']) !!}

		@if(isset($is_search))
			{!! Form::hidden('is_search', 'true', ['id' => 'is_search']) !!}
		@endif
		<div class="row">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="{{$classContainer or 'col-sm-6'}}">
				<input type="text" id="hp" name="hp" value="" />

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" id="first_name" name="name" placeholder="first name *" class="form-control" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" id="last_name" name="Lname" placeholder="last name" class="form-control" />
						</div>
					</div>
				</div>
				<div class="form-group">
					<input type="text" id="email" name="email" placeholder="email *" class="form-control" />
				</div>
				<div class="form-group">
					<input type="text" id="phone" name="phone" placeholder="phone" class="form-control" />
				</div>
				<div class="form-group">
					<select id="state" name="state" class="form-control select2">
						<option value=""></option>
						<option value="AL">Alabama</option>
						<option value="AK">Alaska</option>
						<option value="AZ">Arizona</option>
						<option value="AR">Arkansas</option>
						<option value="CA">California</option>
						<option value="CO">Colorado</option>
						<option value="CT">Connecticut</option>
						<option value="DE">Delaware</option>
						<option value="DC">District Of Columbia</option>
						<option value="FL">Florida</option>
						<option value="GA">Georgia</option>
						<option value="HI">Hawaii</option>
						<option value="ID">Idaho</option>
						<option value="IL">Illinois</option>
						<option value="IN">Indiana</option>
						<option value="IA">Iowa</option>
						<option value="KS">Kansas</option>
						<option value="KY">Kentucky</option>
						<option value="LA">Louisiana</option>
						<option value="ME">Maine</option>
						<option value="MD">Maryland</option>
						<option value="MA">Massachusetts</option>
						<option value="MI">Michigan</option>
						<option value="MN">Minnesota</option>
						<option value="MS">Mississippi</option>
						<option value="MO">Missouri</option>
						<option value="MT">Montana</option>
						<option value="NE">Nebraska</option>
						<option value="NV">Nevada</option>
						<option value="NH">New Hampshire</option>
						<option value="NJ">New Jersey</option>
						<option value="NM">New Mexico</option>
						<option value="NY">New York</option>
						<option value="NC">North Carolina</option>
						<option value="ND">North Dakota</option>
						<option value="OH">Ohio</option>
						<option value="OK">Oklahoma</option>
						<option value="OR">Oregon</option>
						<option value="PA">Pennsylvania</option>
						<option value="RI">Rhode Island</option>
						<option value="SC">South Carolina</option>
						<option value="SD">South Dakota</option>
						<option value="TN">Tennessee</option>
						<option value="TX">Texas</option>
						<option value="UT">Utah</option>
						<option value="VT">Vermont</option>
						<option value="VA">Virginia</option>
						<option value="WA">Washington</option>
						<option value="WV">West Virginia</option>
						<option value="WI">Wisconsin</option>
						<option value="WY">Wyoming</option>
					</select>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<div class="input-group datepicker-icon">
								<input type="text" id="arrival_date_2" name="arrival_date" placeholder="arrival" class="form-control datepicker" />
								<span class="input-group-btn">
									<button class="btn btn-default" type="button">
							        	<span class="glyphicon glyphicon-calendar date-button" ></span>
							        </button>
								</span>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<div class="input-group datepicker-icon">
								<input type="text" id="departure_date_2" name="departure_date" placeholder="departure" class="form-control datepicker" />
								<span class="input-group-btn">
									<button class="btn btn-default" type="button">
							        	<span class="glyphicon glyphicon-calendar date-button" ></span>
							        </button>
								</span>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" id="adults" name="adults" placeholder="# of adults" class="form-control" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" id="children" name="children" placeholder="# of children" class="form-control" />
						</div>
					</div>
				</div>
			</div>
			<div class="{{$classContainer or 'col-sm-6'}}">
				<div class="form-group">
					<textarea id="message_to_owner" class="form-control" rows="7" name="personalization" placeholder="Personalize your search. Provide travel dates."></textarea>
					<span class="help-block"><small>(Give rental owners/managers a few details about your {{$state->abbr}} getaway)</small></span>
				</div>

				<div class="checkbox">
					<label>
						<input type="checkbox" name="email_optin" checked="checked" value="yes" />
						Add me to the {{$state->seo_name}} email list to receive periodic rental specials &amp; news. I understand I can opt-out at any time.
					</label>
				</div>

				{!! Form::submit('Send Inquiry', ['class' => 'btn btn-contrast mt30', 'id' => 'btn-inquiry']) !!}
				<span class="help-block"><small>(Click Send Inquiry button 1 time.)</small></span>
			</div>
		</div>
	{!! Form::close() !!}
</div>

<script type="text/javascript">
			$(document).ready(function(){
				$('input[name="select_rental[]"]').on('click',function(){
					if($('input[name="select_rental[]"]:checked').length) {
						$('#optin').fadeIn();
						let userValue = [];
						$.each($('input[name="select_rental[]"]:checked'), function(){
							userValue.push($(this).val());
							$('#selected_properties').val(userValue);
						});
					} else {
						$('#optin').fadeOut();
					}
				});
				$('.clickable_hotspot').on('click',function(){
					window.location.href = $(this).find('.result_details_link').attr('href');
				});
			});
</script>
