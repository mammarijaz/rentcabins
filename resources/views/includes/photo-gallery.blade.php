<div class="gallery-container fancyframe">
	<div class="flexslider">
		<ul class="slides">
		@if(empty($property->property_photos))
			<li>
				<img src="/images/photo_placeholder.png" alt="{{$state->seo_name}}" />
			</li>
		@else
		@foreach($property->property_photos as $photo)
			@if(!empty($photo))
				<li>
					<img src="{{$photo->image_path}}" alt="@if($property->resort !== NULL){{$property->resort->name}}: @endif{{$property->name}}" />
					<div class="fancyframe"><center>{{$photo->description}}</center></div>
				</li>
			@endif
		@endforeach
		@endif
		</ul>
	</div>
</div>
