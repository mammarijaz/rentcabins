@extends('layouts.app', ['two_pack' => 'true'])
@section('content')
<script type="text/template" id="resort-result-template">
<div class="row">
    <div class="col-sm-9">

        <div class="result-container">
            <div class="row no-gutter clickable_hotspot">
                <div class="col-sm-5 col-md-4 text-center">
                    <div class="photo fancyframe">
                        <img src="" alt="" class="result_photo img-responsive">
                    </div>
                </div>
                <div class="col-sm-7 col-md-8 pt30 pb15">
                    <h2 class="result_resort mt0"></h2>
                    <h2 class="result_name mt0"></h2>
                    <h3 class="result_city mt0"></h3>

                    <ul class="list-inline">
                        <li class="result_bedrooms">- bedrooms,</span>
                        <li class="result_bathrooms">- bathrooms,</span>
                        <li class="result_max_capacity">sleeps -</span>
                    </ul>
                    <h4 class="pet_policy">Pet Text Here</h4>
                    <div class="row"> 
                        <div class="col-sm-6">
                        <span style="font-weight: bold;">BOOK DIRECT<br></span>
                        <span class="result_rates_daily">- daily</span>
                        <span class="result_rates_weekly">- weekly</span>
                        </div>
                        <div style="font-style: italic; opacity: 0.4" class="col-sm-6">
                        3rd party booking<br>
                        <span class="result_rates_daily_strike">- daily</span>
                        <span class="result_rates_weekly_strike">- weekly</span>
                        </div>
                    </div>
                    <p class="result_description" style="font-size: x-small"></p>

                    <p><a class="result_details_link" href="javascript:" data-href="/rental/">VIEW DETAILS and rent direct. No booking website fees. &raquo;</a></p>
                </div>
            </div>
        </div>

    </div>
</div>
</script>
<div id="resort_id" value="{{$resort->id}}">
<div class="container">

	<div class="row">
		<div class="col-sm-12">
			<div class="backtosearch">
				@if(!empty($resort->location->id))
					<a href='../location/{{$resort->location->slug}}' target='_top'>View all {{$resort->location->name}},{{$state->abbr}} cabins</a>
				@endif
			</div>
	</div>
</div>
<div class="container main-container">
	<div id="about" class="row property-banner">
		<div class="col-sm-7">
			<div class="">
				<h1>{{$resort->name}}</h1>
				<h2><span>A {{$resort->city}}, {{$state->name}}</span> resort</h2>
			</div>
		</div>
	</div>
	<div class="row hidden-xs">
		<div class="col-sm-12">

			<div id="nav-detail">
				<ul class="nav nav-pills">
					<li><a href="#about" title="About">About</a></li>
					<li><a href="#listings" title="Listings">Show Listings</a></li>
					<li><a href="#inquiry" title="Inquiry">Inquiry Form</a></li>
				</ul>
			</div>

			<hr class="dashed">

			<script type="text/javascript">
				$(document).ready(function(){
					// pixels from top when fixed menu is triggered
					$("#nav-detail").affix({
						offset: {
							top: 300
						}
					});
					// used to offset height of fixed nav at top
					$('body').scrollspy({
						target: '#nav-detail',
						offset: 90
					});
					// used to offset menu link of fixed nav at top
					var offsetValue = 85;
					$('#nav-detail a').click(function(event) {
						event.preventDefault();
						$($(this).attr('href'))[0].scrollIntoView();
						scrollBy(0, -offsetValue);
					});
				});
			</script>
		</div>
	</div>
	<div class="row">

		<!-- main content -->
		<div class="col-sm-12">

			<!-- about -->
			<div>
				<div class="row">
					<div id="page-search" class="col-sm-7 hidden-xs">
			            <div id="map-container" class="fancyframe">
			                @include('includes.map', ['resort_id' => $resort->id, 'isResort' => true])
			                <script type="text/javascript">
				                $.ajax({
									type: "GET",
									url: "/api/all-properties",
									data: {
										csrf: $('[name="csrf_token"]').attr('content'),
										resort_id: $('#resort_id').attr('value'),
									},
									dataType: 'json',
									success: function(data) {
										$.each(data,function(i, rlocation){
											if(rlocation.address_latitude != null) {
												mapPD[rlocation.public_id] = rlocation;
											}
										});
										googleMapUpdate(data);
										$('.result-number span').html(data.length);
										$('.result-number').show(0);
										for(var i=0;i<data.length;i++) {
											var template = $('#resort-result-template').html();
											template = $(template).clone();
											
											if(data[i]['resort']['name']!='') {$('.result_resort',template).html(data[i]['resort']['name']);} else {$('.result_resort',template).remove();}
											if(data[i].photos !== null && data[i].photos[0] != null)
											{
												$('.result_photo',template).attr('src', data[i].photos[0].image_path);
											}
											else
											{
												$('.result_photo',template).attr('src','/images/photo_placeholder.png');
											}
											if(data[i]['name']!='') {$('.result_name',template).html(data[i]['name']);} else {$('.result_name',template).remove();}
											if(data[i]['description']!='') {$('.result_description',template).html(data[i]['description']);} else {$('.result_description',template).remove();}
											if(data[i]['bedrooms']!='') {$('.result_bedrooms',template).html(data[i]['bedrooms']+' - bedrooms,');} else {$('.result_bedrooms',template).remove();}
											if(data[i]['bathrooms']!='') {$('.result_bathrooms',template).html(data[i]['bathrooms']+' - bathrooms,');} else {$('.result_bathrooms',template).remove();}
											if(data[i]['max_capacity']!='') {$('.result_max_capacity',template).html('sleeps - '+data[i]['max_capacity']);} else {$('.result_max_capacity',template).remove();}
											// if(data[i]['features'].indexOf('NO Pets Allowed') !== -1) {$('.result_no_pets_allowed',template).html('NO Pets Allowed');}
											// else if(data[i]['features'].indexOf('Pets Allowed') !== -1) {$('.result_no_pets_allowed',template).remove();}
											// else {$('.result_no_pets_allowed',template).remove();}
											if(data[i]['pet_policy'] != '') {$('.pet_policy', template).html(data[i]['pet_policy']);} else {$('.pet_policy',template).remove();}
											if(data[i]['on_waters']!='') {$('.result_on_waters',template).html('On '+data[i]['on_waters']);} else {$('.result_on_waters',template).remove();}
											if(data[i]['near_locations']!='') {$('.result_near_locations',template).html('Near '+data[i]['near_locations']);} else {$('.result_near_locations',template).remove();}
											if(data[i]['city']!='') {$('.result_city',template).html(data[i]['city']);} else {$('.result_city',template).remove();}
											if(data[i]['max_rates_daily']!='' && data[i]['max_rates_daily']!='0') {
												$('.result_rates_daily',template).html('$'+data[i]['min_rates_daily']+' - $'+data[i]['max_rates_daily']+' per night<br>').show();
												$('.result_rates_daily_strike',template).html('<span style="text-decoration: line-through;">$'+(parseFloat(data[i]['min_rates_daily']) * 1.3).toFixed(0) +' - $'+(parseFloat(data[i]['max_rates_daily']) * 1.3).toFixed(0)+' per night<br></span>').show();
											}
											else {
												if(data[i]['max_rates_weekly']=='' && data[i]['max_rates_weekly']=='0') {
													$('.result_rates_daily',template).html("").hide();
													$('.result_rates_daily_strike',template).html("").hide();
												} else {
													$('.result_rates_daily',template).html("Inquire for daily rates").show();
													$('.result_rates_daily_strike',template).html("Not available").show();
												}
											}
											if(data[i]['max_rates_weekly']!='' && data[i]['max_rates_weekly']!='0') {
												$('.result_rates_weekly',template).html('$'+data[i]['min_rates_weekly']+' - $'+data[i]['max_rates_weekly']+' per week<br>').show();
												$('.result_rates_weekly_strike',template).html('<span style="text-decoration: line-through;">$'+(parseFloat(data[i]['min_rates_weekly']) * 1.3).toFixed(0) +' - $'+(parseFloat(data[i]['max_rates_weekly']) * 1.3).toFixed(0)+' per week</span>').show();
											}
											else {
												if(data[i]['max_rates_daily']=='' && data[i]['max_rates_daily']=='0') { 
													$('.result_rates_weekly',template).html("Inquire for details").show();
													$('.result_rates_weekly_strike',template).html("Not available").show();
												} else {
													$('.result_rates_weekly',template).html("Inquire for weekly rates").show();
													$('.result_rates_weekly_strike',template).html("Not available").show();
												}
											}
											$('input[name="select_rental[]"]',template).val(data[i]['pkid']);
											$('.result_details_link',template).attr('href', '/rental/'+data[i]['public_id']);
											$('#results').append(template);
										}
									},
									error: function(jqXhr, status) {
										console.log(jqXhr);
										console.log(status);
									}
								});
							</script>
			            </div>
			        </div>
					<div class="col-sm-5">
						<a href="#inquiry" style="width: 100%; margin-bottom: 10px; line-height: 2;" class="btn btn-contrast">INQUIRE NOW. BOOK DIRECT. SAVE.</a>
						<ul class="description-list list-unstyled">
						<h2>About {{$resort->name}}:</h2>
						<p>
							@if(!empty($resort->address)){{$resort->address}}<br>@endif
							@if(!empty($resort->city)){{$resort->city}}, {{$state->name}} {{$resort->zipcode}}@endif
						</p>

						<p>@if(!empty($resort->phone))Phone #: {{$resort->phone}}@endif</p>

						<p>@if(!empty($resort->website))<a href="{{$resort->website}}">VISIT {{$resort->name}} WEBSITE</a>@endif</p>
						<p>@if(!empty($resort->description)){{nl2br(strip_tags($resort->description))}}@endif</p>						
					</div>
				</div>
			</div>
			<br><br>
			<div id="listings">
				<p class="result-number noshow"><span>0</span> vacation rental(s) at {{$resort->name}}:</p>
				<div id="form-search-results" onsubmit="return false;">
	                <div id="results"></div>
	            </div>
        	</div>
			<div id="inquiry" class="rental-inquiry">
				<br><br>
				<h3 class="mt0">Inquire and book with {{$state->abbr}} rental owners or managers to get the lowest price online.</h3>

					@include('includes.inquiry-form', ['classContainer' => 'col-sm-12'])
				</div>
			</div>

			<div class="hidden-xs hidden-sm">
				<!-- ad -->
				<script type="text/javascript">
					<!--
					google_ad_client = "ca-pub-8772899038103543";
					/* RentWisCabinsDetailsPg3 */
					google_ad_slot = "5230219708";
					google_ad_width = 468;
					google_ad_height = 60;
					//-->
				</script>
				<script type="text/javascript" src="//pagead2.googlesyndication.com/pagead/show_ads.js"></script>
			</div>
		</div>
		<!-- /main content -->

	</div>
</div>
@endsection
