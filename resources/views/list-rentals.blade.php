@extends('layouts.app', ['two_pack' => 'false'])
@section('content')
<div style="padding-bottom: 40px" class ="container">
	<div class="row">
        <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">

            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <div id="seo" class="text-center">
						<h1>List your {{$state->name}} vacation rentals. We offer a more relaxed approach to promoting rentals that pays off for everyone. No booking commissions or traveler fees.</h1>

						<h2>We deliver inquiries, you complete the booking</h2>
					</div>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="container main-container">

	<div class="row">
		<div class="col-sm-8 col-sm-push-4">
			@include('partials.featured-property', ['featuredProeprty' => $featuredProperty])

			<h1>List your {{$state->name}} vacation rentals here. Introductory Special! Second Year FREE! No commissions. No traveler fees.</h1>
			<ul>
				<li style="font-weight: bold">LISTINGS START AT $128 (FOR TWO YEARS!)</li>
				<li>We initially set up your listing(s) for NO additional fee.</ll>
			</ul>
			<p>We say NO to booking commissions and traveler fees. We YES to more exposure and direct bookings for our members. Control the conversation! We provide a clear path so YOUR renters can talk to you. It's time to diversify your advertising and list your Kentucky vacation rentals for a low annual fee. We deliver quality inquiries and you control the entire inquiry through guest check-out experience. Your listing(s) link to your property website and social media pages - we don't hide your brand, we promote it.</p>
			<p>Ready for more direct bookings? JOIN OUR TEAM TODAY</p>
			<hr>
			@include('includes.rental-form')
			<hr>
			<p>The JM Creative Group has been helping rental owners and property managers become less reliant on third-party booking websites since 2009. Our roll out of state-specific, vacation rental websites started with <a href="//RentWisconsinCabins.com">RentWisconsinCabins.com</a>. In 2014, we took our in-demand service on the road and introduced <a href="//RentMinnesotaCabins.com">RentMinnesotaCabins.com</a> and <a href="//RentMichiganCabins.com">RentMichiganCabins.com</a>. We recently launched websites for Tennessee, New York and Montana too.</p>
			<p>List your {{$state->name}} vacation rentals for a LOW ANNUAL FEE. Renter inquiries go directly to you and they include the renter’s name, email address, phone number, travel dates, personalized message, and much more.</p>
			<p>Travelers demand HUNDREDS of choices, so it's DISCOUNT SEASON. Don’t let our low annual rates fool you, we are a VALUE SUPERSTAR for vacation rental owners and property managers in 6 other states.</p>
			<p style="font-weight: bold">Fast growing book direct channel</p>
			<ul>
				<li>7 states</li>
				<li>1,700+ rental listings</li>
			</ul>
			<p style="font-weight: bold">Organic renter traffic increases 20% every year</p>
			<ul>
				<li>5.7 million+ website views</li>
			</ul>
			<p style="font-weight: bold">A loyal social following</p>
			<ul>
				<li>35,000+ (unpaid) Facebook Likes</li>
			</ul>
			<p>At {{$state->seo_name}} renters connect, plan and book directly with {{$state->name}} cabin rental owners or local property managers. We don’t dip in to your booking dollars or charge YOUR renters additional service fees. Boosting your online presence and delivering quality inquiries is our priority. LOW ANNUAL FEE includes the initial set up your listing(s) too.</p>
			<p>Renters love the tools we give them to find {{$state->name}} cabin rentals that meet their needs: The Narrow Your Search form it tied to an Interactive Google Map which makes the search easy and fun. You can display up to 3 YouTube videos on your listing(s). We put the map pins on the rooftops so renters can see exactly where each {{$state->name}} vacation rental is located. Property address display is optional.</p>
			<p>{{$state->abbr}} rental owners and property managers use a property dashboard to make updates.</p>
			<ul>
				<li>Run rental specials on your details pages (optional)</li>
				<li>Select amenities unique to your property</li>
				<li>Prominately display a WEEEKLY ONLY policy (optional)</li>
				<li>Prominately display your pet policy</li>
				<li>Use an availability calendar (optional)</li>
			</ul>
			<p>Shortly after sign up, we will call for a quick "welcome". If you have property content online, we will use it to initially set up your listing(s) on {{$state->seo}}. Once payment is recieved, we activate your listing(s). We have FREE Text Alerts so you can reply to potential renters quickly</p>
			<p style="font-weight: bold">ATTENTION MULTI-CABIN RESORT OWNERS AND PROPERTY MANAGERS:</p>
			<p>Each rental unit can have its own listing. WHY? Each rental offers different availability, views, and amenities. Multiple listings means more exposure and inquiries too.</p>
			<h2>LOW ANNUAL FEE + BOOK DIRECT + 2 YEARS FOR THE PRICE OF ONE = A NO-BRAINER MARKETING ADD-ON</h2>
			<hr>
			<p>
			    1 rental unit listing $128 FOR TWO YEARS!<br>
			    2 rental unit listings $158 FOR TWO YEARS!<br>
			    3 rental unit listings $188 FOR TWO YEARS!<br>
			    4 rental unit listings $218 FOR TWO YEARS!<br>
			    5 rental unit listings $248 FOR TWO YEARS!<br>
			    6 rental unit listings $278 FOR TWO YEARS!<br>
			    7 rental unit listings $308 FOR TWO YEARS!<br>
			    8 rental unit listings $338 FOR TWO YEARS!<br>
			    9 rental unit listings $368 FOR TWO YEARS!<br>
			    10 rental unit listings $398 FOR TWO YEARS!<br>
			    11 rental unit listings $428 FOR TWO YEARS!<br>
			    12 rental unit listings $458 FOR TWO YEARS!<br>
			    13 rental unit listings $488 FOR TWO YEARS!<br>
			    14 rental unit listings $518 FOR TWO YEARS!<br>
			    15 rental unit listings $548 FOR TWO YEARS!<br>
			    16 rental unit listings $578 FOR TWO YEARS!<br>
			    17 rental unit listings $608 FOR TWO YEARS!<br>
			    18 rental unit listings $638 FOR TWO YEARS!<br>
			    19 rental unit listings $668 FOR TWO YEARS!<br>
			    20 rental unit listings $698 FOR TWO YEARS!<br>
			    Over 20 rental units? Call us for your discounted rate.
			</p>
			<hr>
			<p>Payment options: See your listing(s) before you send payment.</p>
			<ul>
				<li>Check</li>
				<li>Credit Card/Paypal</li>
			</ul>
			<p>Do you use email spam-blocking tools? To ensure you receive our email communications, please add info@rentkentuckycabins.com to your list of safe addresses. It is your responsibility to ensure that your email program allows inquiries from us. All rental contracts are between renters and owners or managers. RENTKENTUCKYCABINS.COM does not take a booking commission. RENTKENTUCKYCABINS.COM is your proud brand-building partner. If you have questions, please contact us at 608-850-4242.</p>
			<p>As a general rule, the larger the population base of the area in which your vacation rental is located, the more likely there will be additional county and or city regulations you will have to follow. Make sure you check the rental laws. To learn more about how rental laws may affect your property, start your due diligence here:</p>
		</div>
		<div class="col-sm-4 col-sm-pull-8">
			{{$settings->sidebar}}
		</div>
	</div>

</div>
@endsection
