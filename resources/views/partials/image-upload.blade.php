@if(isset($photos))
	@foreach($photos as $photo)
		<div class="photoUpload panel">
			<img src='{{$photo->image_path}}' style="max-height: 100px; max-width: 100px;">
			{!! Form::label('order['.$photo->id.']', 'Photo Order') !!}
			{!! Form::number('order['.$photo->id.']', old('order['.$photo->id.']',$photo->photo_order)) !!}
			{!! Form::label('existingDescription['.$photo->id.']', 'Photo Description') !!}
			{!! Form::text('existingDescription['.$photo->id.']', old('existingDescription['.$photo->id.']', $photo->description)) !!}
			<button class ="small alert button" name="submitbutton" value="{{$photo->id}}" type="submit">Delete</button>
		</div>
	@endforeach
@endif
