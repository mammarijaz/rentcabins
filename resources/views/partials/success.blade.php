@if (session('success'))

    <div class="alert-box success">
        {{ session('success') }}
         <a href="#" class="close">&times;</a>
    </div>
@endif
