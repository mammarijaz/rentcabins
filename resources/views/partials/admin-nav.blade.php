<div class="row">
	<div class="seven columns" style="padding:20px;">
		@if(Auth::user()->isAdmin())
			<strong>{{ $state->seo_name }} - Admin</strong>
		@else
			<strong>Don’t Let Your Property Manager Time-Out. Save Your Updates Often.</strong>
		@endif
	</div>
	<div class="five columns text-right" style="padding:20px;">
		Logged In As {{ Auth::user()->name }} ( <a href="/logout">Log Out</a> )
	</div>
</div>
<div class="row" style="background-color: #514a42">
    <div class="twelve columns text-center" style="padding:20px;">
		<a href="/"><img src="{{ $state->main_logo }}" alt="{{ $state->seo_name }}" /></a>
	</div>
</div>
<div class="row" style="background-color: #514a42">
	<div class="twelve columns">

		<dl class="tabs mobile" id="rwc-top-tabs" style="margin-bottom: 0;">
			<dd class="{{ Request::is('admin')? 'active' : '' }}">{!! link_to('admin', 'Home') !!}</dd>
			<dd class="{{ Request::is('admin/properties*')? 'active' : '' }}">{!! link_to('admin/properties', 'Property') !!}</dd>
			@if(Auth::user()->isAdmin())
				<dd class="{{ Request::is('admin/inquiries*')? 'active' : '' }}">{!! link_to('admin/inquiries', 'Inquiries') !!}</dd>
				<dd class="{{ Request::is('admin/leads*')? 'active' : '' }}">
					{!! link_to('admin/leads', 'Optins') !!}
				</dd>
				<dd class="{{ Request::is('admin/pages*')? 'active' : '' }}">
					{!! link_to_action('Admin\PagesController@index', 'Pages') !!}
				</dd>
				<dd class="{{ Request::is('admin/users*')? 'active' : '' }}">
					{!! link_to_action('Admin\UsersController@index', 'Users') !!}
				</dd>
				<dd class="{{ Request::is('admin/locations*')? 'active' : '' }}">
					{!! link_to_action('Admin\LocationsController@index', 'Locations') !!}
				</dd>
				<dd class="{{ Request::is('admin/resorts*')? 'active' : '' }}">
					{!! link_to_action('Admin\ResortsController@index', 'Resorts/PMs') !!}
				</dd>
				<dd class="{{ Request::is('admin/regions*')? 'active' : '' }}">
					{!! link_to_action('Admin\RegionsController@index', 'Regions') !!}
				</dd>
				<dd class="{{ Request::is('admin/options*')? 'active' : '' }}">
					{!! link_to_action('Admin\AdminController@getOptions', 'Global') !!}
				</dd>
				<dd class="{{ Request::is('admin/settings')? 'active' : '' }}">
					{!! link_to_action('Admin\AdminController@getSettings', 'State') !!}
				</dd>
			@else
				<dd class="{{ Request::is('admin/inquiries*')? 'active' : '' }}">{!! link_to('admin/inquiries?filter_fkid_users='.Auth::user()->id, 'Inquiries') !!}</dd>
				<dd class="{{ Request::is('admin/advertising-opportunities')? 'active' : '' }}">
					{!! link_to('/admin/advertising-opportunities', 'Advertising Opportunities') !!}
				</dd>
				<dd class="{{ Request::is('admin/faq')? 'active' : '' }}">
					{!! link_to('/admin/faq', 'FAQ') !!}
				</dd>
			@endif
		</dl>

	</div>
</div>
