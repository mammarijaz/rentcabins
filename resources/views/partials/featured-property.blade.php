@if(!empty($featuredProperty))
<div class="row">
    <div class="col-sm-12">
       <div id="featured-property" class="mb40">
            <h3>Visit our Featured Property</h3>
            <div class="box">
                <div class="col-xs-12 col-md-5">
                    @if($featuredProperty->featured_photo)
                        <img src="{{$featuredProperty->featured_photo}}" alt="Property Logo" class="img-responsive"  />
                    @endif
                </div>
                <div class="col-xs-12 col-md-7">
                    <p><strong>{{$featuredProperty->name}}</strong></p>
                    {!! $featuredProperty->featured_description !!}
                    <p class="viewdetails"><a href="{{$featuredProperty->listing_url}}" target="_blank" onClick="_gaq.push(['_trackEvent', 'Featured Property', 'View', {{$featuredProperty->name}}]);">Visit Featured Property Website &raquo;</a></p>
                </div>
            </div>
       </div>
    </div>
</div>
@endif