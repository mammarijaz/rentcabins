@extends('layouts.app', ['two_pack' => 'true'])
@section('content');
<div class="container">
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <div id="seo" class="text-center">
					<h1>{!! $location->header_text !!}</h1>
					</div>
					<div id="use" class="hidden-xs text-center">
					<h2></h2>
					</div>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="container main-container">
	<div id="about" class="row property-banner">
		<div class="col-sm-7">
			<h1>{{  $location->name }}, {{ $state->name }} cabins and resort rentals</h1>
		</div>
		<div class="col-sm-5">
			<div class="sharethis pull-right">
				<h2>Share this {{$state->abbr}} town page</h2>
				<!-- Go to www.addthis.com/dashboard to customize your tools -->
			<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55882f01094d4b2d" async="async"></script>

			<!-- Go to www.addthis.com/dashboard to customize your tools -->
			<div class="addthis_sharing_toolbox"></div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-8 col-sm-push-4">

			<p class="result-number"><span>{{count($location->properties)}}</span>  vacation rental(s) near this location...</p>

			<form id="searchform">
				@foreach($location->properties as $property)
				<div class="result-container">
					<div class="inquiry-select">
						<label>
							<input class="result_select_rental" type="checkbox" name="select_rental[]" value="{{$property->id}}"> select
						</label>
						<button type="button" class="btn btn-icon btn-xs btn-tooltip" data-toggle="tooltip" data-placement="auto left" title="Send 1 inquiry to multiple rental owners. SELECT this listing, PLUS your other favorite listings. An Inquiry form will appear below the last listing. Otherwise, send inquiries from the details pages."><span class="glyphicon glyphicon-question-sign"></span></button>
					</div>
					{{$property->photos}}
					<div class="row no-gutter clickable_hotspot">
						<div class="col-sm-5 col-md-4 text-center">
							<div class="photo fancyframe">
								<img src="
									@if(!empty($property->property_photos)){{$property->property_photos->first()['image_path']}}
									@else/images/photo_placeholder.png
									@endif" alt="{{$property->name}}" class="result_photo img-responsive">
							</div>
						</div>
						<div class="col-sm-7 col-md-8 pt30 pb15">

							@if(!is_null($property->resort))<h2 class="result_resort mt0">{{$property->resort->name}}</h2>@endif
							<h2 class="result_name mt0">{{$property->name}}</h2>
							<h3 class="result_city mt0">{{$property->city}}, {{$state->abbr}}</h3>

							<h4 class="pet_policy">{{$property->pet_policy}}</h4>
		                    <div class="row"> 
		                        <div class="col-sm-6">
		                        <span style="font-weight: bold;">BOOK DIRECT<br></span>
		                        
								@if(!empty($property->min_rates_daily))<span>${{number_format(strip_tags($property->min_rates_daily), 0, '.', '')}} - 
									@endif${{number_format(strip_tags($property->max_rates_daily), 0, '.', '')}} per night</span>
								<br>
								@if(!empty($property->min_rates_weekly))<span>${{number_format(strip_tags($property->min_rates_weekly), 0, '.', '')}} - 
									@endif${{number_format(strip_tags($property->max_rates_weekly), 0, '.', '')}} per week</span>
		                        </div>
		                        <div style="font-style: italic; opacity: 0.4" class="col-sm-6">
		                        3rd party booking<br>
		                        @if(!empty($property->min_rates_daily))<span>${{1.3 * number_format(strip_tags($property->min_rates_daily), 0, '.', '')}} - 
									@endif${{1.3 * number_format(strip_tags($property->max_rates_daily), 0, '.', '')}} per night</span>
								<br>
								@if(!empty($property->min_rates_weekly))<span>${{1.3 * number_format(strip_tags($property->min_rates_weekly), 0, '.', '')}} - 
									@endif${{1.3 * number_format(strip_tags($property->max_rates_weekly), 0, '.', '')}} per week</span>
		                        </div>
		                    </div>

							<div class="row">
								<div class="col-md-9">
									<!-- <ul>
										@if(in_array('Pets Allowed', $property->amenities()->lists('title')->toArray()) !== false)
										<li class="result_no_pets_allowed">Pet friendly</li>
										@else
										<li class="result_no_pets_allowed">Sorry, no pets</li>
										@endif
									</ul> -->
									<!-- <div class="row">
										<div class="col-sm-6">
											<p><span style="font-weight: bold;">BOOK DIRECT</span><br>
												@if(!empty($property->min_rates_daily))
												@if(!empty($property->min_rates_daily))${{number_format(strip_tags($property->min_rates_daily), 0, '.', '')}} - 
												@endif${{number_format(strip_tags($property->max_rates_daily), 0, '.', '')}} per night<br>
												@if(!empty($property->min_rates_weekly))${{number_format(strip_tags($property->min_rates_weekly), 0, '.', '')}} - 
													@endif${{number_format(strip_tags($property->max_rates_weekly), 0, '.', '')}} per week<br>
												
											</p>
											</div>
											<div style="font-style: italic; opacity: 0.4" class="col-sm-6">
											3rd party booking
											<p>@if(!empty($property->min_rates_daily))
												<span style="text-decoration: line-through;">${{number_format(strip_tags($property->min_rates_daily) * 1.3, 0, '.', '')}} - 
												@endif ${{number_format(strip_tags($property->max_rates_daily) * 1.3, 0, '.', '')}}</span> per night<br>
												@if(!empty($property->min_rates_weekly))<span style="text-decoration: line-through;">${{number_format(strip_tags($property->min_rates_weekly) * 1.3, 0, '.', '')}} - 
												@endif ${{number_format(strip_tags($property->max_rates_weekly)* 1.3, 0, '.', '')}}</span> per week
												@else No information available
												@endif
											</p>
										</div>
									</div> -->
								</div>

							</div>

							<p class="result_description"><small>{{substr($property->description, 0 , 120) . '...'}}</small></p>

							<p><a class="result_details_link" href="/rental/{{$property->public_id}}">VIEW DETAILS and rent direct. No booking website fees. &raquo;</a></p>
						</div>
					</div>
				</div>
				@endforeach
			</form>

			<div class="inquiry-form-container">
				@include('includes.inquiry-form')
			</div>

		</div>
		<div class="col-sm-4 col-sm-pull-8">
			<div id="events-links">
				<h2><span>Things to do during "cabin time" in</span> {{$location->name}}</h2>
				{!! $location->description !!}
				<span class="subcopy">
					{!! $location->features !!}
				</span>
			</div>
			{!! $settings->home_sidebar !!}
			<div class="text-center hidden-xs">
				<script type="text/javascript"><!--
					google_ad_client = "ca-pub-8772899038103543";
					/* RentWisCabins_ad */
					google_ad_slot = "1586069301";
					google_ad_width = 160;
					google_ad_height = 600;
					//-->
				</script>
				<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js"></script>
			</div>
		</div>
	</div>
</div>
@endsection
