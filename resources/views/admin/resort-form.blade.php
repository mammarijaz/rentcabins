@extends('layouts.admin')
@section('content')
@include('partials.admin-nav')
<div class="row rwc-module">
	<div class="twelve columns">
		<div class="rwc-wrapper">
			<h3>{{isset($resort->id)?'Edit':'Add'}} Resort or Property Management Company</h3>
			@include('partials.errors')
			@include('partials.success')

			@if(isset($resort->id))
			{!! Form::model($resort, ['url' => action('Admin\ResortsController@update', $resort->id), 'method' => 'put', 'files' => true]) !!}
			@else
			{!! Form::open(['url' => action('Admin\ResortsController@store'), 'method' => 'post', 'files' => true]) !!}
			@endif
				{!! Form::hidden('state_id', $state->id) !!}
				{!! Form::label('name', 'Name', ['class' => 'required']) !!}
				{!! Form::text('name') !!}
				{!! Form::label('address', 'Address', ['class' => 'required']) !!}
				{!! Form::text('address') !!}
				<div class="row">
					<div class="four columns">
						{!! Form::label('city', 'City', ['class' => 'required']) !!}
						{!! Form::text('city') !!}
					</div>
					<div class="four columns">
						{!! Form::label('state', 'State', ['class' => 'required']) !!}
						{!! Form::text('state') !!}
					</div>
					<div class="four columns">
						{!! Form::label('zipcode', 'Zip', ['class' => 'required']) !!}
						{!! Form::text('zipcode') !!}
					</div>
				</div>
				{!! Form::label('phone', 'Phone Number') !!}
				{!! Form::text('phone')!!}
				<br><br>
				{!! Form::label('website', 'Website') !!}
				{!! Form::text('website') !!}
				<br><br>
				{!! Form::label('location_id', 'Related Location') !!}
				{!! Form::select('location_id', $locations, null, ['placeholder' => 'Choose a nearby location']) !!}
				<br><br>
				{!! Form::label('user_id', 'Owner') !!}
				{!! Form::select('user_id', $owners, null, ['placeholder' => 'Choose an Owner']) !!}
				<br><br>
				{!! Form::label('description', 'Description') !!}
				{!! Form::textarea('description', old('description', $resort->description), ['class'=>"tinymce"]) !!}
				<br><br>
				<div id="photosSection">
					<h3>Photos</h3>
					@include('partials.image-upload')
					{!! Form::label('image[]', 'Add an Image') !!}
					{!! Form::file('image[]') !!}
					{!! Form::label('newImageDescription[0]', 'Image Description') !!}
					{!! Form::text('newImageDescription[0]', '', array('class' => 'imageDescription')) !!}
					<br>
				</div>
				<button type="button" onclick="addImageElement()">Add Another Image</button>
				<hr />
				<div class="panel text-center">
					{!! Form::submit('Save', ['class' => 'success button', 'name' => 'submitbutton', 'value' => 'Update']) !!}
					{!! link_to_action('Admin\ResortsController@index', 'Cancel',[], ['class' => 'secondary button']) !!}
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection
@push('scripts')
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>tinymce.init({ selector:'.tinymce' });</script>
<script type="text/javascript">
	function addImageElement(){
		linebreak = document.createElement("br");
		input = document.createElement("input");
		description = document.createElement("input");
		input.type = "file";
		input.name = "image[]";
		description.type = "text";
		description.className = "imageDescription";
		description.name = "newImageDescription["+ (document.querySelectorAll('.imageDescription').length) +"]";
		document.getElementById('photosSection').append(input);
		document.getElementById('photosSection').append(description);
		document.getElementById('photosSection').append(linebreak);	
	}
</script>
@endpush
