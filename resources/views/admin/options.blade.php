@extends('layouts.admin')
@section('content')
@include('partials.admin-nav')
<div class="row rwc-module">
	<div class="twelve columns">
		<div class="rwc-wrapper">
			<h3>Global Options</h3>
			<hr/>
			@include('partials.errors')
			@include('partials.success')
			<dl class="tabs mobile">
				<dd class="active"><a href="#tab0">Categories</a></dd>
				<dd><a href="#tab1">Amenities</a></dd>
				<dd><a href="#tab2">Settings</a></dd>
				<dd><a href="#tab3">Styles</a></dd>
			</dl>
			<ul class="tabs-content">
				<li class="active" id="tab0Tab">
					<h5>Categories</h5>
					<p>
					{!! Form::open(['url' => action('Admin\AmenitiesCategoryController@store')]) !!}
						{!! Form::hidden('state_id', $state->id) !!}
						{!! Form::text('title', null, ['placeholder' => 'New Category']) !!}
						{!! Form::submit('Add', ['class' => 'button']) !!}
					{!! Form::close() !!}
					</p>

					<table width="100%" border="0" cellspacing="1" cellpadding="0">
						<thead>
							<tr>
								<th width="70%" align="left" valign="top">Title</th>
								<th width="15%" align="center" valign="top">Edit</th>
								<th width="15%" align="center" valign="top">DELETE</th>
							</tr>
						</thead>
						<tbody>
							@foreach($categories as $category)
							<!-- Modal for category -->
							<div id="category-modal-{{$category->id}}" class="white-popup-block mfp-hide">
								<button title="Close (Esc)" type="button" class="mfp-close" style="color: grey">×</button>
								<h3>Edit Category</h3>
								{!!Form::open(['url' => action('Admin\AdminController@postEditSetting', $category->id)]) !!}
									{!! Form::hidden('settingType', 'category') !!}
									{!! Form::label('newName', 'Category Name:') !!}
									{!! Form::text('newName', $category->title) !!}
									{!! Form::label('newOrder', 'Category Order:') !!}
									{!! Form::text('newOrder', $category->category_order) !!}
									{!! Form::submit('Save', ['class' => 'button success']) !!}
								{!! Form::close() !!}
							</div>
							<tr>
								<td align="left" valign="top" class="tablecell">{{$category->category_order}}. {{ $category->title }}</td>
								<td align="left" valign="top" class="tablecell"><a class="popup-modal button small" href="#category-modal-{{$category->id}}">Edit</a></td>
								<td align="center" valign="top" class="tablecell">
									<form action="{{action('Admin\AmenitiesCategoryController@destroy', $category->id)}}" method="POST">
										{{ csrf_field() }}
										<input type="hidden" name="_method" value="DELETE">
										<input type="submit" value="Delete" class="small alert button"
											onclick="if(confirm('Are you sure you want to delete this category?')) {return true;} else {return false;}">
									</form>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</li>
				<li id="tab1Tab">
					<h5>Amenities</h5>
					<p>
					{!! Form::open(['url' => action('Admin\AmenitiesController@store')]) !!}
						{!! Form::hidden('state_id', $state->id) !!}
						{!! Form::select('amenity_category_id', $categories->pluck('title','id'), null, array('class' => 'form-control')) !!}
						{!! Form::text('title', null, ['placeholder' => 'New Amenity']) !!}
						{!! Form::submit('Add', ['class' => 'button']) !!}
					{!! Form::close() !!}
					</p>
							@foreach($categories as $category)
								<div class="amenitycategory">
									<h3>{{$category->title}}</h3>
									<table width="100%" border="0" cellspacing="1" cellpadding="0">
										<thead>
											<tr>
												<th width="55%" align="left" valign="top">Title</th>
												<th width="15%" align="center" valign="top">Edit</th>
												<th width="15%" align="center" valign="top">Is Searchable?</th>
												<th width="15%" align="center" valign="top">DELETE</th>
											</tr>
										</thead>
										<tbody>
											@foreach($category->amenities as $amenity)
												<!-- Modal for amenity -->
												<div id="amenity-modal-{{$amenity->id}}" class="white-popup-block mfp-hide">
													<button title="Close (Esc)" type="button" class="mfp-close" style="color: grey">×</button>
													<h3>Edit Amenity</h3>
													{!!Form::open(['url' => action('Admin\AdminController@postEditSetting', $amenity->id)]) !!}
														{!! Form::hidden('settingType', 'amenity') !!}
														{!! Form::text('newName', $amenity->title) !!}
														{!! Form::submit('Save', ['class' => 'button success']) !!}
													{!! Form::close() !!}
												</div>
												<tr>
													<td align="left" valign="top" class="tablecell">{{ $amenity->title }}</td>
													<td align="left" valign="top" class="tablecell"><a class="popup-modal button small" href="#amenity-modal-{{$amenity->id}}">Edit</a></td>
													<td align="center" valign="top" class="tablecell">
														<form action="{{action('Admin\AdminController@postToggleAmenityFilter', $amenity->id)}}" method="POST">
															{{ csrf_field() }}
															<input type="submit" value="{{$amenity->is_searchable ? 'Remove from Filter' : 'Add to Filter' }}">
														</form>
													</td>
													<td align="center" valign="top" class="tablecell">
														<form action="{{action('Admin\AmenitiesController@destroy', $amenity->id)}}" method="POST">
															{{ csrf_field() }}
															<input type="hidden" name="_method" value="DELETE">
															<input type="submit" value="Delete" class="small alert button"
																onclick="if(confirm('Are you sure you want to delete this amenity?')) {return true;} else {return false;}">
														</form>
													</td>
												</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							@endforeach
				</li>
				<li id="tab2Tab">
					<h5>Settings</h5>
					<p>
						{!! Form::open(['url' => action('Admin\PropertySettingsController@store')]) !!}
						{!! Form::hidden('state_id', $state->id) !!}
						{!! Form::text('setting', null, ['placeholder' => 'New Setting']) !!}
						{!! Form::submit('Add', ['class' => 'button']) !!}
						{!! Form::close() !!}
					</p>

					<table width="100%" border="0" cellspacing="1" cellpadding="0">
						<thead>
							<tr>
								<th width="55%" align="left" valign="top">Title</th>
								<th width="15%" align="center" valign="top">Edit</th>
								<th width="15%" align="center" valign="top">Is Searchable?</th>
								<th width="15%" align="center" valign="top">DELETE</th>
							</tr>
						</thead>
						<tbody>
							@foreach($propertySettings as $setting)
							<!-- Modal for setting -->
							<div id="setting-modal-{{$setting->id}}" class="white-popup-block mfp-hide">
								<button title="Close (Esc)" type="button" class="mfp-close" style="color: grey">×</button>
								<h3>Edit Setting</h3>
								{!!Form::open(['url' => action('Admin\AdminController@postEditSetting', $setting->id)]) !!}
									{!! Form::hidden('settingType', 'setting') !!}
									{!! Form::text('newName', $setting->setting) !!}
									{!! Form::submit('Save', ['class' => 'button success']) !!}
								{!! Form::close() !!}
							</div>
							<tr>
								<td align="left" valign="top" class="tablecell">{{ $setting->setting }}</td>
								<td align="left" valign="top" class="tablecell"><a class="popup-modal button small" href="#setting-modal-{{$setting->id}}">Edit</a></td>
								<td align="center" valign="top" class="tablecell">
									<form action="{{action('Admin\AdminController@postToggleSettingsFilter', $setting->id)}}" method="POST">
										{{ csrf_field() }}
										<input type="submit" value="{{$setting->is_searchable ? 'Remove from Filter' : 'Add to Filter' }}">
									</form>
								</td>
								<td align="center" valign="top" class="tablecell">
									<form action="{{action('Admin\PropertySettingsController@destroy', $setting->id)}}" method="POST">
										{{ csrf_field() }}
										<input type="hidden" name="_method" value="DELETE">
										<input type="submit" value="Delete" class="small alert button"
											onclick="if(confirm('Are you sure you want to delete this setting?')) {return true;} else {return false;}">
									</form>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</li>
				<li id="tab3Tab">
					<h5>Styles</h5>
					<p>
						{!! Form::open(['url' => action('Admin\PropertyStylesController@store')]) !!}
						{!! Form::hidden('state_id', $state->id) !!}
						{!! Form::text('style', null, ['placeholder' => 'New Style']) !!}
						{!! Form::submit('Add', ['class' => 'button']) !!}
					{!! Form::close() !!}
					</p>

					<table width="100%" border="0" cellspacing="1" cellpadding="0">
						<thead>
							<tr>
								<th width="55%" align="left" valign="top">Title</th>
								<th width="15%" align="center" valign="top">Edit</th>
								<th width="15%" align="center" valign="top">Is Searchable?</th>
								<th width="15%" align="center" valign="top">DELETE</th>
							</tr>
						</thead>
						<tbody>
							@foreach($propertyStyles as $style)
							<!-- Modal for style -->
							<div id="style-modal-{{$style->id}}" class="white-popup-block mfp-hide">
								<button title="Close (Esc)" type="button" class="mfp-close" style="color: grey">×</button>
								<h3>Edit Style</h3>
								{!!Form::open(['url' => action('Admin\AdminController@postEditSetting', $style->id)]) !!}
									{!! Form::hidden('settingType', 'style') !!}
									{!! Form::text('newName', $style->style) !!}
									{!! Form::submit('Save', ['class' => 'button success']) !!}
								{!! Form::close() !!}
							</div>
							<tr>
								<td align="left" valign="top" class="tablecell">{{ $style->style }}</td>
								<td align="left" valign="top" class="tablecell"><a class="popup-modal button small" href="#style-modal-{{$style->id}}">Edit</a></td>
								<td align="center" valign="top" class="tablecell">
									<form action="{{action('Admin\AdminController@postToggleStylesFilter', $style->id)}}" method="POST">
										{{ csrf_field() }}	
										<input type="submit" value="{{$style->is_searchable ? 'Remove from Filter' : 'Add to Filter' }}">
									</form>
								</td>
								<td align="center" valign="top" class="tablecell">
									<form action="{{action('Admin\PropertyStylesController@destroy', $style->id)}}" method="POST">
										{{ csrf_field() }}
										<input type="hidden" name="_method" value="DELETE">
										<input type="submit" value="Delete" class="small alert button"
											onclick="if(confirm('Are you sure you want to delete this style?')) {return true;} else {return false;}">
									</form>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</li>
			</ul>
		</div>
	</div>
</div>
@endsection
