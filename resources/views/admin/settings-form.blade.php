@extends('layouts.admin')
@section('content')
@include('partials.admin-nav')
<div class="row rwc-module" id="settings-form">
	<div class="twelve columns">
		<div class="rwc-wrapper">
			<h3>{{isset($settings->id)?'Edit':'Add'}} State Settings</h3>
			@include('partials.errors')
			@include('partials.success')
			<dl class="tabs mobile">
				<dd class="active"><a href="#tab0">General</a></dd>
				<dd><a href="#tab1">Homepage</a></dd>
				<dd><a href="#tab2">Social</a></dd>
				<dd><a href="#tab3">SEO</a></dd>
				<dd><a href="#tab4">Amenities</a></dd>
				<dd><a href="#tab5">Property Settings</a></dd>
				<dd><a href="#tab6">Property Styles</a></dd>
			</dl>
			@if(empty($settings->id))
			{!! Form::model($settings, ['url' => action('Admin\AdminController@postSettings'), 'method' => 'post', 'files' => true]) !!}
			@else
			{!! Form::model($settings, ['url' => action('Admin\AdminController@putSettings'), 'method' => 'PUT', 'files' => true]) !!}
			@endif
			{!! Form::hidden('state_id', $state->id) !!}
			{!! Form::hidden('formType', 'GenSettings') !!}
			<ul class="tabs-content">
				<li class="active" id="tab0Tab">
					<h5>General</h5>
					<br>
					{!! Form::label('main_logo', 'Site Logo') !!}
					<img src='{{$state->main_logo}}' style="max-height: 100px; max-width: 100px;">
					{!! Form::file('main_logo') !!}
					{!! Form::label('two_pack', 'Two Pack Image') !!}
					<img src='{{$settings->two_pack}}' style="max-height: 300px; max-width: 800px;">
					{!! Form::file('two_pack') !!}
					{!! Form::label('from_email', 'From Email', ['class' => 'required']) !!}
					{!! Form::text('from_email') !!}
					{!! Form::label('latitude', 'Latitude', ['class' => 'required']) !!}
					{!! Form::text('latitude') !!}
					{!! Form::label('longitude', 'Longitude', ['class' => 'required']) !!}
					{!! Form::text('longitude') !!}
					{!! Form::label('footer_links') !!}
					{!! Form::textarea('footer_links') !!}
					{!! Form::label('footer_sidebar_links') !!}
					{!! Form::textarea('footer_sidebar_links') !!}
				</li>
				<li id="tab1Tab">
					<h5>Homepage</h5>
					{!! Form::label('home_header_image') !!}
					<img src='{{$settings->home_header_image}}' style="max-height: 100px; max-width: 100px;">
					{!! Form::file('home_header_image') !!}
					<br>
					{!! Form::label('home_header') !!}
					{!! Form::textarea('home_header') !!}
					{!! Form::label('home_sidebar') !!}
					{!! Form::textarea('home_sidebar') !!}
					{!! Form::label('home_content') !!}
					{!! Form::textarea('home_content', old('home_content', $settings->home_content), ['class' => 'tinymce']) !!}
					<br>
					{!! Form::label('home_bottom_content') !!}
					{!! Form::textarea('home_bottom_content', old('home_bottom_content', $settings->home_bottom_content), ['class' => 'tinymce']) !!}
					<br>
				</li>
				<li id="tab2Tab">
					<h5>Social Media</h5>
					{!! Form::label('facebook') !!}
					{!! Form::text('social_media[facebook]') !!}
					{!! Form::label('twitter') !!}
					{!! Form::text('social_media[twitter]') !!}
					{!! Form::label('pinterest') !!}
					{!! Form::text('social_media[pinterest]') !!}
				</li>
				<li id="tab3Tab">
					<h5>SEO</h5>
					{!! Form::label('google_analytics', 'Google Analytics', ['class' => 'required']) !!}
					{!! Form::text('google_analytics') !!}
					{!! Form::label('seo_title', 'Default SEO Title') !!}
					{!! Form::text('seo_title') !!}
					{!! Form::label('seo_description', 'Default SEO Description') !!}
					{!! Form::text('seo_description') !!}
				</li>
			</ul>
			<hr />
			<div class="panel text-center">
				{!! Form::submit('Save', ['class' => 'success button']) !!}
				{!! link_to_action('Admin\AdminController@getSettings', 'Cancel',[], ['class' => 'secondary button']) !!}
			</div>
			{!! Form::close() !!}

			<h3>Keys/API settings</h3>
			{!! Form::model($state, ['url' => action('Admin\AdminController@postStateParams'), 'method' => 'post']) !!}
			{!! Form::label('map_key', 'Map Key', ['class' => 'required']) !!}
			{!! Form::text('map_key') !!}
			{!! Form::label('mail_key', 'Mailgun Key', ['class' => 'required']) !!}
			{!! Form::text('mail_key') !!}
			{!! Form::label('mail_username', 'Mailgun Username', ['class' => 'required']) !!}
			{!! Form::text('mail_username') !!}
			{!! Form::label('mail_password', 'Mailgun Password', ['class' => 'required']) !!}
			{!! Form::text('mail_password') !!}
			<div class="panel text-center">
				{!! Form::submit('Save Keys', ['class' => 'success button']) !!}
				{!! link_to_action('Admin\AdminController@getSettings', 'Cancel',[], ['class' => 'secondary button']) !!}
			</div>
			{!! Form::close() !!}

			<div id="tab4"></div>
			<h3>Amenities</h3>
			{!! Form::model($amenities, ['url' => action('Admin\AdminController@postStateAmenities'), 'method' => 'post', 'files' => true]) !!}
			{!! Form::hidden('formType', 'Amenities') !!}

			<div class="row">
				<label for="features">{{$state->name}} Amenities</label>
				<p>Choose all that apply to this state.</p>
				@foreach($categories as $category)
					<h4>{{$category->title}}</h4>
					<div class="twelve columns">
					@php $i = 0; @endphp
					@foreach($category->amenities as $amenity)
						@if($i % 3 == 0)
							<div class="row">
						@endif
						<div class="four columns">
							<label>
								{!! Form::checkbox('amenities[]', $amenity->id, $amenity->states()->where('id', $state->id)->exists()) !!}
								{{$amenity->title}}</label>
							<label>
						</div>
						@if($i % 3 == 2)
							</div>
						@endif
						@php $i++; @endphp
					@endforeach
					@if($i % 3 != 0)
						@php $remainder = $i % 3; @endphp
						@for($remainder; $remainder < 3; $remainder++)
							<div class="four columns">
							</div>
						@endfor
					</div>
					@endif
					</div>
				@endforeach
			</div>
				<div class="panel text-center">
					{!! Form::submit('Save Amenities', ['class' => 'success button']) !!}
					{!! link_to_action('Admin\AdminController@getSettings', 'Cancel',[], ['class' => 'secondary button']) !!}
				</div>
			{!! Form::close() !!}
			<div id="tab5"></div>
			<h3>Property Settings</h3>
			{!! Form::model($propertySettings, ['url' => action('Admin\AdminController@postStateSettings'), 'method' => 'post', 'files' => true]) !!}

			<div class="row">
				<label for="features">{{$state->name}} Property Settings</label>
				<p>Choose all that apply to this state.</p>
					@php $i = 0; @endphp
					@foreach($propertySettings as $setting)
						@if($i % 3 == 0)
							<div class="row">
						@endif
						<div class="four columns">
							<label>
								{!! Form::checkbox('settings[]', $setting->id, $setting->states()->where('id', $state->id)->exists()) !!}
								{{$setting->setting}}</label>
							<label>
						</div>
						@if($i % 3 == 2)
							</div>
						@endif
						@php $i++; @endphp
					@endforeach
					@if($i % 3 != 0)
						@php $remainder = $i % 3; @endphp
						@for($remainder; $remainder < 3; $remainder++)
							<div class="four columns">
							</div>
						@endfor
					</div>
					@endif
					</div>
				<div class="panel text-center">
					{!! Form::submit('Save Property Settings', ['class' => 'success button']) !!}
					{!! link_to_action('Admin\AdminController@getSettings', 'Cancel',[], ['class' => 'secondary button']) !!}
				</div>
			{!! Form::close() !!}
			<div id="tab6"></div>
			<h3>Property Styles</h3>
			{!! Form::model($propertyStyles, ['url' => action('Admin\AdminController@postStateStyles'), 'method' => 'post', 'files' => true]) !!}

			<div class="row">
				<label for="features">{{$state->name}} Property Styles</label>
				<p>Choose all that apply to this state.</p>
					@php $i = 0; @endphp
					@foreach($propertyStyles as $style)
						@if($i % 3 == 0)
							<div class="row">
						@endif
						<div class="four columns">
							<label>
								{!! Form::checkbox('styles[]', $style->id, $style->states()->where('id', $state->id)->exists()) !!}
								{{$style->style}}</label>
							<label>
						</div>
						@if($i % 3 == 2)
							</div>
						@endif
						@php $i++; @endphp
					@endforeach
					@if($i % 3 != 0)
						@php $remainder = $i % 3; @endphp
						@for($remainder; $remainder < 3; $remainder++)
							<div class="four columns">
							</div>
						@endfor
					</div>
					@endif
					</div>
				<div class="panel text-center">
					{!! Form::submit('Save Property Styles', ['class' => 'success button']) !!}
					{!! link_to_action('Admin\AdminController@getSettings', 'Cancel',[], ['class' => 'secondary button']) !!}
				</div>
			{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@endsection
@push('scripts')
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>tinymce.init({ selector:'.tinymce' });</script>
@endpush
