@extends('layouts.admin')
@section('content')
@include('partials.admin-nav')
<div class="row rwc-module">
	<div class="twelve columns">
		<div class="rwc-wrapper">
			<h3>Property Management</h3>
			@include('partials.success')
			<hr/>
			<a class="success button" href="{{ action('Admin\PropertiesController@create') }}">Add New</a>
			@if(Auth::user()->isAdmin())
				<p>
					<br />
					<script type="text/javascript">
						$(document).ready(function(){
							$('#filter_fkid_users').change(function(){
								$('#form1').submit();
							}).val('{{ Request::input('user_id') }}');
						});
					</script>
					<form action="{{ action('Admin\PropertiesController@index') }}" method="GET" id="form1">
						<select name="user_id" id="filter_fkid_users">
							<option value="">- View All Owners</option>
							@foreach($owners as $owner)
								<option value="{{ $owner->id }}">{{ $owner->name }}</option>
							@endforeach
						</select>
					</form>
				</p>
			@endif
			<table width="100%" border="0" cellspacing="1" cellpadding="0">
				<thead>
					<tr>
						<th width="70%" align="left" valign="top">Click the green Edit button to update your listing.</th>
						<th width="15%" align="center" valign="top">EDIT</th>
						<th width="15%" align="center" valign="top">DELETE</th>
					</tr>
				</thead>
				<tbody>
					@foreach($properties as $property)
						@if( isset($property) )
						<tr>
							<td align="left" valign="top" class="tablecell">
								<strong><a href="/rental/{{ $property->public_id }}" target="_blank">
								{{ $property->name }}
								</a>
								</strong>
								@if( isset($property->user->name) )
									- Owned by {{ $property->user->name }} <br>
								@endif
								{{ $property->address }}, {{ $property->city }}, {{ $property->state->abbr }} {{ $property->zip }}
							</td>
							<td align="center" valign="top" class="tablecell">
								<a class="small success button"
									href="{{ action('Admin\PropertiesController@edit',  $property->id) }}">Edit
								</a>
							</td>
							<td align="center" valign="top" class="tablecell">
								<form action="{{ action('Admin\PropertiesController@destroy', $property->id) }}" method="POST">
								{{ csrf_field() }}
									<input type="hidden" name="_method" value="DELETE">
									<button type="submit" class="small alert button" onClick="if(confirm('Are you sure you want to delete this Property?')) {return true;} else {return false;}">Delete</button>
								</form>
							</tr>
							@endif
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection
