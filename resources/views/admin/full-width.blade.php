@extends('layouts/admin')
@section('content')
@include('partials.admin-nav')
<div class="row rwc-module">
	<div class="twelve columns">
		<div class="rwc-wrapper">
			<h3>{{ $page->title }}</h3>
			<hr>
			{!! $page->content !!}
		</div>
	</div>
</div>
@endsection
