@extends('layouts.admin')
@section('content')
@include('partials.admin-nav')
<div class="row rwc-module">
	<div class="twelve columns">
		<div class="rwc-wrapper">
			<h3>{{isset($user->id)?'Edit':'Add'}} User</h3>
			<hr>
			@include('partials.errors')
			@include('partials.success')

			@if(isset($user->id))
			{!! Form::model($user, ['url' => action('Admin\UsersController@update', $user->id), 'method' => 'put', 'files' => true]) !!}
			@else
			{!! Form::open(['url' => action('Admin\UsersController@store'), 'method'=>'post', 'files' => true]) !!}
			@endif
				{!! Form::hidden('state_id', $state->id) !!}
				<div class="row">
					<div class="columns six">
						{!! Form::label('name', 'Full Name', ['class' => 'required']) !!}
						{!! Form::text('name') !!}
					</div>
					<div class="columns six">
						{!! Form::label('status', 'Status') !!}
						{!! Form::select('status', ['active' => 'Active', 'inactive' => 'Inactive']) !!}
					</div>
				</div>
				<hr />
				<div class="row">
					<div class="four columns">
						{!! Form::label('to_email', 'To Email') !!}
						{!! Form::text('to_email') !!}
					</div>
					<div class="five columns">
						{!! Form::label('password', 'Password') !!}
						<div class="row">
							<div class="five columns">
								{!! Form::password('password') !!}
							</div>
							<div class="seven columns">
								<div class="row collapse">
									<div class="five columns">
										<span class="prefix">Retype:</span>
									</div>
									<div class="seven columns">
										{!! Form::password('password2') !!}
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="three columns">
						{!! Form::label('role_id', 'Role') !!}
						{!! Form::select('role_id', [ 3 => 'Owner', 2 => 'Admin']) !!}
					</div>
				</div>
				<hr />
				<div class="row">
					<div class="four columns">
						{!! Form::label('email', 'Billing Email Address', ['class' => 'required']) !!}
						{!! Form::email('email') !!}
					</div>
					<div class="four columns">
						{!! Form::label('phone', 'Billing Phone') !!}
						{!! Form::text('phone') !!}
					</div>
					<div class="four columns">
						{!! Form::label('address', 'Billing Address') !!}
						{!! Form::textarea('address', old('address', $user->address), ['rows' => '5']) !!}
					</div>
				</div>
				<div class="row">
					{!! Form::label('user_photo', 'Add an Image') !!}
					{!! Form::file('user_photo') !!}
				</div>
				<hr />
				<div class="panel text-center">
					{!! Form::submit('Save', ['class' => 'success button']) !!}
					{!! link_to_action('Admin\UsersController@index', 'Cancel',[], ['class' => 'secondary button']) !!}
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection
