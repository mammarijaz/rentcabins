@extends('layouts.admin')
@section('content')
@include('partials.admin-nav')
<div class="row rwc-module">
	<div class="twelve columns">
		<div class="rwc-wrapper">
			<h3>Welcome, {{Auth::user()->name }}</h3>
			<p>
			Welcome to your Property Dashboard. This is where you can make updates to your listing(s) 24/7. Take your time and become familiar with the content under each tab. Avoid typos and extra spaces, use good grammar, and write in first-person. Your property is likely being seen for the first time, so make a friendly first impression.
			</p>
			<p>
			– We got you started with descriptions, rates information, pictures, videos, link to your property website, contact information, and more. For best results, keep your descriptions original - do not copy & paste them from OTHER websites. Duplicate content will result in fewer inquiries. Note: You can use the same copy in the About the Owner/Manager box if you have more than one listing.
			</p>
			<p>
			– Keep your availability calendar(s) current. Using the calendar is optional - once you start using it, it will appear on your listing. The calendar is tied to our search tool.
			</p>
			<p>
			– Make sure your phone number and email address have been entered correctly. If you need to change non-editable content, email us.
			</p>
			<p>
			– Keep your photo gallery(s) full. Showing 3-4 pictures will result in fewer inquiries. See the video link under the Photos tab for instructions.
			</p>
			<p>
			Your inquiries will come via email or phone. You should occasionally send yourself a "test" inquiry from your listing. NOTE: Every email inquiry is automatically backed up under the Inquiries tab. Also, check out our Advertising Opportunities and read through the Frequently Asked Questions above.
			</p>
			<p>
			READY TO REVIEW & UPDATE YOUR LISTING(S)? CLICK ON THE PROPERTY TAB ABOVE.
			</p>
		</div>
	</div>
</div>
@endsection
