@extends('layouts.admin')
@section('content')
@include('partials.admin-nav')
<div class="row rwc-module" id="create-property">
	<div class="twelve columns">
		<div class="rwc-wrapper">
			<h3>{{$property->id?'Edit':'Add'}} {{$property->name}}</h3>
			<hr/>
			@include('partials.errors')

			@if($property->id)
			{!! Form::model($property, ['url' => action('Admin\PropertiesController@update', $property->id),
				'method' => 'put', 'files' => true]) !!}
			@else
			{!! Form::open(['url' => action('Admin\PropertiesController@store'), 'method' => 'post', 'files' => true]) !!}
			@endif
				<dl class="tabs mobile">
					<dd class="active"><a href="#tab0">Description</a></dd>
					<dd><a href="#tab1">Location</a></dd>
					<dd><a href="#tab2">Availability</a></dd>
					<dd><a href="#tab3">Amenities</a></dd>
					<dd><a href="#tab4">Rates / Specials</a></dd>
					<dd><a href="#tab5">Photos</a></dd>
					<dd><a href="#tab6">Videos</a></dd>
					<dd><a href="#tab7">Links / Reviews</a></dd>
					@if(Auth::user()->isAdmin())
						<dd><a href="#tab8">SEO</a></dd>
					@endif
				</dl>
				<ul class="tabs-content">
					<li class="active" id="tab0Tab">
						<h5>Fill up these Brief Description; Unique Features; and Thing to Do boxes with exciting copy!</h5>
						<div class="row">
							<div class="six columns">
							{!! Form::label('status', 'Status', ['class' => 'required']) !!}
							{!! Form::select('status', ['active' => 'Active', 'inactive' => 'Inactive']) !!}
							</div>
						</div>
						@if(Auth::user()->isAdmin() == false)
							{!! Form::hidden('user_id', Auth::user()->id) !!}
						@else
						<div class="row">
							<div class="columns six">
								{!! Form::label('user_id', 'Owner') !!}
								{!! Form::select('user_id', $owners, null, ['placeholder' => 'Choose an Owner']) !!}
							</div>
						</div>
						@endif

						<div class="row">
							
							<div class="six columns">
								{!! Form::label('resort', 'Resort/PM') !!}
								{!! Form::select('resort_id', $resorts, null, array('class' => 'form-control')) !!}
							</div>
							<div class="six columns">
								{!! Form::label('name', 'Rental Name', ['class' => 'required']) !!}
								{!! Form::text('name') !!}
							</div>
						</div>

						<div class="row">
							<div class="twelve columns">
								<br />
								<label>Brief Description<br />(3000 characters max - Use this box to sell your vacation experience. The number of bedrooms, bathrooms, and capacity will ALREADY appear on your listing.)</label>
								{!! Form::textarea('description', old('description', $property->description), ['id'=>'description','class'=>"charcount", 'data-maxlength'=>"3000"]) !!}
								<div id="charcount-description" style="margin-bottom: 20px;margin-top:-10px;font-size:11px;"></div>
							</div>
						</div>
						<div class="row">
							<div class="twelve columns">
								<label for="unique_features"><br />Unique Features<br />(2000 characters max - Use lots of adjectives and describe why this rental is unique.)</label>
								{!! Form::textarea('unique_features', old('unique_features', $property->unique_features), ['id'=>'unique_features','class'=>"charcount", 'data-maxlength'=>"2000"]) !!}
								<div id="charcount-unique_features" style="margin-bottom: 20px;margin-top:-10px;font-size:11px;"></div>
							</div>
						</div>
						<div class="row">
							<div class="twelve columns">
								<label for="things_to_do"><br />Things To Do<br />(3000 characters max - At your property, and NEARBY.)</label>
								{!! Form::textarea('things_to_do', old('things_to_do', $property->things_to_do), ['id'=>'things_to_do','class'=>"charcount", 'data-maxlength'=>"3000"]) !!}
								<div id="charcount-things_to_do" style="margin-bottom: 20px;margin-top:-10px;font-size:11px;"></div>
							</div>
						</div>
						<div class="row">
							<div class="twelve columns">
								<label for="owner_bios"><br />About the Owner/Manager<br />(3000 characters max. Avoid copying and pasting from other sites.)</label>
								{!! Form::textarea('owner_bios', old('owner_bios', $property->owner_bios), ['id'=>'owner_bios','class'=>"charcount", 'data-maxlength'=>"3000"]) !!}
								<div id="charcount-owner_bios" style="margin-bottom: 20px;margin-top:-10px;font-size:11px;"></div>
							</div>
						</div>

						<div class="row">
							<div class="twelve columns">
								<label style="padding-top: 25px;">
								{!! Form::checkbox('featured') !!}&nbsp;Featured Property
								</label>
							</div>
						</div>

						<div class="row">
							<div class="twelve columns">
								<label for="featured_description"><br />Featured Description<br />Bulleted List for properties that are featured on the homepage.</label>
								{!! Form::textarea('featured_description', old('featured_description', $property->featured_description), ['id'=>'featured_description','class'=>"charcount", 'data-maxlength'=>"3000"]) !!}
								<div id="charcount-featured_description" style="margin-bottom: 20px;margin-top:-10px;font-size:11px;"></div>
							</div>
						</div>

						<div class="row">
							<div class="six columns">
							{!! Form::label('featured_photo', 'Featured Photo:') !!}
							{!! Form::file('featured_photo') !!}
							</div>
						
							<div class="six columns">
							<img src='{{$property->featured_photo}}' style="max-height: 100px; max-width: 100px;">
							</div>
						</div>

					</li>

  					<li id="tab1Tab">
						<h5>Location</h5>
						<p>The street address will be used to position your property pin on the Google Map.</p>

						<div class="row">
							<div class="nine columns">
								{!! Form::label('address', 'Address', ['class' => 'required']) !!}
								{!! Form::text('address') !!}
								<div class="row">
									<div class="four columns">
										{!! Form::label('city', 'City', ['class' => 'required']) !!}
										{{-- {!! Form::text('city', $property->city, [$isSuperAdmin ? '' : 'disabled']) !!} --}}
										{!! Form::text('city', $property->city) !!}
									</div>
									<input type="hidden" name="state_id" value="{{$state->id}}">
									<div class="four columns">
										{!! Form::label('zip', 'Zip', ['class' => 'required']) !!}
										{!! Form::text('zip') !!}
									</div>
								</div>
							</div>
							<div class="three columns">
								<label for="address_latitude">Latitude <a href="javascript:alert(\' If your address does not position your property pin correctly on the Google Map; entering Latitude and Longitude coordinates will ASSURE exact location. If you have a smart phone, there are several free apps you can download.\');">What Is This?</a></label>
								{!! Form::text('address_latitude', old('address_latitude', $property->address_latitude),
									['id'=>'address_latitude']) !!}
								{!! Form::label('address_longitude', 'Longitude') !!}
								{!! Form::text('address_longitude', old('address_longitude', $property->address_longitude),
									['id'=>'address_longitude']) !!}
							</div>
						</div>

						<div class="row">
							<div class="three columns">
								{!! Form::label('email', 'Email Address', ['class' => 'required']) !!}
								{{-- {!! Form::text('email', $property->email, [$isSuperAdmin ? '' : 'disabled']) !!} --}}
								{!! Form::text('email', $property->email) !!}

							</div>
							<div class="three columns">
								{!! Form::label('phone', 'Phone', ['class' => 'required']) !!}
								{!! Form::text('phone') !!}
							</div>
							<div class="three columns">
								{!! Form::label('phone_cell', 'Phone') !!}
								{!! Form::text('phone_cell') !!}
							</div>
							<div class="three columns">
								{!! Form::label('phone_cell_carrier', 'To Get Inquiry TXT Alerts! Provide Carrier:') !!}
								{!! Form::select('phone_cell_carrier', [
									'@message.alltel.com' => 'Alltel',
									'@txt.att.net' => 'AT&T',
									'@msg.fi.google.com' => 'Google Fi',
									'@messaging.nextel.com' => 'Nextel',
									'@messaging.sprintpcs.com' => 'Sprint',
									'@tmomail.net' => 'T-Mobile',
									'@mms.uscc.net' => 'US Cellular',
									'@vtext.com' => 'Verizon',
									'@vmobl.com' => 'Virgin Mobile',
								], null, ['placeholder' => '--']) !!}
							</div>
						</div>

						<div class="row">
							<div class="twelve columns">
								<label><br />Which Region(s) do you want this listing to appear under?</label>
								<p><span class="radius label">Tip:</span> You can choose more than one, but be realistic.</p>
								<div class="row">
									@for($i=0; $i<count($regions); $i++)
										@if($i == 0)
											<div class="three columns">
										@elseif($i == 4)
											</div>
											<div class="three columns end">
										@endif

										<label>
											{!! Form::checkbox("regions[$i]", $regions[$i]->id, in_array($regions[$i]->id, $userRegions)) !!}
											{{$regions[$i]->name}}
										</label>

										@if($i == count($regions))
										</div>
										@endif
									@endfor
								</div>
							</div>
						</div>

						<div class="row">
							<div class="twelve columns">
								{!! Form::label('location_id', "In addition to your rental's physical location, you can choose 1 additional location from this drop-down menu. When visitors choose either location using the search tool, your listing will appear in the Results.") !!}
								{!! Form::select('location_id', $locations, old('location_id',
									$property->location_id), ['placeholder'=> '--']) !!}
							</div>
						</div>
						<br />
						<div class="row">
							<div class="twelve columns">
								<label>Which Property Setting(s) do you want this listing to appear under?</label>
								<p><span class="radius label">Tip:</span> You can choose more than one, but be realistic.</p>
								<div class="row">
									<div class="four columns">
										@for($i=0; $i < count($propertySettings); $i++)
											@if($i == ceil(count($propertySettings) / 3))
											</div>
											<div class="four columns">
											@elseif($i == (ceil(count($propertySettings) / 3) * 2) )
											</div>
											<div class="four columns end">
											@endif
											<label>
												{!! Form::checkbox('settings[]', $propertySettings[$i]->id) !!}
												{{$propertySettings[$i]->setting}}</label>
											<label>
										@endfor
										</label>
									</div>
								</div>
								<br>
								<div class="row">
									<div class="twelve columns">
										<label for="on_waters">Finish this statement, On (_________). Type in the name of lake or river rental is on/near. Not on water? Describe setting (6 acres of wooded paradise):</label>
										{!! Form::text('on_waters') !!}
									</div>
								</div>
								<br>
								<div class="row">
									<div class="twelve columns">
										<label for="weekly_only">Finish this statement. Weekly only (_________). Example: (7/1 to 9/1. Saturday to Saturday. Otherwise, 3 night minimum.) 100 characters with spaces. Leave BLANK if you don&rsquo;t want to display a weekly only policy:</label>
										{!! Form::text('weekly_only') !!}
									</div>
								</div>
							</div>
						</div>
						<br />
						<div class="row">
							<div class="twelve columns">
								<label>Which Property Style(s) do you want this listing to appear under?</label>
								<p><span class="radius label">Tip:</span> You can choose more than one, but be realistic.</p>
								<div class="row">
									<div class="four columns">
										@for($i=0; $i < count($propertyStyles); $i++)
											@if($i == ceil(count($propertyStyles) / 3))
											</div>
											<div class="four columns">
											@elseif($i == (ceil(count($propertyStyles) / 3) * 2) )
											</div>
											<div class="four columns end">
											@endif
											<label>
												{!! Form::checkbox('styles[]', $propertyStyles[$i]->id) !!}
												{{$propertyStyles[$i]->style}}</label>
											<label>
										@endfor
										</label>
									</div>
								</div>
							</div>
						</div>

					</li>

					<li id="tab2Tab">
						<h5>Rental owners who use the calendars receive more inquiries.</h5>
						<div class="row">
							<div class="twelve columns">
								<div class="panel">
								<label for="blocked_dates_textarea">Blocked Dates - Click days to block. Click again to unblock. Click Save.</label>
								<textarea name="blocked_dates" id="blocked_dates_textarea" rows="5" cols="60" style="display: none;"></textarea>
								<div id="blocked_dates"></div>
								</div>
							</div>
						</div>
					</li>

					<li id="tab3Tab">
						<h5>Choose rental amenities carefully.</h5>

						<div class="row">
							<div class="four columns">
								{!! Form::label('bedrooms', 'Number of Bedrooms') !!}
								{!! Form::number('bedrooms') !!}
							</div>
							<div class="four columns">
								{!! Form::label('bathrooms', 'Number of Bathrooms') !!}
								{!! Form::number('bathrooms', null, ['step' => '.5']) !!}
							</div>
							<div class="four columns">
								{!! Form::label('max_capacity', 'Maximum Capacity:') !!}
								{!! Form::number('max_capacity') !!}
							</div>
						</div>

						<div class="row">
				<label for="features">Amenities and Special Needs</label>
				<p>Choose all that apply to this property.</p>
				@foreach($categories as $category)
					<h4>{{$category->title}} (select all that apply)</h4>
					<div class="twelve columns">
					@php $i = 0; @endphp
					@foreach($category->amenities as $amenity)
						@if($i % 3 == 0)
							<div class="row">
						@endif
						<div class="four columns">
							<label>
								{!! Form::checkbox('amenities[]', $amenity->id) !!}
								{{$amenity->title}}</label>
							<label>
						</div>
						@if($i % 3 == 2)
							</div>
						@endif
						@php $i++; @endphp
					@endforeach
					@if($i % 3 != 0)
						@php $remainder = $i % 3; @endphp
						@for($remainder; $remainder < 3; $remainder++)
							<div class="four columns">
							</div>
						@endfor
					</div>
					@endif
					</div>
				@endforeach
					</li>
					<li id="tab4Tab">
						<h5>Rates &amp; Specials</h5>

						<div class="row">
							<div class="three columns">
								{!! Form::label('min_rates_daily', 'Lowest Nightly Rate') !!}
								<p>If your rates do not change seasonally; enter the same number for both. Do not use commas.</p>
								<div class="row collapse">
									<div class="one mobile-one column"><span class="prefix">$</span></div>
									<div class="seven mobile-two columns">
										{!! Form::text('min_rates_daily',
											old('min_rates_daily', $property->min_rates_daily), ['style'=>"text-align: right;"]) !!}
									</div>
									<div class="four mobile-one columns"><span class="postfix">/ night</span></div>
								</div>
								{!! Form::label('max_rates_daily', 'Highest Nightly Rate') !!}
								<div class="row collapse">
									<div class="one mobile-one column"><span class="prefix">$</span></div>
									<div class="seven mobile-two columns">
										{!! Form::text('max_rates_daily',
											old('max_rates_daily', $property->max_rates_daily), ['style'=>"text-align: right;"]) !!}
									</div>
									<div class="four mobile-one columns"><span class="postfix">/ night</span></div>
								</div>
							</div>
							<div class="three columns">
								{!! Form::label('min_rates_weekly', 'Lowest Weekly Rate') !!}
								<p>If your rates do not change seasonally; enter the same number for both. Do not use commas.</p>
								<div class="row collapse">
									<div class="one mobile-one column"><span class="prefix">$</span></div>
									<div class="seven mobile-two columns">
										{!! Form::text('min_rates_weekly',
											old('min_rates_weekly', $property->min_rates_weekly), ['style'=>"text-align: right;"]) !!}
									</div>
									<div class="four mobile-one columns"><span class="postfix">/ week</span></div>
								</div>
								{!! Form::label('max_rates_weekly', 'Highest Weekly Rate') !!}
								<div class="row collapse">
									<div class="one mobile-one column"><span class="prefix">$</span></div>
									<div class="seven mobile-two columns">
										{!! Form::text('max_rates_weekly',
											old('max_rates_weekly', $property->max_rates_weekly), ['style'=>"text-align: right;"]) !!}
									</div>
									<div class="four mobile-one columns"><span class="postfix">/ week</span></div>
								</div>
							</div>
							<div class="three columns">
								<label for="rates">Rate Details</label>
								<textarea name="rates" id="rates" rows="5" cols="40" class="charcount"
									data-maxlength="1200">{{old('rates', $property->rates)}}</textarea>
								<div id="charcount-rates" style="margin-bottom: 20px;margin-top:-10px;font-size:11px;"></div>
								<p>Example: Oct.-Apr. $120/nt, 2 nt. min; $1200/wk. May-Sept. $190/nt, 2 nt. min; $1750/wk. Max length 1200 characters</p>
							</div>
							<div class="three columns">
								<label for="special_offers">Rental Specials</label>
								<textarea name="special_offers" id="special_offers" rows="5" cols="40" class="charcount"
									data-maxlength="600">{{old('special_offers', $property->special_offers)}}</textarea>
								<div id="charcount-special_offers" style="margin-bottom: 20px;margin-top:-10px;font-size:11px;"></div>
								<p>List your special(s). Update them 24/7.</p>
							</div>
						</div>
					</li>

					<li id="tab5Tab">
						<div id="photosSection">
							<h3>Photos</h3>
							@include('partials.image-upload')
							{!! Form::label('image[]', 'Add an Image') !!}
							{!! Form::file('image[]') !!}
							{!! Form::label('newImageDescription[0]', 'Image Description') !!}
							{!! Form::text('newImageDescription[0]', '', array('class' => 'imageDescription')) !!}
							<br>
						</div>
				<button type="button" onclick="addImageElement()">Add Another Image</button>
				<hr />
				<div class="panel text-center">
					</li>

					<li id="tab6Tab">
						<h5>Rental owners who fill up their video gallery with quality videos receive more inquiries.</h5>

						<p>Instructions: You can add up to <strong>3 YouTube videos</strong>. You only need to add the <strong>YouTube video ID</strong> in each field below.  For example, if your video url is <kbd>https://www.youtube.com/watch?v=gfsOboszD78</kbd>, you only need to add the <kbd>gfsOboszD78</kbd> in the field below.</p>

						<div class="row">
							<div class="twelve columns">
								@for($i=1;$i<=3;$i++)
									{!! Form::label('videos['.$i.']', 'YouTube video '.$i.' ID') !!}
									{!! Form::text('videos['.$i.']') !!}
								@endfor
							</div>
						</div>
					</li>

					<li id="tab7Tab">
						<h5>Display links to your property website and social media pages. Add testimonials from renters!</h5>

						<div class="row">
							<div class="twelve columns">
								{!! Form::label('listing_url', "Your Full Website Link (Example: http://www.mycabinsite.com/)") !!}
								{!! Form::url('listing_url') !!}
								<div class="panel">
									<strong>Displaying</strong> your property website Link will reduce the number of inquiries you receive from us, but it will result in more traffic to YOUR website. We believe there is value in delivering inquiries AND traffic to your property website. We help you build YOUR online brand - not hide it. We even link to your Social media channels (renters love this). <strong>You decide:</strong> Use our website as a brand builder by adding your links. Or, maximize your inquiries from us by leaving them blank.
								</div>
							</div>
						</div>

						<div class="row">
							<div class="three columns">
								{!! Form::label('social_media_link[facebook]', 'Facebook Address') !!}
								{!! Form::text('social_media_links[facebook]') !!}
							</div>
							<div class="two columns">
								{!! Form::label('social_media_link[twitter]', 'Twitter Address') !!}
								{!! Form::text('social_media_links[twitter]') !!}
							</div>
							<div class="three columns">
								{!! Form::label('social_media_link[pinterest]', 'Pinterest Address') !!}
								{!! Form::text('social_media_links[pinterest]') !!}
							</div>
							<div class="two columns">
								{!! Form::label('social_media_link[google-plus]', 'Google+ Address') !!}
								{!! Form::text('social_media_links[google-plus]') !!}
							</div>
							<div class="two columns">
								{!! Form::label('social_media_link[linkedin]', 'LinkedIn Address') !!}
								{!! Form::text('social_media_links[linkedin]') !!}
							</div>
						</div>
						<br />
						<div class="row">
							<div class="twelve columns">
								{!! Form::label('testimonials', 'Testimonials') !!}
								<p>Share a few renter testimonials. 1200 chars max. Avoid extra spaces between testimonials.</p>
								{!! Form::textarea('testimonials') !!}
								<div id="charcount-testimonials" style="margin-bottom: 20px;margin-top:-10px;font-size:11px;"></div>
							</div>
						</div>
					</li>
					@if(Auth::user()->isAdmin())
					<li id="tab8Tab">
						<h5>Property SEO</h5>
						<div class="row">
							<div class="twelve columns">
								{!! Form::label('old_id', 'Old Database ID') !!}
								{!! Form::number('old_id') !!}
							</div>
						</div>
						<div class="row">
							<div class="twelve columns">
								{!! Form::label('seo_description', 'SEO Description') !!}
								{!! Form::textarea('seo_description') !!}
							</div>
						</div>
					</li>
					@endif
				</ul>
				<hr />
				<div class="panel text-center">
					{!! Form::submit('Save', ['class' => 'success button', 'name' => 'submitbutton', 'value' => 'Update']) !!}
					{!! link_to_action('Admin\PropertiesController@index', 'Cancel',[], ['class' => 'secondary button']) !!}
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
<script>
var savedDates = {};

@if(!empty($property->blocked_dates->lists('blocked_date')))
	savedDates = {!! json_encode($property->blocked_dates->lists('blocked_date')) !!};
@endif
</script>

<script type="text/javascript">
	function addImageElement(){
		linebreak = document.createElement("br");
		input = document.createElement("input");
		description = document.createElement("input");
		input.type = "file";
		input.name = "image[]";
		description.type = "text";
		description.className = "imageDescription";
		description.name = "newImageDescription["+ (document.querySelectorAll('.imageDescription').length) +"]";
		document.getElementById('photosSection').append(input);
		document.getElementById('photosSection').append(description);
		document.getElementById('photosSection').append(linebreak);	
	}
</script>
@endsection
