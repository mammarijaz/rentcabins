@extends('layouts.admin')
@section('content')
@include('partials.admin-nav')
<div class="row rwc-module">
	<div class="twelve columns">
		<div class="rwc-wrapper">
			<h3>Users</h3>
			@include('partials.success')
			<hr/>
			<p><a class="button" href="{{ action('Admin\UsersController@create') }}">Add New</a></p>
			<table width="100%" border="0" cellspacing="1" cellpadding="0">
				<thead>
					<tr>
						<th width="70%" align="left" valign="top">Name</th>
						<th width="15%" align="center" valign="top">EDIT</th>
						<th width="15%" align="center" valign="top">DELETE</th>
					</tr>
				</thead>
				<tbody>
					@foreach($users as $user)
					<tr>
						<td align="left" valign="top" class="tablecell">{{ $user->name }}</td>
						<td align="center" valign="top" class="tablecell">
							<a class="small success button" href="{{ action('Admin\UsersController@edit', $user->id) }}">Edit</a>
						</td>
						<td align="center" valign="top" class="tablecell">
							<form action="{{action('Admin\UsersController@destroy', $user->id)}}" method="POST">
								{{ csrf_field() }}
								<input type="hidden" name="_method" value="DELETE">
								<input type="submit" value="Delete" class="small alert button"
									onclick="if(confirm('Are you sure you want to delete this User?')) {return true;} else {return false;}">
							</form>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection
