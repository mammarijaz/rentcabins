@extends('layouts.admin')
@section('content')
@include('partials.admin-nav')
<div class="row rwc-module" id="page-create">
	<div class="twelve columns">
		<div class="rwc-wrapper">
			<h3>{{isset($page->id)?'Edit':'Add'}} Page</h3>
			@include('partials.errors')

			@if(isset($page->id))
			{!! Form::model($page, ['url' => action('Admin\PagesController@update', $page->id), 'method' => 'put', 'files' => true]) !!}
			@else
			{!! Form::open(['url' => action('Admin\PagesController@store'), 'method'=>'post', 'files' => true]) !!}
			@endif
				{!! Form::hidden('state_id', $state->id) !!}
				{!! Form::label('title', 'Title', ['class' => 'required']) !!}
				{!! Form::text('title') !!}
				{!! Form::label('slug', 'Slug', ['class' => 'required']) !!}
				{!! Form::text('slug') !!}
				<div class="row" style="margin-bottom: 10px;">
					<div class="columns six">
						<label>
							{!! Form::checkbox('featured') !!}&nbsp;Featured
						</label>
					</div>
					<div class="columns six">
						{!! Form::file('thumb_image') !!}
					</div>
					<div class="six columns end">
						<img src='{{$page->thumb_image}}' style="max-height: 100px; max-width: 100px;">
					</div>
				</div>

				{!! Form::label('seo_title', 'SEO Title') !!}
				{!! Form::text('seo_title') !!}
				{!! Form::label('seo_description', 'SEO Description') !!}
				{!! Form::text('seo_description') !!}
				{!! Form::label('header_text') !!}
				{!! Form::text('header_text', old('header_text', $page->header_text), ['class'=>"tinymce"]) !!}
				{!! Form::label('content', 'Content') !!}
				{!! Form::textarea('content', old('content', $page->content), ['class'=>"tinymce"]) !!}
				<hr />
				<div class="panel text-center">
					{!! Form::submit('Save', ['class' => 'success button']) !!}
					{!! link_to_action('Admin\PagesController@index', 'Cancel',[], ['class' => 'secondary button']) !!}
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection
@push('scripts')
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>tinymce.init({ selector:'.tinymce' });</script>
@endpush
