@extends('layouts.admin')
@section('content')
@include('partials.admin-nav')
<div class="row rwc-module">
	<div class="twelve columns">
		<div class="rwc-wrapper">
			<h3>{{isset($location->id)?'Edit':'Add'}} Location</h3>
			@include('partials.errors')
			@include('partials.success')

			@if(isset($location->id))
			{!! Form::model($location, ['url' => action('Admin\LocationsController@update', $location->id), 'method' => 'put']) !!}
			@else
			{!! Form::open(['url' => action('Admin\LocationsController@store'), 'method' => 'post']) !!}
			@endif
				{!! Form::hidden('state_id', $state->id) !!}
				{!! Form::label('name', 'Name', ['class' => 'required']) !!}
				{!! Form::text('name') !!}
				{!! Form::label('slug', 'Slug', ['class' => 'required']) !!}
				{!! Form::text('slug') !!}
				{!! Form::label('region_id', 'Region', ['class' => 'required']) !!}
				{!! Form::select('region_id', $regions) !!}
				<br><br>
				{!! Form::label('seo_title', 'SEO Title') !!}
				{!! Form::text('seo_title', old('seo_title', $location->seo_title)) !!}
				{!! Form::label('seo_description', 'SEO Description') !!}
				{!! Form::text('seo_description', old('seo_description', $location->seo_description)) !!}
				<br><br>
				{!! Form::label('header_text', 'Header Text') !!}
				{!! Form::textarea('header_text', old('header_text', $location->header_text), ['class'=>"tinymce"]) !!}
				<br><br>
				{!! Form::label('description', 'Description') !!}
				{!! Form::textarea('description', old('description', $location->description), ['class'=>"tinymce"]) !!}
				<br><br>
				{!! Form::label('features', 'Features') !!}
				{!! Form::textarea('features', old('features', $location->features), ['class'=>"tinymce"]) !!}
				<hr />
				<div class="panel text-center">
					{!! Form::submit('Save', ['class' => 'success button']) !!}
					{!! link_to_action('Admin\LocationsController@index', 'Cancel',[], ['class' => 'secondary button']) !!}
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection
@push('scripts')
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>tinymce.init({ selector:'.tinymce' });</script>
@endpush
