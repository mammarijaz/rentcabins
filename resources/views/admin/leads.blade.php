@extends('layouts.admin')
@section('content')
@include('partials.admin-nav')
<div class="row rwc-module">
	<div class="twelve columns">
		<div class="rwc-wrapper">
			<h3>Optins</h3>
			<table width="100%" border="0" cellspacing="1" cellpadding="0">
				<thead>
					<tr>
						<th width="30%" align="left" valign="top">Name</th>
						<th width="40%" align="left" valign="top">Email</th>
						<th width="30%" align="left" valign="top">Date</th>
					</tr>
				</thead>
				<tbody>
				@foreach($leads as $lead)
				<tr>
					<td align="left" valign="top" class="tablecell">{{ucwords($lead->name) }}</td>
					<td align="left" valign="top" class="tablecell">{{ $lead->email }}</td>
					<td align="left" valign="top" class="tablecell">{{ date('m/d/Y g:ia',strtotime($lead->created_at)) }}</td>
				</tr>
				@endforeach
				</tbody>
			</table>
			<hr/>
		</div>
	</div>
</div>
@endsection
