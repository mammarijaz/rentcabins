@extends('layouts.admin')
@section('content')
@include('partials.admin-nav')
<div class="row rwc-module">
	<div class="twelve columns">
		<div class="rwc-wrapper">
			<h3>Resorts and Property Managers</h3>
			<hr/>
			<p><a class="button" href="{{ action('Admin\ResortsController@create') }}">Add New</a></p>
			@include('partials.success')
			<table width="100%" border="0" cellspacing="1" cellpadding="0">
				<thead>
					<tr>
						<th width="70%" align="left" valign="top">Name</th>
						<th width="15%" align="center" valign="top">EDIT</th>
						<th width="15%" align="center" valign="top">DELETE</th>
					</tr>
				</thead>
				<tbody>
					@foreach($resorts as $resort)
					<tr>
						<td align="left" valign="top" class="tablecell">{{ $resort->name }}</td>
						<td align="center" valign="top" class="tablecell">
							<a class="small success button" href="{{ action('Admin\ResortsController@edit', $resort->id) }}">Edit</a>
						</td>
						<td align="center" valign="top" class="tablecell">
							<form action="{{action('Admin\ResortsController@destroy', $resort->id)}}" method="POST">
								{{ csrf_field() }}
								<input type="hidden" name="_method" value="DELETE">
								<input type="submit" value="Delete" class="small alert button"
									onclick="if(confirm('Are you sure you want to delete this resort?')) {return true;} else {return false;}">
							</form>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection
