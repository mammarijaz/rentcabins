@extends('layouts.admin')
@section('content')
@include('partials.admin-nav')
<div class="row rwc-module">
	<div class="twelve columns">
		<div class="rwc-wrapper">
			<h3>{{isset($region->id)?'Edit':'Add'}} Region</h3>
			@include('partials.errors')

			@if(isset($region->id))
			{!! Form::model($region, ['url' => action('Admin\RegionsController@update', $region->id), 'method' => 'put']) !!}
			@else
			{!! Form::open(['url' => action('Admin\RegionsController@store'), 'method'=>'post']) !!}
			@endif
				{!! Form::hidden('state_id', $state->id) !!}
				{!! Form::label('name', 'Name', ['class' => 'required']) !!}
				{!! Form::text('name') !!}
				{!! Form::label('slug', 'Slug', ['class' => 'required']) !!}
				{!! Form::text('slug') !!}
				<hr />
				<div class="panel text-center">
					{!! Form::submit('Save', ['class' => 'success button']) !!}
					{!! link_to_action('Admin\RegionsController@index', 'Cancel',[], ['class' => 'secondary button']) !!}
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection
