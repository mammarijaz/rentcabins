@extends('layouts.admin')
@section('content')
@include('partials.admin-nav')
<div class="row rwc-module">
		<div class="twelve columns">
			<div class="rwc-wrapper">
				<h3>Inquiries</h3>
				<hr/>
				<script type="text/javascript">
					$(document).ready(function(){
						$('#filter_fkid_users').change(function(){
							$('#form1').submit();
						}).val('{{ Request::input('filter_fkid_users') }}');
						$('.uidatepicker').datepick({
							dateFormat: 'yyyy-mm-dd'
						});
					});
				</script>
				@if(isset($users) && (count($users) > 1))
				<form method="get" action="{{action('Admin\InquiriesController@index') }}" id="form1">
					<p>
						<select id="filter_fkid_users" name="filter_fkid_users">
							<option value="">--</option>
							@foreach($users as $user)
							<option value="{{ $user->id }}">{{$user->name}}</option>
							@endforeach
						</select>
					</p>
					<br />
				</form>
				@endif
				@if($properties)
					<div class="row">
						<div class="three columns">
							<label for="filter_date_begin">From</label>
							<input type="text" id="filter_date_begin" name="filter_date_begin" class="uidatepicker" value="" />
						</div>
						<div class="three columns">
							<label for="filter_date_end">To</label>
							<input type="text" name="filter_date_end" class="uidatepicker" value="" />
						</div>
						<div class="three columns end">
							<br />
							<input class="medium button" type="submit" value="Update" />
						</div>
					</div>

					<h4>Total Inquiries: {{ $totalLeads }}</h4>

					<table width="100%" border="0" cellspacing="1" cellpadding="0">
						<thead>
							<tr>
								<th width="30%" align="left" valign="top">Name</th>
								<th width="40%" align="left" valign="top">Email</th>
								<th width="30%" align="left" valign="top">Date</th>
							</tr>
						</thead>
						<tbody>
						@for($i=0; $i < count($properties); $i++)
							@if($i > 0)
							<tr>
								<td align="left" valign="top" class="tablecell" colspan="3"><hr /></td>
							</tr>
							@endif
							<tr>
								<td align="left" valign="top" class="tablecell" colspan="3"><h4>{{ $properties[$i]->name }} (Inquiries: {{ $properties[$i]->leads()->count() }})</h4></td>
							</tr>
							@foreach($properties[$i]->leads as $lead)
							<div id="lead-modal-{{$lead->id}}" class="white-popup-block mfp-hide">
								<button title="Close (Esc)" type="button" class="mfp-close" style="color: grey">×</button>
								<br><b>Name:</b><br>
								{{$lead->name}}<br>
								<br><b>Email:</b><br>
								{{$lead->email}}<br>
								<br><b>Phone:</b><br>
								{{$lead->phone}}<br>
								<br><b>State:</b><br>
								{{$lead->state}}<br>
								<br><b>Arrival Date:</b><br>
								{{json_decode($lead->search_info)->arrival_date}}<br>
								<br><b>Departure Date:</b><br>
								{{json_decode($lead->search_info)->departure_date}}<br>
								<br><b>Adults:</b><br>
								{{json_decode($lead->search_info)->adults}}<br>
								<br><b>Children:</b><br>
								{{json_decode($lead->search_info)->children}}<br>
								<br><b>Message:</b><br>
								{{$lead->personalization}}

							</div>
							<tr>
								<td align="left" valign="top" class="tablecell"><a class="popup-modal" href="#lead-modal-{{$lead->id}}">{{ucwords($lead->name) }}</a></td>
								<td align="left" valign="top" class="tablecell">{{ $lead->email }}</td>
								<td align="left" valign="top" class="tablecell">{{ date('m/d/Y g:ia',strtotime($lead->created_at)) }}</td>
							</tr>
							@endforeach
						@endfor
						</tbody>
					</table>
				<p>&nbsp;</p>
				@endif
			</div>
		</div>
	</div>

	@for($i=0; $i < count($properties); $i++)
		@foreach($properties[$i]->leads() as $lead)
			<div id="lead-{{ md5($lead->id) }}" style="display: none;" class="reveal-modal large">
				@foreach(json_decode($lead->search_info,true) as $search_key=>$search_value)
					@if($search_key != 'select_rental' && $search_key != 'email_optin' && $search_key != 'is_search' && $search_key != 'spam_check')
						<strong>{{ ucwords(str_ireplace('_',' ',$search_key)) }}</strong>
						<br />
						{{ ((is_array($search_value))?implode(', ', $search_value):$search_value) }}<br /><br />
					@endif
				@endforeach
			</div>
		@endforeach
	@endfor
@endsection
