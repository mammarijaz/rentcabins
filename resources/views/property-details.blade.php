@extends('layouts.app', ['two_pack' => 'true'])
@section('content')
<div class="container">

	<div class="row">
		<div class="col-sm-12">
			<div class="backtosearch">
				@if($property->location !== NULL)
					<a href='../location/{{$property->location->slug}}' target='_top'>View all {{$property->location->name}},{{$state->abbr}} cabins</a>
				@endif
				@if(!is_null($property->resort_id))
					 | <a href='../resort/{{$property->resort->public_id}}' target='_top'>View all {{$property->resort->name}} cabins</a>
				@endif
			</div>
	</div>
</div>
<div class="container main-container">
	<div id="about" class="row property-banner">
		<div class="col-sm-7">
			<div class="">
				<h1>@if($property->resort !== NULL){{$property->resort->name}}: @endif{{$property->name}}</h1>
				<h2><span>{{$property->city}}, {{$state->name}}</span> vacation rental</h2>
			</div>
		</div>
		<div class="col-sm-5">
			<div class="sharethis pull-right">
				<h2>Share this {{$state->abbr}} cabin rental</h2>
				<!-- Go to www.addthis.com/dashboard to customize your tools -->
			<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55882f01094d4b2d" async="async"></script>

			<!-- Go to www.addthis.com/dashboard to customize your tools -->
			<div class="addthis_sharing_toolbox"></div>
			</div>
		</div>
	</div>
	<div class="row hidden-xs">
		<div class="col-sm-12">

			<div id="nav-detail">
				<ul class="nav nav-pills">
					<li><a href="#about" title="About">About</a></li>
				@if(!empty($property->amenities))
					<li><a href="#amenities" title="Amenities">Amenities</a></li>
				@endif
				@if(count($property->blocked_dates) > 0	)
					<li><a href="#calendar" title="Calendar">Calendar</a></li>
				@endif
					<li><a href="#rates" title="Rates &amp; Specials">Rates &amp; Specials</a></li>
				@if(!empty($property->testimonials))
					<li><a href="#reviews" title="Reviews">Reviews</a></li>
				@endif
					<li><a href="#contact" title="Map &amp; Contact">Map &amp; Owner/Manager Info.</a></li>
					<li><a href="#inquiry" title="Inquiry">Inquiry Form</a></li>
				</ul>
			</div>

			<hr class="dashed">

			<script type="text/javascript">
				$(document).ready(function(){
					// pixels from top when fixed menu is triggered
					$("#nav-detail").affix({
						offset: {
							top: 300
						}
					});
					// used to offset height of fixed nav at top
					$('body').scrollspy({
						target: '#nav-detail',
						offset: 90
					});
					// used to offset menu link of fixed nav at top
					var offsetValue = 85;
					$('#nav-detail a').click(function(event) {
						event.preventDefault();
						$($(this).attr('href'))[0].scrollIntoView();
						scrollBy(0, -offsetValue);
					});
				});
			</script>
		</div>
	</div>
	<div class="row">

		<!-- main content -->
		<div class="col-sm-12">

			<!-- about -->
			<div>
				<div class="row">
					<div class="col-sm-7">
						@include('includes.photo-gallery')


						@if(!empty($property->videos))
						<h2>Property/Area Video(s)</h2>

						<div class="row">
						@foreach($property->videos as $i=>$video)
						@if(!empty($video))
							<div class="col-xs-4">
								<a class="popup-video video-link" href="http://www.youtube.com/watch?v={{$video}}">
									<i class="fa fa-youtube-play"></i>
									<img src="http://img.youtube.com/vi/{{$video}}/mqdefault.jpg" alt="" class="img-responsive" />
								</a>
							</div>
						@endif
						@endforeach
						</div>
						@endif

					</div>
					<div class="col-sm-5">
						<a href="#inquiry" style="width: 100%; margin-bottom: 10px; line-height: 2;" class="btn btn-contrast">INQUIRE NOW. BOOK DIRECT. SAVE 30%.</a>
						<ul class="description-list list-unstyled">
						@if(!empty($property->bedrooms))
							<li>Bedrooms - {{strip_tags($property->bedrooms)}}</li>
						@endif
						@if(!empty($property->bathrooms))
							<li>Bathrooms - {{strip_tags($property->bathrooms)}}</li>
						@endif
						@if(!empty($property->max_capacity))
							<li>Sleeps - {{strip_tags($property->max_capacity)}}</li>
						@endif
						@if(in_array('Pets Considered', $property->amenities()->lists('title')->toArray()) !== true)
							<li>Pets Considered</li>
						@else
							<li>No Pets Allowed</li>
						@endif
						@if(!empty($property->on_waters))
							<li class="mt30">On {{strip_tags($property->on_waters)}}</li>
						@endif
						@if(!empty($property->weekly_only))
							<li class="mt30">Weekly only {{strip_tags($property->weekly_only)}}</li>
						@endif
						</ul>

						<hr class="dashed">
						<p style="overflow-wrap: break-word;">{!! nl2br(strip_tags($property->description)) !!}</p>
					</div>
				</div>
			</div>

			<!-- amenities -->
			<div id="amenities">
				<div class="row">
					<div class="col-sm-6">
						@if(!empty($property->unique_features))
							<h2>Unique Features</h2>
							<p style="overflow-wrap: break-word;">{!! nl2br(strip_tags($property->unique_features)) !!}</p>
						@endif
					</div>
					<div class="col-sm-6">
						@if(!empty($property->things_to_do))
							<h2>Things To Do</h2>
							<p style="overflow-wrap: break-word;">{!! nl2br(strip_tags($property->things_to_do)) !!}</p>
						@endif
					</div>
				</div>

				@foreach($amenityCategories as $amenity)
					<div id="amenities-modal-{{$amenity->id}}" class="white-popup-block mfp-hide">
						<button title="Close (Esc)" type="button" class="mfp-close" style="color: grey">×</button>
						<h1>Amenities: {{$amenity->title}}</h1>
						<div class="row">
							<ul>
									@foreach($property->amenities()->where('amenity_category_id', $amenity->id)->get() as $feature)
									<li>{{$feature->title}}</li>
									@endforeach
							</ul>
						</div>
					</div>
				@endforeach

				<div class="row">
					<div class="col-sm-12">
						<h2>Amenities:</h2>
					</div>
					<div class="col-sm-12">
					@php $i = 0; @endphp
					@foreach($amenityCategories as $amenity)
						@if($i % 3 == 0)
							<div class="row">
						@endif
						<div class="col-sm-4">
							<!--<h2 class="popup-modal" href="#amenities-modal-{{$amenity->id}}">{{$amenity->title}}</h2>-->
							<h3>{{$amenity->title}}</h3>
							<div class="row">
							<ul>
									@foreach($property->amenities()->where('amenity_category_id', $amenity->id)->get() as $feature)
									<li>{{$feature->title}}</li>
									@endforeach
							</ul>
						</div>
						</div>
						@if($i % 3 == 2)
							</div>
						@endif
						@php $i++; @endphp
					@endforeach
					@if($i % 3 != 0)
						@php $remainder = $i % 3; @endphp
						@for($remainder; $remainder < 3; $remainder++)
							<div class="col-sm-4">
							</div>
						@endfor
					</div>
					@endif
					</div>
				</div>
			</div>

			<hr class="dashed tall">
			@if(count($property->blocked_dates()->get()) > 0)
			<!-- calendar -->
			<div id="calendar">
				<h2>Calendar</h2>

				<div class="calendar-display-disabled"></div>
				<dl class="calendar-legend">
					<dt class="legend-key">
						<span class="box"></span>
					</dt>
					<dd class="legend-label">Available</dd>
					<dt class="legend-key">
						<span class="box unavailable"></span>
					</dt>
					<dd class="legend-label">Unavailable</dd>
				</dl>

				<div class="row">
					{!! $calHtml !!}
				</div>

				<p><small>* Please contact owner to ensure availability.</small></p>
			</div>

			<hr class="dashed tall">
			@endif

			<!-- rates & specials -->
			<div id="rates">
				<h2>Rates &amp; Specials</h2>
				<div class="row">
				<div class="col-sm-3">
				<p><span style="font-weight: bold;">BOOK DIRECT</span><br>
					@if(!empty($property->min_rates_daily))
					@if(!empty($property->min_rates_daily))${{number_format(strip_tags($property->min_rates_daily), 0, '.', '')}} - 
					@endif${{number_format(strip_tags($property->max_rates_daily), 0, '.', '')}} per night<br>
					@if(!empty($property->min_rates_weekly))${{number_format(strip_tags($property->min_rates_weekly), 0, '.', '')}} - 
						@endif${{number_format(strip_tags($property->max_rates_weekly), 0, '.', '')}} per week<br>

				</p>
				</div>
				<div style="font-style: italic; opacity: 0.4" class="col-sm-3">
				3rd party booking
				<p>@if(!empty($property->min_rates_daily))
					<span style="text-decoration: line-through;">${{number_format(strip_tags($property->min_rates_daily) * 1.3, 0, '.', '')}} - 
					@endif ${{number_format(strip_tags($property->max_rates_daily) * 1.3, 0, '.', '')}}</span> per night<br>
					@if(!empty($property->min_rates_weekly))<span style="text-decoration: line-through;">${{number_format(strip_tags($property->min_rates_weekly) * 1.3, 0, '.', '')}} - 
					@endif ${{number_format(strip_tags($property->max_rates_weekly)* 1.3, 0, '.', '')}}</span> per week
					@else No information available
					@endif

				</div>
				</div>
				<p>{!! nl2br(strip_tags($property->rates)) !!}</p>
			
			@if(!empty($property->special_offers))
				<h2>Woohoo! We are offering vacation rental special(s)</h2>
				{!! nl2br(strip_tags($property->special_offers)) !!}
			@endif
			</div>

			<hr class="dashed tall">

			<!-- reviews -->
			@if(!empty($property->testimonials))
			<div id="reviews">
				<h2>Reviews</h2>

				<blockquote class="review">{!! str_replace("\n",'</blockquote><blockquote class="review">',strip_tags($property->testimonials)) !!}</blockquote>
			</div>

			<hr class="dashed tall">
			@endif

			<!-- map & owner info -->
			<div id="contact">
				<h2>Map &amp; Owner/Manager Info.</h2>

				<div class="row">
					<div class="col-sm-6">
						<h3>Property Details</h3>
						<p>
							{{strip_tags($property->address)}}<br />
							{{strip_tags($property->city)}}, {{$state->abbr}} {{strip_tags($property->zip)}}<br>
							<a href="https://www.google.com/maps/search/?api=1&query={{$property->address_latitude}},{{$property->address_longitude}}" target="_blank">See this location on Google Maps</a>
						</p>

						<p>
							{{strip_tags($property->phone)}}<br>
							{{strip_tags($property->phone_cell)}}
							<span class="help-block"><small>If you call, mention "{{$state->seo_name}}". Visit website (if listed) then return and send your inquiry.</small></span>
						</p>

						<p>
							@if($property->listing_url != '')<a href="{{strip_tags($property->listing_url)}}" onclick="_gaq.push(['_trackEvent','outgoing_links','{{$property->listing_url}})" target="_blank">VISIT PROPERTY WEBSITE</a>@endif
						</p>

						<ul class="list-inline">
						 	@foreach($property->social_media_links as $brand => $link)
						 	@if(!empty($link))
						 		<li>
							 		<a href="{{strip_tags($link)}}"
							 			onclick="_gaq.push(['_trackEvent','outgoing_links','{{$link}}'])"
						 				title="Visit us on Facebook" class="btn btn-social-icon btn-{{$brand}}" target="_blank">
						 				<i class="fa fa-{{$brand}}"></i>
					 				</a>
						 		</li>
					 		@endif
						 	@endforeach
						</ul>
					</div>
					<div class="col-sm-6">
						@if(!empty($property->owner_bios))
							<h3>About the Owner/Manager</h3>

							<p style="overflow-wrap: break-word;">{!! nl2br(strip_tags($property->owner_bios)) !!}</p>
						@endif
					</div>
				</div>
			</div>

			<hr class="dashed tall">

			<div id="inquiry" class="rental-inquiry">
				<br><br>
				<h3 class="mt0">Inquire and book with {{$state->abbr}} rental owners or managers to get the lowest price online.</h3>

					@include('includes.inquiry-form', ['classContainer' => 'col-sm-12'])
				</div>
			</div>

			<div class="hidden-xs hidden-sm">
				<!-- ad -->
				<script type="text/javascript">
					<!--
					google_ad_client = "ca-pub-8772899038103543";
					/* RentWisCabinsDetailsPg3 */
					google_ad_slot = "5230219708";
					google_ad_width = 468;
					google_ad_height = 60;
					//-->
				</script>
				<script type="text/javascript" src="//pagead2.googlesyndication.com/pagead/show_ads.js"></script>
			</div>
		</div>
		<!-- /main content -->

	</div>
</div>
@endsection
