<!DOCTYPE html>
<html>
<head>
	<title>Admin - {{ $state->seo_name }}</title>
	<meta http-equiv="content-language" content="en" />
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />

	<!-- Style, Script, RSS, Icon Links -->
	<link rel="stylesheet" href="/css/foundation-3.1.1-custom/stylesheets/foundation.min.css" type="text/css" />
	<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/themes/cupertino/jquery-ui.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="/js/libs/datepicker/jquery.datepick.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="/js/libs/datepicker/ui-cupertino.datepick.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="/css/admin.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="/css/magnific-popup/magnific-popup.css">
	<script type="text/javascript" src="/js/libs/foundation/foundation.min.js"></script>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/libs/datepicker/jquery.datepick.min.js"></script>
	<script type="text/javascript" src="/js/libs/magnific-popup/jquery.magnific-popup.js"></script>
<META NAME="ROBOTS" CONTENT="NOINDEX, FOLLOW">
</head>

<body>
	@yield('content')
<div class="row">
	<div class="twelve columns text-center">
		<p>Copyright {{ date('Y') }} {{ $state->seo_name }} is a member of JM Creative Group, LLC</p>
	</div>
</div>
@stack('scripts')
<script src={{ asset('js/admin.js') }}></script>
</body>
</html>
