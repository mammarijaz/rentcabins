<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-5213974-
    8"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-5213974-8');
    </script>
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @stack('meta')

    <title>{{ $page->seo_title or $settings->seo_title }}</title>
    <meta name="description" content="{{ $page->seo_description or $settings->seo_description }}">
    <meta name="csrf_token" content="{{ csrf_token() }}">
    <meta name="author" content="Joe Mogensen">
    <meta name="google-site-verification" content="evN9UgQl80_u9kFvjmHeu3ubNuBn3cT2Nu_5xCpZoDk" />
    <meta name="p:domain_verify" content="746e08f043733e3fafb071b74892c245"/>
    <meta name="apple-mobile-web-app-title" content="{{$state->seo_name}}">

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link href="//fonts.googleapis.com/css?family=Arvo:400,700%7CRoboto:400,700%7CDancing+Script" rel="stylesheet">
    <link href="/js/libs/select2/select2.css" rel="stylesheet">
    <link href="/css/select2-bootstrap.css" rel="stylesheet">
    <link href="/css/custom-theme/jquery-ui-1.8.23.custom.css" rel="stylesheet">
    <link href="/css/bootstrap-social.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/magnific-popup/magnific-popup.css">
    <link rel="stylesheet" href="/js/libs/flexslider/flexslider.css" type="text/css" media="screen" />
    @stack('styles')
    <link href="{{ elixir('css/app.css') }}" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="/js/libs/modernizr-2.5.3.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="/js/jquerypp.custom.formParam.js"></script>
    <script src="/js/libs/flexslider/jquery.flexslider.js"></script>
    <script type="text/javascript" src="/js/store.min.js"></script>
    <script>
        $(document).mouseup(function (e) {
                var button = $("#dropdownbtn");
                var container = $("#dropdowncontent");

                if (!button.is(e.target) 
                    && button.has(e.target).length === 0) {
                    container.hide();
                } else {
                    container.toggle();
                }
            });
    </script>
</head>
<body id="page-{{ $templateClass or "default"}}" data-spy="scroll" data-target="#nav-detail">
<noscript><p class="jsdisabled">You currently have JavaScript disabled. {{$state->seo_name}} makes heavy use of JavaScript. You can easily enable it in your browser's preferences.</p></noscript>
<!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->
   <div id="header-container">
    <center>
            <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- 728x90fixed -->
            <ins class="adsbygoogle"
            style="display:inline-block;min-width:400px;max-width:970px;width:100%;height:90px"
            data-ad-client="ca-pub-8772899038103543"
            data-ad-slot="4687709576"></ins>
            <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </center>
    <header class="container">
        <!-- Static navbar -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">
                        <img src="{{ $state->main_logo }}" alt="{{$state->seo_name}}" class="img-responsive">
                    </a>
                </div>
                <div class="navbar-collapse collapse">
                    <div class="top-box col-sm-7 col-md-6 col-lg-5 pull-right">
                        @if($settings->social_media)
                            <ul class="social list-inline pull-right">
                            @foreach($settings->social_media as $brand => $link)
                            <li>
                                <a href="{{$link}}" title="{{$brand}}" target="_blank"
                                    class="btn btn-sm btn-soxial-icon btn-{{$brand}}">
                                    <i class="fa fa-{{$brand}}"></i>
                                </a>
                            </li>
                            @endforeach
                            </ul>
                        @endif
                    </div>
                    <ul class="nav navbar-nav navbar-right">
                        @if(Auth::check())
                            <li><a href="/admin/" title="WI Rental Owner Login">{{Auth::user()->name}}'s Dashboard</a></li>
                        @else
                            <li><a href="/admin/" title="WI Rental Owner Login">Login</a></li>
                        @endif
                        <li><a href="/list-your-rental" title="List Your WI Rentals">List With Us</a></li>
                        {{-- <li><a href="/help" title="Help">Help</a></li> --}}
                        @if($menuStates)
                        <li>
                            <div class="dropdown">
                                <button id="dropdownbtn" class="dropbtn">Explore Other States</button>
                                <div id="dropdowncontent" class="dropdown-content">
                                @foreach($menuStates as $stateObj)
                                    @if($stateObj->name != $state->name && $stateObj->name != '')
                                        <a href="http://{{ $stateObj->domain }}" target="_blank">{{ $stateObj->domain }}</a>
                                    @endif
                                @endforeach
                                </div>
                            </div>
                        </li>
                        @endif
                    </ul>
                </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
        </div>
    </header>
</div>
@if($two_pack == 'true')
    <div class="body-container" style="background-image:url('{{$settings->two_pack}}')">
        @yield('content')
    </div>
@else
    <div class="body-container" style="background-image:url('{{$settings->home_header_image}}')">
        @yield('content')
    </div>  
@endif

<button id="toTop" class="btn btn-contrast">^ back to top</button>

<div class="footer-container">
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="logo">
                        <a href="/">
                            <img src="{{ $state->main_logo }}" alt="{{$state->seo_name}}" class="img-responsive" />
                        </a>
                    </div>
                <p class="text-center">
                    {!! $settings->footer_sidebar_links !!}
                </p>
                </div>
                <div class="col-sm-9">
                    <ul class="footer-nav nav nav-pills mb40">
                        {!! $settings->footer_links !!}
                    </ul>

                    <ul class="list-inline">
                        <li>&copy; 2009-{{ date('Y') }}. All rights reserved. {{ $state->seo_name }}</li>
                        @if($state->id == 1)
                        <li><a href="http://www.bbb.org/wisconsin/business-reviews/rental-vacancy-listing-service/jm-creative-group-llc-in-waunakee-wi-1000010763/" title="A member of JM Creative Group, LLC an A+ BBB Accredited Business.">A member of JM Creative Group, LLC an A+ BBB Accredited Business</a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </footer>
</div>

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <script src="/js/libs/jquery-ui-1.10.1.custom.min.js"></script>
    <script src="/js/libs/select2/select2.js"></script>
    <script src="/js/libs/jquery.placeholder.js"></script>
    <script src="/js/libs/jquery.scrollTo.min.js"></script>
    <script src="/js/libs/jquery.validity.min.js"></script>
    <script src="/js/libs/flexslider/jquery.flexslider-min.js"></script>
    <script src="/js/libs/magnific-popup/jquery.magnific-popup.min.js"></script>
    @stack('scripts')
    <script src="/js/all.js"></script>
    <script type="text/javascript">var switchTo5x=true;</script>
    <script type="text/javascript" src="//w.sharethis.com/button/buttons.js"></script>

    <script type="text/javascript">
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-5213974-1']);
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();
    </script>

    <!-- Google Code for Remarketing Tag -->
    <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 1029276839;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1029276839/?value=0&amp;guid=ON&amp;script=0"/>
    </div>
    </noscript>
</body>
</html>
