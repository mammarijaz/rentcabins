// usage: log('inside coolFunc', this, arguments);
// paulirish.com/2009/log-a-lightweight-wrapper-for-consolelog/
window.log = function f(){ log.history = log.history || []; log.history.push(arguments); if(this.console) { var args = arguments, newarr; args.callee = args.callee.caller; newarr = [].slice.call(args); if (typeof console.log === 'object') log.apply.call(console.log, console, newarr); else console.log.apply(console, newarr);}};

// make it safe to use console.log always
(function(a){function b(){}for(var c="assert,count,debug,dir,dirxml,error,exception,group,groupCollapsed,groupEnd,info,log,markTimeline,profile,profileEnd,time,timeEnd,trace,warn".split(","),d;!!(d=c.pop());){a[d]=a[d]||b;}})
(function(){try{console.log();return window.console;}catch(a){return (window.console={});}}());


// place any jQuery/helper plugins in here, instead of separate, slower script files.

jQuery(document).ready(function($){

/**
  * Select2 - jQuery form plugin - http://ivaynberg.github.com/select2/
  */
	$("#destination").select2({
		placeholder: "select one or more towns",
		allowClear: true
	});
	$("#region").select2({
		placeholder: "or select one or more regions",
		allowClear: true
	});
	$("#setting").select2({
		placeholder: "select one or more settings",
		allowClear: true
	});
	$("#style").select2({
		placeholder: "select one or more styles",
		allowClear: true
	});
	$("#bedrooms").select2({
		placeholder: "bedrooms",
		allowClear: true
	});
	$("#sleeps").select2({
		placeholder: "sleeps",
		allowClear: true
	});
	$("select#amenities").select2({
		placeholder: "Select pet preference & other needs",
		allowClear: true
	});
	$("#state").select2({
		placeholder: "state *",
		allowClear: true
	});

/**
  * jQuery UI
  */
	// Datepicker
	if($('#arrival_date').length){
		$( "#arrival_date, #arrival_date_2" ).datepicker({
			defaultDate: "+0d",
			minDate:+0,
			changeMonth: true,
			onSelect: function() {
				var date = $(this).datepicker('getDate');
				if (date){
					date.setDate(date.getDate() + 1);
					$( "#departure_date" ).datepicker( "option", "minDate", date );
				}
			}
		});
	}
	if($('#departure_date').length){
		$( "#departure_date, #departure_date_2" ).datepicker({
			defaultDate: "+0d",
			minDate:+1,
			changeMonth: true
		});
	}

	$( ".calendar-display" ).datepicker({
			numberOfMonths: 3,
			showButtonPanel: true
		});

	// Slider
	$("#slider").slider({
		range: true,
		values: [17, 67]
	});

	$("#daily-rate").slider({
		range: true,
		min: 0,
		max: 450,
		values: [0, 450],
		step: 50,
		slide: function( event, ui ) {
			$( "#amount-daily" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] + ((ui.values[ 1 ] === 450)?'+':'') );
		}
	});
	$("#amount-daily").val( "$" + $( "#daily-rate" ).slider( "values", 0 ) +
		" - $" + $( "#daily-rate" ).slider( "values", 1 ) + '+' );

	$("#weekly-rate").slider({
		range: true,
		min: 0,
		max: 3000,
		values: [0, 3000],
		step: 250,
		slide: function( event, ui ) {
			$("#amount-weekly").val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] + ((ui.values[ 1 ] === 3000)?'+':'') );
		}
	});
	$("#amount-weekly").val( "$" + $( "#weekly-rate" ).slider( "values", 0 ) +
		" - $" + $( "#weekly-rate" ).slider( "values", 1 ) + '+' );

	//hover states on the static widgets
	$('#dialog_link, ul#icons li').hover(
		function() { $(this).addClass('ui-state-hover'); },
		function() { $(this).removeClass('ui-state-hover'); }
	);

/**
  * jQuery Placeholder plugin - https://github.com/mathiasbynens/jquery-placeholder
  */
	$('input, textarea').placeholder();

/**
  * jQuery Validity plugin - http://validity.thatscaptaintoyou.com/
  */
	$.validity.setup({ outputMode:"label" });

	$("#listrentalform").validity(function() {
		$("#first_name")
			.require();
		$("#last_name")
			.require();
		$("#email")
			.require()
			.match('email');
	});

	$("#subscribeform").validity(function() {
		$("#first_name_subscribe")
			.require();
		$("#last_name_subscribe")
			.require();
		$("#email_subscribe")
			.require()
			.match('email');
	});

	$("#inquiryform").validity(function() {
		$("#first_name")
			.require();
		$("#email")
			.require()
			.match('email');
		$("#state")
			.require();
	});

	window.validateAjaxInquiryForm = function() {

		$.validity.start();

		$("#first_name")
			.require();
		$("#email")
			.require()
			.match('email');
		$("#state")
			.require();

		var result = $.validity.end();

		return result.valid;
	};

});
