jQuery(document).ready(function($){

	$(document).foundationTabs();

	if($('#create-property').length){

		$('.charcount').keyup(function(){
			limitChars($(this).attr('id'), $(this).data('maxlength'), 'charcount-'+$(this).attr('id'));
		});
		// characters remaining code
		function limitChars(textid, limit, infodiv) {
			var text = $('#'+textid).val();
			var textlength = text.length;
			if(textlength > limit)
			{
				$('#' + infodiv).html('(You cannot write more than '+limit+' characters!)');
				$('#'+textid).val(text.substr(0,limit));
				return false;
			}
			else
			{
				$('#' + infodiv).html('(You have '+ (limit - textlength) +' characters left.)');
				return true;
			}
		}
	}

	(function(){
		const today = new Date();
		const day = today.getDate();
		const monthIndex = today.getMonth();
		const year = today.getFullYear();
		const dateContainer = $('#blocked_dates');
		let months_to_show;
		let savedDatesArray = [];

		if(dateContainer.length){
			if(savedDates.length){
				savedDatesArray = savedDates;
			}

			if($(document).innerWidth() > 768){
				months_to_show = [4,3];
				dateContainer.css('margin-left','10%');
			} else {
				months_to_show = 1;
			}

			dateContainer.datepick({
				//closeText: 'Save',
				defaultDate: today,
				multiSelect: 99999,
				multiSeparator: ', ',
				monthsToShow: months_to_show,
				dateFormat: 'yyyy-mm-dd',
				onSelect: function(){
					var selected_dates = dateContainer.datepick('getDate');
					let newDatesArray = [];
					$.each(selected_dates, function(i, selected_date){
						if(typeof(selected_date) === "object") {
							var dateFormatted = $.datepick.formatDate('yyyy-mm-dd', selected_date).toString();
							newDatesArray.push(dateFormatted);
						}
					});
					setDates(newDatesArray);
				},
				onShow: function(){
					$('.datepick-highlight').removeClass('datepick-highlight');
				},
				onChangeMonthYear: function(){
					$('.datepick-highlight').removeClass('datepick-highlight');
				},
			});
			setTimeout(function(){
				setDates(savedDatesArray);
				dateContainer.datepick('setDate', savedDatesArray);
				dateContainer.datepick('showMonth', year, monthIndex);
				$('.datepick-highlight').removeClass('datepick-highlight');
			},1000);

			function setDates(dates){
				$('#blocked_dates_textarea').val(JSON.stringify(dates));
				console.log($('#blocked_dates_textarea').val());
			};
		}
	})();

});
