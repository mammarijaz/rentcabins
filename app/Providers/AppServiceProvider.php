<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Cache::forever('state_settings', State::where('domain', $_SERVER['SERVER_NAME'])->first()::all());
        //
        // Blade::directive('article_and_word', function($word){
        //     switch ($word[0]) {
        //         case 'a': case 'e': case 'i': case 'o': case 'u':
        //         case 'A': case 'E': case 'I': case 'O': case 'U':
        //         return 'An';
        //     }
        //     return 'A';
        // });

        // Form::macro('customSelect', function($name, $values = [], $oldValue = null){
        //     $html = "<select name='{$name}'>";
        //     foreach($values as $label){
        //         $value = trim($label);
        //         $html .= '<option value="'.$value.'"'.($oldValue == $value ? ' selected':'').'>'.$value.'</option>';
        //     }
        //     $html .= "</select>";
        //     return $html;
        // });

        //Since mailgun settings are set in the config/services.php file and DB access creates a circular dependency,
        //override its params here after application starts so mailgun domain/secret can be set on a state-by-state basis.
        if (\Schema::hasTable('states')) {
            $state = \DB::table('states')->where('domain', \Request::server('SERVER_NAME', 'UNKNOWN'))->first();
            if(env('APP_ENV') == 'local'){
                //Use sandbox environment if on localhost or dev instance
                $mailgunParams = array(
                        'domain' => 'sandboxf611370632894204a15fd525791085be.mailgun.org',
                        'secret' => 'key-b89b99f854bfc249d5287baf5ebf56eb'
                    );
                    \Config::set('mailgun', $mailgunParams);
                    //These params will generally be set properly in a local environment, but setting them here to maintain consistency with the code below.
                    $config = array(
                        'driver' => 'smtp',
                        'host' => 'smtp.mailgun.org',
                        'port' => '587',
                        'from' => array('address' => 'taucesauce@gmail.com', 'name' => 'Testin This'),
                        'encryption' => null,
                        'username' =>"mammarijaz@gmail.com",
                        'password' => "Admin-3221",
                        'sendmail' => '/usr/sbin/sendmail -bs',
                        'pretend' => false
                    );
                    \Config::set('mail',$config);
            } else {
                if($state){
                    $mailgunParams = array(
                        'domain' => strtolower($state->domain),
                        'secret' => $state->mail_key
                    );
                    \Config::set('mailgun', $mailgunParams);
                    //These params will generally be set properly in a local environment, but setting them here to maintain consistency with the code below.
                    $config = array(
                        'driver' => 'smtp',
                        'host' => 'smtp.mailgun.org',
                        'port' => '587',
                        'from' => array('address' => 'info@rentwisconsincabins.com', 'name' => 'Testin This'),
                        'encryption' => null,
                        'username' => $state->mail_username,
                        'password' => $state->mail_password,
                        'sendmail' => '/usr/sbin/sendmail -bs',
                        'pretend' => false
                    );
                    \Config::set('mail',$config);
                }
            }
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {   
        
    }
}
