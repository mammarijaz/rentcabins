<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyPhoto extends Model
{
    protected $table = "property_photos";
    protected $fillable = ['property_id', 'image_path', 'description', 'photo_order'];

    public function property(){
    	return $this->belongsTo('App\Model\Property');
    }
}
