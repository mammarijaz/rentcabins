<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CellCarrier extends Model
{
    protected $table = 'cell_carriers';
    public $timestamps = false;
}
