<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    //
    protected $casts = [
    	'home' => 'boolean',
    	'featured' => 'boolean',
    ];
    protected $table = 'pages';
    protected $fillable = ['state_id', 'title', 'slug', 'type','featured', 'header_text', 'content', 'home', 'featured', 'thumb_image',
    	'seo_title', 'seo_description'];
}
