<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class State extends Model
{
    //
    use SoftDeletes;
    protected $table = 'states';
    protected $hidden = [];
    protected $fillable = [
    	'id',
        'name',
        'abbr',
        'domain',
        'main_logo',
    ];

    public function amenities(){
        return $this->belongsToMany('App\Models\Amenity', 'amenities_states');
    }

    public function propertyStyles(){
        return $this->belongsToMany('App\Models\PropertyStyles', 'state_property_styles');
    }

    public function propertySettings(){
        return $this->belongsToMany('App\Models\PropertySettings', 'state_property_settings');
    }

    public function settings(){
        return $this->hasOne('App\Models\Settings', 'state_id');
    }
}
