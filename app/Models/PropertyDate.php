<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyDate extends Model
{
    //
    protected $table = 'property_dates';
    public $timestamps = false;
    protected $hidden = ['id'];
    protected $fillable = ['blocked_date'];
}
