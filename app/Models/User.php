<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use SoftDeletes;
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'id',
       'name',
       'email',
       'to_email',
       'phone',
       'address',
       'status',
       'role_id',
       'state_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'role_id', 'state_id',
    ];

    public function isAdmin(){
        return $this->role_id <= 2;
    }

    public function isSuperAdmin(){
      return $this->role_id == 1;
    }
    public function properties(){
      return $this->hasMany('App\Models\Property');
    }
    public function photo(){
      return $this->hasOne('App\Models\UserPhoto');
    }
}
