<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPhoto extends Model
{
    protected $table = "users_photos";
    protected $fillable = ['user_id', 'filename', 'image_path'];

    public function user(){
    	return $this->belongsTo('App\Model\User');
    }
}
