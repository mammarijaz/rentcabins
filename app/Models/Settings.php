<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    //
    protected $table = 'state_settings';
    public $timestamps = false;
    protected $casts = ['social_media' => 'array'];
    protected $fillable = ['state_id', 'google_analytics', 'seo_title', 'seo_description', 'home_sidebar', 'home_header',
    	'home_header_image', 'home_content','home_bottom_content', 'footer_links', 'footer_sidebar_links', 'updated_at', 'created_at',
		'social_media', 'from_email', 'latitude', 'longitude', 'map_key', 'mail_username', 'mail_password', 'mail_key'];
}
