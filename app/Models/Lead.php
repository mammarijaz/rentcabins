<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
    //
    protected $table = 'leads';
    protected $fillable = ['name', 'property_id', 'phone', 'email', 'email_optin', 'personalization', 'selected_results', 'search_info', 'ts_create'];

    public function properties(){
    	return $this->BelongsToMany('App\Models\Property');
    }
}
