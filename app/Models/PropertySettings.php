<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertySettings extends Model
{
    //
    protected $table = 'property_settings';
    public $timestamps = false;
    protected $fillable = ['id', 'setting'];

    public function properties(){
    	return $this->hasMany('App\Models\Property', 'property_property_settings');
    }

    public function states(){
        return $this->belongsToMany('App\Models\State', 'state_property_settings');
    }
}
