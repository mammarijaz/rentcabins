<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PagePhoto extends Model
{
    protected $table = "page_photos";
    protected $fillable = ['page_id', 'image_path', 'description', 'photo_order'];

    public function resort(){
    	return $this->belongsTo('App\Model\Page');
    }
}
