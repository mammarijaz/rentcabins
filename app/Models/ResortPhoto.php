<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResortPhoto extends Model
{
    protected $table = "resort_photos";
    protected $fillable = ['resort_id', 'image_path', 'description', 'photo_order'];

    public function resort(){
    	return $this->belongsTo('App\Model\Resort');
    }
}
