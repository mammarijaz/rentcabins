<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AmenityCategory extends Model
{
    //
    protected $table = "amenity_categories";
    public $timestamps = false;
    protected $fillable = ['id', 'title', 'category_order'];
    protected $hidden = ['created_at', 'pivot', 'updated_at'];

    public function amenities(){
        return $this->hasMany('App\Models\Amenity');
    }
}
