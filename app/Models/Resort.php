<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class Resort extends Model
{
    protected $table = "resorts";
    protected $fillable = ['name', 'address', 'city', 'state', 'zipcode', 'website', 'description', 'user_id', 'state_id', 'location_id', 'phone'];
    
    public function properties(){
    	return $this->hasMany('App\Models\Property');
    }

    public function photos(){
    	return $this->hasMany('App\Models\ResortPhoto');
    }

    public function location(){
    	return $this->belongsTo('App\Models\Location');
    }
}
