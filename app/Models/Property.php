<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Resort;

class Property extends Model
{
    //
    use SoftDeletes;
    protected $table = "cabins";

    protected $casts = [
       'allow_pets'=>'boolean',
       'allow_accessible'=>'boolean',
       'accepts_credit_cards'=>'boolean',
       'featured'=>'boolean',
       // 'settings' => 'array',
       // 'styles' => 'array',
       // 'features' => 'array',
       'photos' => 'array',
       // 'blocked_dates' => 'array',
       'videos' => 'array',
       'social_media_links' => 'array',
    ];

    protected $guarded = [];

    protected $fillable = [
        'status',
        'user_id',
        'name',
        'description',
        'seo_description',
        'old_id',
        'owner_bios',
        'email',
        'phone',
        'address',
        'city',
        'address_location_id',
        'zip',
        'state_id',
        'location_id',
        'listing_url',
        // 'blocked_dates',
        'on_waters',
        // 'settings',
        // 'styles',
        'featured',
        'featured_photo',
        'featured_description',
        'allow_pets',
        'allow_accessible',
        'accepts_credit_cards',
        'max_rates_daily',
        'max_rates_weekly',
        'rates',
        'max_capacity',
        'bedrooms',
        'bathrooms',
        'min_rates_daily',
        'min_rates_weekly',
        'address_longitude',
        'address_latitude',
        'resort_id',
        'unique_features',
        'things_to_do',
        'social_media_links',
        'phone_cell',
        'phone_cell_carrier',
        'special_offers',
        'features',
        'testimonials',
        'videos',
        'weekly_only',
    ];

    protected $hidden = ['user_id', 'created_at', 'deleted_at', 'old_id', 'state_id', 'location_id'];

    // public $bedroomSizes = ["1","2","3","4","5","6","7","8","9","10+"];
    // public $bathroomSizes = ["1","2","3","4","5","6","7","8","9","10+"];
    // public $maxCapacityList = ["2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20+"];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function state(){
        return $this->belongsTo('App\Models\State');
    }

    public function leads(){
        return $this->hasMany('App\Models\Lead');
    }

    public function regions(){
        return $this->belongsToMany('App\Models\Region');
    }

    public function amenities(){
        return $this->belongsToMany('App\Models\Amenity', 'amenities_property');
    }

    public function location(){
        return $this->belongsTo('App\Models\Location');
    }

    public function resort(){
        return $this->belongsTo('App\Models\Resort');
    }

    public function totalLeads(){
        return $this->leads()->selectRaw('id, count(*) as aggregate')->groupBy('id');
    }

    public function settings(){
        return $this->belongsToMany('App\Models\PropertySettings');
    }

    public function styles(){
        return $this->belongsToMany('App\Models\PropertyStyles');
    }
    public function blocked_dates(){
        return $this->hasMany('App\Models\PropertyDate');
    }
    public function property_photos(){
        return $this->hasMany('App\Models\PropertyPhoto');
    }

}
