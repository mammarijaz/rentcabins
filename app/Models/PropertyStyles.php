<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyStyles extends Model
{
    //
    protected $table = 'property_styles';
    public $timestamps = false;
    protected $hidden = ['id'];
    protected $fillable = ['style'];

    public function properties(){
    	return $this->hasMany('App\Models\Property', 'property_property_styles');
    }

    public function states(){
        return $this->belongsToMany('App\Models\State', 'state_property_styles');
    }
}
