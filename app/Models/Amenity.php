<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Amenity extends Model
{
    //
    protected $table = "amenities";
    public $timestamps = false;
    protected $fillable = ['id','title', 'amenity_category_id'];
    protected $hidden = ['created_at', 'pivot', 'updated_at'];

    public function properties(){
    	return $this->hasMany('App\Models\Property', 'amenities_property');
    }

    public function states(){
    	return $this->belongsToMany('App\Models\State', 'amenities_states');
    }

    public function category(){
        return $this->belongsTo('App\Models\AmenityCategory', 'amenities_subitems_amenities_id_foreign');
    }
}
