<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    //
    protected $table = 'locations';
    protected $fillable = ['name', 'slug', 'state_id', 'region_id', 'description', 'features', 'seo_title', 'seo_description', 'header_text'];
    protected $hidden = ['id', 'created_at', 'region_id', 'state_id', 'updated_at'];

    public function state(){
    	return $this->belongsTo('App\Models\State');
    }

    public function properties(){
    	 return $this->hasMany('App\Models\Property');
    }
}
