<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    //
    protected $table = 'regions';
    protected $fillable = ['name', 'slug', 'state_id'];

    public function state(){
    	return $this->belongsTo('App\Models\State');
    }

    public function locations(){
    	return $this->hasMany('App\Models\Location');
    }

    public function properties(){
    	return $this->belongsToMany('App\Models\Property');
    }
}
