<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\State;
use DB;

class StateMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
     
        
        
//        $name=ucfirst( trim($_SERVER['REQUEST_URI'], "/") );
        if( !isset($_GET['state']) )
        {
            $adminState= session('admin_state');
            if(empty($adminState) || $adminState==NULL )
            {
                $state = State::where('name', $_GET['state'] )->first();
                if( $state === NULL  )
                {
                    $state = State::where('name',"Montana")->first();           
                }
            }
            else
            {
                $state=$adminState;
            }
        }
        else
        {
            $state = State::where('name', $_GET['state'] )->first();
            if( $state === NULL  )
            {
                    $state = State::where('name',"Montana")->first();           
            }

        }    
        session(['admin_state' => $state]);

        if(empty($state)){
            print_r($_SERVER['SERVER_NAME']);
            print_r("Connected to: " . DB::connection()->getDatabaseName());
            abort(503);
        }
        $menuStates = State::orderBy('name','ASC')->take(null)->get();
        $request->server->set('REQUEST_URI', '/');
        $request->attributes->add(['state' => $state]);
        view()->share('menuStates' , $menuStates);
        view()->share('state' , $state);
        return $next($request);
    }
}
