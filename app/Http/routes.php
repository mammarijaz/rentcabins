<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
if(version_compare(PHP_VERSION, '7.2.0', '>=')) {
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
}
Route::group(['middleware' => 'state'], function(){
	// Authentication Routes...
	//Route::controller('api', 'ApiController');

	Route::get('/api/property-photos', 'ApiController@getPropertyPhotos');
	Route::get('/api/properties', 'ApiController@getProperties');
	Route::get('/api/all-properties', 'ApiController@getAllProperties');

	Route::get('login', 'Auth\AuthController@showLoginForm');
	Route::post('login', 'Auth\AuthController@login');
	Route::get('logout', 'Auth\AuthController@logout');

	Route::get('/', [ 'as' => 'home', 'uses'=>'PagesController@getIndex']);
	Route::get('/rental/{publicId}', 'PropertyController@getProperty');
	Route::post('/inquiry', 'PropertyController@postInquiry');
	Route::post('/rental-inquiry', 'PagesController@postRentalInquiry');
	Route::get('/location/{slug}', 'LocationController@getLocation');
	Route::get('resort/{publicId}', 'ResortController@getResort');
	//Ajax retrieval routes...

	// Registration Routes...
	// Route::get('register', 'Auth\AuthController@showRegistrationForm');
	// Route::post('register', 'Auth\AuthController@register');

	// Password Reset Routes...
	Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
	Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
	Route::post('password/reset', 'Auth\PasswordController@reset');

	Route::group(['middleware' => ['auth','user'], 'namespace' => 'Admin', 'prefix' => 'admin'], function(){
		Route::resource('properties', 'PropertiesController');
		Route::resource('regions', 'RegionsController');
		Route::resource('locations', 'LocationsController');
		Route::resource('resorts', 'ResortsController');
		Route::resource('amenities', 'AmenitiesController', ['only' => ['destroy', 'store']]);
		Route::resource('amenity-categories', 'AmenitiesCategoryController', ['only' => ['destroy', 'store']]);
		Route::resource('property-settings', 'PropertySettingsController', ['only' => ['destroy', 'store']]);
		Route::resource('property-styles', 'PropertyStylesController', ['only' => ['destroy', 'store']]);
		Route::resource('inquiries', 'InquiriesController');
		Route::resource('leads', 'LeadsController');
		Route::resource('pages', 'PagesController', ['except' => ['show']]);
		Route::resource('users', 'UsersController');
		Route::controller('/', 'AdminController');
		Route::get('{slug}', 'AdminController@getPage');
	});


	Route::get('list-your-rental', 'PagesController@getListInquiry');
	Route::get('thanks', 'PagesController@getThanks');
	Route::get('rental-thanks', 'PagesController@getRentalThanks');
	Route::get('{slug}', [
    	'uses' => 'PagesController@getPage'
	])->where('slug', '([A-Za-z0-9\-\/]+)');
	
	Route::get('/sitemap.xml', 'SitemapController@index');
});
