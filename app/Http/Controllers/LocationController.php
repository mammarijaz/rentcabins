<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Location;
use App\Models\Page;
use App\Models\Settings;
use App\Models\Property;

class LocationController extends Controller
{
    //
    public function getLocation($slug){
    	//Check if City
		$location = Location::where('slug', $slug)->where('state_id', $this->state->id)->with('properties')->firstOrFail();
        //Another quick fix for the address relationship, fix later after pivot table
        foreach(Property::where('address_location_id', $location->id)->get() as $address_property){
            $location->properties->push($address_property);
        }
        foreach($location->properties as $property){
            $petPolicy = "NO Pets Allowed";
                foreach($property->amenities as $amenity){
                    if($amenity->title == "Pets Considered"){
                        $petPolicy = "Pets Considered";
                        break;
                    }
                }
                $property->{"pet_policy"} = $petPolicy;
        }
    	$page = new Page;
    	$page->title = $location->name;
    	$page->seo_description = 'Learn about our vacation experience at {{$this->state->seo_name}}; a website devoted to {{$this->state->abbr}} cabins, cottages, lodges and homes.';
    	$settings = Settings::where('state_id', $this->state->id)->first();
    	return view('location-single', compact('location', 'page', 'settings'));
    }
}
