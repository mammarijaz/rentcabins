<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\UserPhoto;

class UsersController extends Controller
{

    public function __construct(Request $request){
        parent::__construct($request);
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $users = User::where('role_id', '>=', 2)->where('state_id', $this->state->id)->get();
        return view('admin.users', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $user = new User;
        return view('admin.user-form', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required',
            'password' => 'min:6',
            'email' => 'email|unique:users',
            'password2' => 'same:password',
            'role_id' => 'not_in:[1]'
        ]);

        $user = User::create($request->except(['password']));
        //handle uploaded photos
        $image = $request->file('user_photo');
        if(!is_null($image))
        {
            $imageName = str_replace(' ', '', $image->getClientOriginalName());
            $imageFilename = time() . '.' . $imageName;
            $s3Ref = \Storage::disk('s3');
            $filepath = '/images/'.$this->state->name.'/users/'.$user->name.'/'.$imageFilename;
            $s3Ref->put($filepath, fopen($image, 'r+'), 'public');
            UserPhoto::create(['user_id' => $user->id, 'filename' => $imageFilename, 'image_path' => $filepath]);
        }

        if(!empty($request->input('password'))){
            $user->password = bcrypt($request->input('password'));
            $user->save();
        }

        return redirect('admin/users')->with('success', 'New User Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user = User::findOrFail($id);
        return view('admin.user-form', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'name' => 'required',
            'password' => 'min:6',
            'email' => 'email',
            'password2' => 'same:password',
        ]);

        $user = User::findOrFail($id);

        //handle uploaded photos
        $image = $request->file('user_photo');
        if(!is_null($image))
        {
            $imageName = str_replace(' ', '', $image->getClientOriginalName());
            $imageFilename = time() . '.' . $imageName;
            $s3Ref = \Storage::disk('s3');
            $filepath = '/images/'.$this->state->name.'/users/'.$user->name.'/'.$imageFilename;
            $s3Ref->put($filepath, fopen($image, 'r+'), 'public');
            UserPhoto::create(['user_id' => $user->id, 'filename' => $imageFilename, 'image_path' => $filepath]);
        }
        $user->fill($request->except(['password']));
        if(!empty($request->input('password'))){
            $user->password = bcrypt($request->input('password'));
        }
        $user->save();

        return redirect('admin/users')->with('success', 'User Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = User::find($id);
        $user->delete();
        return redirect('admin/users')->with('success', 'User Deleted!');
    }
}
