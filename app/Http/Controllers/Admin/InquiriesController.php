<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Auth;

use App\Models\User;
use App\Models\Property;

class InquiriesController extends Controller
{
    public function __construct(Request $request){
        parent::__construct($request);
        $this->middleware('admin',['except' => ['index']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $isAdmin = Auth::user()->isAdmin();
        $properties = null;
        $totalLeads = 0;
        if($request->input('filter_fkid_users')){
            if($isAdmin || $request->input('filter_fkid_users') == Auth::user()->id){
            $properties = Property::where('user_id', $request->input('filter_fkid_users'))->get();
            $totalLeads = DB::table('leads')
                ->join('cabins', 'leads.property_id', '=', 'cabins.id')
                ->join('users', 'cabins.user_id', '=', 'users.id')
                ->where('users.id', $request->input('filter_fkid_users'))
                ->count();
            }
        }
        if(Auth::user()->isAdmin()){
            $users = User::where('role_id', 3)->get();
        } else {
            $users[0] = Auth::user();
        }
        return view('admin.inquiries', compact('users', 'properties', 'totalLeads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
