<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Lead;
use App\Models\Page;
use App\Models\Settings;
use App\Models\Amenity;
use App\Models\AmenityCategory;
use App\Models\State;
use App\Models\PropertySettings;
use App\Models\PropertyStyles;

class AdminController extends Controller
{
    //
    public function __construct(Request $request){
        parent::__construct($request);
        $this->middleware('admin',['only' => ['getOptins', 'getSettings', 'postSettings', 'postToggleAmenityFilter']]);
    }

    public function uploadFileToS3(Request $request, string $existingFilename = null, string $requestVar) {
        $image = $request->file($requestVar);
        $s3 = \Storage::disk('s3');
        if(!is_null($existingFilename)) {
            $path = '/images/'.$this->state->name.'/headers/'. $existingFilename;
           
            if($s3->exists($path)) {
                $s3->delete($path);
            }
        }
        if(!is_null($image)) {
            $name = str_replace(' ', '', $image->getClientOriginalName());
            $imageFilename = uniqid() . '.' . $name;
            $filepath = '/images/'.$this->state->name.'/headers/'.$imageFilename;
            $s3->put($filepath, fopen($image, 'r+'), 'public');
        }

        return env('S3_IMAGE_PATH') . $this->state->name.'/headers/'.$imageFilename;
    }

    public function getIndex(){
    	return view('admin.index');
    }

    // public function getPage($slug){
    //     $page = Page::where('state_id', $this->state->id)->where('slug', $slug)->firstOrFail();
    //     return view('admin.full-width', compact('page'));
    // }

    public function getLeads(){
    	// $leads = Lead::where('state_id', $this->state->id)->where('slug', 'opt-ins')->first();
    	return view('admin.leads');
    }

    public function getAdvertisingOpportunities(){
        $page = Page::where('state_id', $this->state->id)->where('slug', 'advertise')->firstOrFail();
        return view('admin.full-width', compact('page'));
    }

    public function getFaq(){
        $page = Page::where('state_id', $this->state->id)->where('slug', 'faq')->firstOrFail();
        return view('admin.full-width', compact('page'));
    }

    public function getOptions(){
        $state = State::find($this->state->id);
        $categories = AmenityCategory::orderBy('category_order', 'asc')->get();
        $amenities = Amenity::all();
        $propertySettings = PropertySettings::all();
        $propertyStyles = PropertyStyles::all();
        return view('admin.options', compact('categories', 'amenities', 'propertySettings', 'propertyStyles'));
    }

    public function getSettings(){
        $state = State::find($this->state->id);
        $settings = Settings::firstOrNew(['state_id' => $this->state->id]);
        $amenities = Amenity::all();
        $propertySettings = PropertySettings::all();
        $propertyStyles = PropertyStyles::all();
        $categories = AmenityCategory::all();
        return view('admin.settings-form', compact('state', 'settings', 'amenities', 'propertySettings', 'propertyStyles', 'categories', 'activeIds'));
    }

    public function postToggleAmenityFilter($amenityID) {
        $amenity = Amenity::find($amenityID);
        $amenity->is_searchable = $amenity->is_searchable ? 0 : 1;
        $amenity->save();
        $returnString = $amenity->title . " updated!";
        return redirect('admin/options#tab1')->with('success', $returnString);
    }

    public function postToggleSettingsFilter($settingID) {
        $setting = PropertySettings::find($settingID);
        $setting->is_searchable = $setting->is_searchable ? 0 : 1;
        $setting->save();
        $returnString = $setting->setting . " updated!";
        return redirect('admin/options#tab2')->with('success', $returnString);
    }

    public function postToggleStylesFilter($styleID) {
        $style = PropertyStyles::find($styleID);
        $style->is_searchable = $style->is_searchable ? 0 : 1;
        $style->save();
        $returnString = $style->style . " updated!";
        return redirect('admin/options#tab3')->with('success', $returnString);
    }
    public function postEditSetting(Request $request, $id){
        $returnString = '';
        $returnTab = '';
        switch ($request->input('settingType')) {
            case 'category':
                $category = AmenityCategory::find($id);
                $category->title = $request->input('newName');
                $category->category_order = $request->input('newOrder');
                $category->save();
                $returnString = $category->title . ' updated!';
                $returnTab = '#tab0';
                break;
            case 'amenity':
                $amenity = Amenity::find($id);
                $amenity->title = $request->input('newName');
                $amenity->save();
                $returnString = $amenity->title . " updated!";
                $returnTab = '#tab1';
                break;
            case 'setting':
                $setting = PropertySettings::find($id);
                $setting->setting = $request->input('newName');
                $setting->save();
                $returnString = $setting->setting . ' updated!';
                $returnTab = '#tab2';
                break;
            case 'style':
                $style = PropertyStyles::find($id);
                $style->style = $request->input('newName');
                $style->save();
                $returnString = $style->style . ' updated!';
                $returnTab = '#tab3';
                break;
            default:
                return redirect('admin/options')->with('success', 'Unknown setting type');
        }
        return redirect('admin/options'.$returnTab)->with('success', $returnString);
    }
    public function postSettings(Request $request){
        $this->validate($request, [
            'from_email' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'state_id' => 'unique:settings',
        ]);

        $settings = Settings::create($request->except(['__token', 'home_header_image', 'main_logo']));
        if($request->hasFile('home_header_image')){
            $filename = uniqid().'.'.$request->file('home_header_image')->guessExtension();
            $uploadSuccess = $this->uploadFileToS3($request, $settings->home_header_image, 'home_header_image');
            if($uploadSuccess){
                $settings->home_header_image = $uploadSuccess;
            }
        }
        if($request->hasFile('two_pack')){
            $filename = uniqid().'.'.$request->file('two_pack')->guessExtension();
            $uploadSuccess = $this->uploadFileToS3($request, $settings->two_pack, 'two_pack');
            if($uploadSuccess){
                $settings->two_pack = $uploadSuccess;
            }
        }
        if($request->hasFile('main_logo')){
            $filename = uniqid().'.'.$request->file('main_logo')->guessExtension();
            $uploadSuccess = $this->uploadFileToS3($request, $settings->home_header_image, 'main_logo');
            if($uploadSuccess){
                $localState = State::find($this->state->id);
                $localState->main_logo = $uploadSuccess;
                $localState->save();
            }
        }
        return redirect('admin/settings')->with('success', 'Settings saved!');
    }

    public function postStateParams(Request $request){
        $state = State::find($this->state->id);
        $state->map_key = $request->map_key;
        $state->mail_key = $request->mail_key;
        $state->mail_username = $request->mail_username;
        $state->mail_password = $request->mail_password;
        $state->save();
        return redirect('admin/settings')->with('success', 'Keys updated!');
    }

    public function postStateAmenities(Request $request){
        $state = State::find($this->state->id);
        $activeAmenities = Amenity::all()->filter(function($value, $key){
            return $value->states()->where('id', $this->state->id)->exists();
        });
        $activeIds = array();
        foreach($activeAmenities as $active){
            $activeIds[] = $active->id;
        }

        $newChecked = array_diff($request->amenities, $activeIds);
        $removedChecked = array_diff($activeIds, $request->amenities);
        if(count($removedChecked) > 0)
            $state->amenities()->detach($removedChecked);
        if(count($newChecked) > 0)
            $state->amenities()->attach($newChecked);
        return redirect('admin/settings')->with('success', 'Amenities updated!');
    }

    public function postStateSettings(Request $request){
        $state = State::find($this->state->id);
        $activeSettings = PropertySettings::all()->filter(function($value, $key){
            return $value->states()->where('id', $this->state->id)->exists();
        });
        $activeIds = array();
        foreach($activeSettings as $active){
            $activeIds[] = $active->id;
        }

        $newChecked = array_diff($request->settings, $activeIds);
        $removedChecked = array_diff($activeIds, $request->settings);
        if(count($removedChecked) > 0)
            $state->propertySettings()->detach($removedChecked);
        if(count($newChecked) > 0)
            $state->propertySettings()->attach($newChecked);
        return redirect('admin/settings')->with('success', 'Property Settings updated!');
    }

    public function postStateStyles(Request $request){
        $state = State::find($this->state->id);
        $activeStyles = PropertyStyles::all()->filter(function($value, $key){
            return $value->states()->where('id', $this->state->id)->exists();
        });
        $activeIds = array();
        foreach($activeStyles as $active){
            $activeIds[] = $active->id;
        }

        $newChecked = array_diff($request->styles, $activeIds);
        $removedChecked = array_diff($activeIds, $request->styles);
        if(count($removedChecked) > 0)
            $state->propertyStyles()->detach($removedChecked);
        if(count($newChecked) > 0)
            $state->propertyStyles()->attach($newChecked);
        return redirect('admin/settings')->with('success', 'Property Styles updated!');
    }

    public function putSettings(Request $request){
        $this->validate($request, [
            'from_email' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'state_id' => 'required',
        ]);
        $settings = Settings::where('state_id', $this->state->id)->first();
        $settings->fill($request->except(['__token', 'home_header_image', 'main_logo']));
        if($request->hasFile('home_header_image')){
            $newHeaderFilename = $this->uploadFileToS3($request, $settings->home_header_image, 'home_header_image');
            $settings->home_header_image = $newHeaderFilename;
            //$uploadSuccess = $request->file('home_header_image')->move(public_path().'/images/', $filename);
            // if($uploadSuccess){
            //     $settings->home_header_image = $filename;
            // }
        }

        if($request->hasFile('two_pack')){
            $filename = uniqid().'.'.$request->file('two_pack')->guessExtension();
            $uploadSuccess = $this->uploadFileToS3($request, $settings->two_pack, 'two_pack');
            if($uploadSuccess){
                $settings->two_pack = $uploadSuccess;
            }
        }

        if($request->hasFile('main_logo')){
            $filename = uniqid().'.'.$request->file('main_logo')->guessExtension();
            $uploadSuccess = $this->uploadFileToS3($request, $settings->home_header_image, 'main_logo');
            if($uploadSuccess){
                $localState = State::find($this->state->id);
                $localState->main_logo = $uploadSuccess;
                $localState->save();
            }
        }
        $settings->save();
        return redirect('admin/settings')->with('success', 'Settings saved!');
    }
}
