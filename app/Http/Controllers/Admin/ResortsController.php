<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Contracts\Filesystem\Filesystem;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Location;
use App\Models\Resort;
use App\Models\ResortPhoto;
use App\Models\User;

use Hashids;

class ResortsController extends Controller
{
    public function __construct(Request $request){
        parent::__construct($request);
        $this->middleware('admin');
    }

    public function uploadFileToS3(Request $request){
        $images = $request->file('image');
        $filenames = array();
        foreach($images as $image){
            if(!is_null($image)) {
                $name = str_replace(' ', '', $image->getClientOriginalName());
                $imageFilename = uniqid() . '.' . $name;
                array_push($filenames, $imageFilename);
                $s3 = \Storage::disk('s3');
                $filepath = '/images/'.$this->state->name.'/resorts/'.$request->name.'/'.$imageFilename;
                $s3->put($filepath, fopen($image, 'r+'), 'public');
            }
        }

        return $filenames;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $resorts = Resort::where('state_id', $this->state->id)->get();
        return view('admin.resorts', compact('resorts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $resort = new Resort;
        $owners = User::where('role_id', 3)->where('state_id', $this->state->id)->lists('name', 'id');
        $locations = Location::where('state_id', $this->state->id)->lists('name', 'id');
        return view('admin.resort-form', compact('resort', 'owners', 'locations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required',
            'state_id' => 'required',
            'user_id' => 'required',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zipcode' => 'required',
            'website' => 'url'
        ]);

        $resort = Resort::create($request->all());
        $id = $resort->id;

        $imageNames = $this->uploadFileToS3($request);
        $photosToInsert = array();
        $photoOrder = 1;
        foreach($imageNames as $key => $imageName){
            $photoLocation =  env('S3_IMAGE_PATH') . $this->state->name . '/resorts/' .$request->name . '/' . $imageName;
            array_push($photosToInsert, ['resort_id' => $id, 'image_path' => $photoLocation, 'filename'=> $imageName, 'description' => $request->newImageDescription[$key], 'photo_order' => $photoOrder]);
            $photoOrder++;
        }
        $resort->public_id = strtolower(str_replace('+', '-', urlencode($resort->name) . '-' . Hashids::encode($resort->id)));
        $resort->save();
        ResortPhoto::insert($photosToInsert);
        return redirect('admin/resorts')->with('success', 'Resort Saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $resort = Resort::findOrFail($id);
        $owners = User::where('role_id', 3)->where('state_id', $this->state->id)->lists('name', 'id');
        $locations = Location::where('state_id', $this->state->id)->lists('name', 'id');
        $photos = ResortPhoto::where('resort_id',$id)->orderBy('photo_order')->get();
        return view('admin.resort-form', compact('resort', 'owners', 'locations', 'photos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $this->validate($request, [
            'name' => 'required',
            'state_id' => 'required',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zipcode' => 'required',
            'website' => 'url'
        ]);
        switch($request->submitbutton) {
            case 'Save':
                $resort = Resort::FindOrFail($id);
                $resort->fill($request->all());
                $resort->save();
                $imageNames = $this->uploadFileToS3($request);
                $photosToInsert = array();
                $photoOrder = ResortPhoto::where('resort_id', $id)->max('photo_order');
                $photoOrder++;
                if(count($imageNames) > 0) {
                    foreach($imageNames as $key => $imageName){
                        $photoLocation =  env('S3_IMAGE_PATH') . $this->state->name . '/resorts/' .$resort->name . '/' . $imageName;
                        array_push($photosToInsert, ['resort_id' => $id, 'image_path' => $photoLocation, 'filename'=> $imageName, 'description' => $request->newImageDescription[$key], 'photo_order' => $photoOrder]);
                        $photoOrder++;
                    }
                }

                if(count($request->existingDescription) > 0){
                    foreach($request->existingDescription as $key => $description){
                        $photo = ResortPhoto::FindOrFail($key);
                        $photo->description = $description;
                        $photo->save();
                    }
                }
                
                if(count($request->order) > 0){
                    foreach($request->order as $key => $newOrder){
                        $photo = ResortPhoto::FindOrFail($key);
                        $photo->photo_order = $newOrder;
                        $photo->save();
                    }
                }
                ResortPhoto::insert($photosToInsert);
                return redirect('admin/resorts')->with('success', 'Resort saved!');
            default:
                if(isset($request->submitbutton)) {
                    $deletedPhotoOrder = ResortPhoto::FindOrFail($request->submitbutton)->photo_order;
                    $this->destroyPhoto($request->submitbutton);
                    $updateOrders = ResortPhoto::where([
                        ['resort_id', '=', $id],
                        ['photo_order', '>', $deletedPhotoOrder]
                        ])->get();
                    if(count($updateOrders) > 0){
                        foreach ($updateOrders as $photoToUpdate) {
                            $photoToUpdate->photo_order -= 1;
                            $photoToUpdate->save();
                        }
                    }
                    return redirect('admin/resorts/'.$id.'/edit')->with('success', 'Photo Deleted!');
                }
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $resort = Resort::find($id);
        $resort->delete();
        return redirect('admin/resorts')->with('success', 'Resort Deleted!');
    }

    public function destroyPhoto($id){
        $resortPhoto = ResortPhoto::find($id);
        $resortId = $resortPhoto->resort_id;
        $resort = Resort::find($resortId);
        $path = '/images/'.$this->state->name.'/resorts/'.$resort->name.'/'.$resortPhoto->filename;
        $s3 = \Storage::disk('s3');
        if($s3->exists($path)) {
            $s3->delete($path);
        }

        $resortPhoto->delete();
    }
}
