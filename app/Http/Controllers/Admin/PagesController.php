<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Page;
use App\Models\PagePhoto;

use Image;
use Log;

class PagesController extends Controller
{
    public function __construct(Request $request){
        parent::__construct($request);
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $pages = Page::where('state_id', $this->state->id)->get();
        return view('admin.pages', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $page = new Page;
        return view('admin.page-form', compact('page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required|alpha_dash',
            'content' => 'required',
        ]);

        $page = Page::create($request->except('thumb_image'));
        $page->featured = $request->featured ? 1 : 0;
        $imageNames = $this->uploadFileToS3($request);
        $photosToInsert = array();
        $photoOrder = 1;
        foreach($imageNames as $key => $imageName){
            $photoLocation =  env('S3_IMAGE_PATH') . $this->state->name . '/pages/' .$request->name . '/' . $imageName;
            array_push($photosToInsert, ['page_id' => $id, 'image_path' => $photoLocation, 'filename'=> $imageName, 'description' => $request->newImageDescription[$key], 'photo_order' => $photoOrder]);
            $photoOrder++;
        }
        PagePhoto::insert($photosToInsert);
        $page->save();
        return redirect('admin/pages')->with('success', 'Page Saved Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $page = Page::find($id);
        return view('admin.page-form', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required|alpha_dash',
            'content' => 'required',
        ]);
        $page = Page::find($id);
        $page->title = $request->input('title');
        $page->slug = $request->input('slug');
        $page->header_text = $request->input('header_text');
        $page->content = $request->input('content');
        $page->seo_title = $request->input('seo_title');
        $page->seo_description = $request->input('seo_description');
        $page->featured = $request->input('featured') ? 1 : 0;

        //Featured photo upload
        $featImage = $request->file('thumb_image');
        if(!is_null($featImage))
        {
            if($page->thumb_image)
            {
                $path = str_replace(env('S3_IMAGE_PATH'), '', $page->thumb_image);
                $path = 'images/'.$path;
                $s3 = \Storage::disk('s3');
                if($s3->exists($path)) {
                    $s3->delete($path);
                }
            }

            $featName = str_replace(' ', '', $featImage->getClientOriginalName());
            $featImageFilename = time() . '.' . $featName;
            $s3Ref = \Storage::disk('s3');
            $featFilepath = '/images/'.$this->state->name.'/pages/'.$page->title.'/'.$featImageFilename;
            $s3Ref->put($featFilepath, fopen($featImage, 'r+'), 'public');
            $page->thumb_image = env('S3_IMAGE_PATH') . $this->state->name.'/pages/'.$page->title.'/'.$featImageFilename;
        }

        $page->save();

        return redirect('admin/pages')->with('success', 'Page Saved Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $page = Page::find($id);
        $page->delete();
        return redirect('admin/pages')->with('success', 'Page Deleted!');
    }

    public function uploadFileToS3(Request $request){
        $images = $request->file('thumb_image');
        $filenames = array();
        foreach($images as $image){
            if(!is_null($image)) {
                $name = str_replace(' ', '', $image->getClientOriginalName());
                $imageFilename = time() . '.' . $name;
                array_push($filenames, $imageFilename);
                $s3 = \Storage::disk('s3');
                $filepath = '/images/'.$this->state->name.'/'.$request->name.'/'.$imageFilename;
                $s3->put($filepath, fopen($image, 'r+'), 'public');
            }
        }

        return $filenames;
    }
}
