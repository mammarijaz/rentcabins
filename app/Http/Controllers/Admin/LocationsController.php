<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Location;
use App\Models\Region;

class LocationsController extends Controller
{
    public function __construct(Request $request){
        parent::__construct($request);
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $locations = Location::where('state_id', $this->state->id)->get();
        return view('admin.locations', compact('locations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $location = new Location;
        $regions = Region::where('state_id', $this->state->id)->lists('name', 'id');
        return view('admin.location-form', compact('regions', 'location'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required',
            'slug' => 'alpha_dash',
            'state_id' => 'required',
            'region_id' => 'required',
        ]);
        Location::create($request->all());

        return redirect('admin/locations')->with('success', 'Location Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $location = Location::findOrFail($id);
        $regions = Region::where('state_id', $this->state->id)->lists('name', 'id');
        return view('admin.location-form', compact('location', 'regions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $this->validate($request, [
            'name' => 'required',
            'slug' => 'required|alpha_dash',
            'state_id' => 'required',
            'region_id' => 'required',
        ]);

        $location = Location::FindOrFail($id);
        $location->fill($request->all());
        $location->save();
        return redirect('admin/locations')->with('success', 'Location saved!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $location = Location::find($id);
        $location->delete();
        return redirect('admin/locations')->with('success', 'Location Deleted!');
    }
}
