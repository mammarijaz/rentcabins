<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use App\Models\Property;
use App\Models\User;
use App\Models\Resort;
use App\Models\Location;
use App\Models\Region;
use App\Models\Amenity;
use App\Models\AmenityCategory;
use App\Models\PropertyPhoto;
use App\Models\PropertySettings;
use App\Models\PropertyStyles;
use App\Models\PropertyDate;
use App\Models\State;
use Auth;
use Hashids;
use Validator;
use Image;
use Carbon\Carbon;

class PropertiesController extends Controller
{
    protected $exceptFields = ['regions', 'amenities', 'settings', 'styles', 'blocked_dates', 'featured_photo'];
    protected $validationRules = [
                'on_waters' => 'max:45',
                'weekly_only' => 'max:100',
                'user_id' => 'required'
    ];
    // protected $validationRules = [
    //         'status' => 'required',
    //         'user_id' => 'required',
    //         'name' => 'required|max:40',
    //         'description' => 'required|max:1200',
    //         'owner_bios' => '',
    //         'email' => 'required|max:150',
    //         'phone' => 'required',
    //         'phone_cell' => '',
    //         'phone_cell_carrier' => '',
    //         'address' => 'required|max:150',
    //         'city' => 'required|max:50',
    //         'zip' => 'required|max:5',
    //         'state_id' => '',
    //         'photos' => 'max:15',
    //         'listing_url' => 'active_url',
    //         // 'blocked_dates' => '',
    //         'location_id' => 'required',
    //         'regions' => 'required',
    //         'on_waters' => 'max:45',
    //         // 'settings' => '',
    //         'allow_pets' => '',
    //         'allow_accessible' => '',
    //         'accepts_credit_cards' => '',
    //         'max_rates_daily' => "required|regex:/^[0-9][\.,]?[0-9]*$/",
    //         'max_rates_weekly' => "required|regex:/^[0-9][\.,]?[0-9]*$/",
    //         'rates' => '',
    //         'max_capacity' => 'required|integer',
    //         'bedrooms' => 'required|integer',
    //         'bathrooms' => 'required|integer',
    //         // 'keywords' => 'required',
    //         'min_rates_daily' => 'required',
    //         'min_rates_weekly' => 'required',
    //         'address_longitude' => 'required|max:150',
    //         'address_latitude' => 'required|max:150',
    //         'resort' => 'max:150',
    //         'unique_features' => 'max:800',
    //         'things_to_do' => 'max:1000',
    //         'social_media_links' => '',
    //         'special_offers' => '',
    //         'amenities' => '',
    //         'testimonials' => 'max:1200',
    //         'videos' => '',
    //         'weekly_only' => 'max:100',
    //     ];
    protected $messages = [
        'location_id.required' => 'The near location dropdown is required.',
        'user_id.required' => 'The owner field is required.'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if(Auth::user()->isAdmin()){
            $properties = Property::where('state_id', $this->state->id)->get();
            if($request->input('user_id')){
                $properties = Property::where('user_id', $request->input('user_id'))->get();
            }
            $owners = User::where('state_id', $this->state->id)->get();
            return view('admin.properties', compact('properties', 'owners'));
        }
        $owners = User::find(Auth::user()->id)->get();
        $properties = Property::where('user_id', Auth::user()->id)->get();
        return view('admin.properties', compact('properties', 'owners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $isSuperAdmin = Auth::user()->isSuperAdmin();
        $state = State::find($this->state->id);
        $locations = Location::where('state_id', $this->state->id)->lists('name', 'id')->prepend('--', '')->toArray();
        $regions = Region::where('state_id', $this->state->id)->get();
        $categories = AmenityCategory::all();
        foreach($categories as $category){
            $filtered = $category->amenities->filter(function($value, $key){
                return $value->states()->where('id', $this->state->id)->exists();   
            });
            $category->amenities = $filtered;
        }
        $propertySettings = PropertySettings::all();
        $propertyStyles = PropertyStyles::all();
        $resorts = Resort::pluck('name','id')->all();
        $resorts = [null => 'Please Select'] + $resorts;
        $owners = User::where('role_id', 3)->where('state_id', $this->state->id)->lists('name', 'id');
        $property = new Property;
        $userRegions = [];

        return view('admin.property-form', compact('owners', 'resorts', 'locations', 'property', 'regions', 'userRegions', 'categories', 'amenities',
            'propertySettings', 'propertyStyles', 'isSuperAdmin'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, $this->validationRules, $this->messages);

        $regions = [];
        if(!empty($request->input('regions'))){
            $regions = $request->input('regions');
        }
        $amenities = [];
        if(!empty($request->input('amenities'))){
             $amenities = $request->input('amenities');
        }
        $settings = [];
        if(!empty($request->input('settings'))){
             $settings = $request->input('settings');
        }
        $styles = [];
        if(!empty($request->input('styles'))){
             $styles = $request->input('styles');
        }

        //Featured photo upload
        $featImage = $request->file('featured_photo');
        if(!is_null($featImage))
        {
            $featName = str_replace(' ', '', $featImage->getClientOriginalName());
            $featImageFilename = time() . '.' . $featName;
            $s3Ref = \Storage::disk('s3');
            $featFilepath = '/images/'.$this->state->name.'/'.$property->name.'/featured_photos/'.$featImageFilename;
            $s3Ref->put($featFilepath, fopen($featImage, 'r+'), 'public');
            $property->featured_photo = env('S3_IMAGE_PATH') . $this->state->name.'/'.$property->name.'/featured_photos/'. $featImageFilename;
        }
        if($request->input('location_id') == ''){
            $request['location_id'] = null;
        }

        if($request->input('resort_id') == ''){
            $request['resort_id'] = null;
        }


        $property = Property::create($request->except($this->exceptFields));

        //Need to create a Location entry if the city in the address isn't something we already track.
        //TODO: avoid the 'address_location_id' with a pivot table synced like regions.
        if($request->input('city') != ''){
           $cityName = $request->input('city');

            $addressLocation = Location::where('name', ucwords(strtolower($cityName)))->first();
            if($addressLocation == null){
                $newLocation = Location::create(['name' => ucwords(strtolower($cityName)), 
                                                'region_id' => reset($regions), 
                                                'slug'=> strtolower(str_replace(' ', '-', $cityName).'-'.$this->state->abbr), 
                                                'state_id' => $this->state->id]);
                $addressLocation = $newLocation;
            }
            $property->address_location_id = $addressLocation; 
        }


        $property->regions()->sync($regions);
        $property->amenities()->sync($amenities);
        $property->settings()->sync($settings);
        $property->styles()->sync($styles);

        //handle uploaded photos
        $imageNames = $this->uploadFileToS3($request);
        $photosToInsert = array();
        $photoOrder = 1;
        foreach($imageNames as $key => $imageName){
            $photoLocation =  env('S3_IMAGE_PATH') . $imageName;
            array_push($photosToInsert, ['property_id' => $property->id, 'image_path' => $photoLocation, 'filename'=> $imageName, 'description' => $request->newImageDescription[$key], 'photo_order' => $photoOrder]);
            $photoOrder++;
        }
        
        PropertyPhoto::insert($photosToInsert);

        if(!empty($request->input('blocked_dates'))) {
            $blocked_dates = json_decode($request->input('blocked_dates'));
            $this->updateDates($property, $blocked_dates);
        }
        $public_id_string = $property->resort_id !== NULL ? urlencode($property->resort->name) . '-' : '' ;
        $property->public_id = strtolower(str_replace('+', '-', $public_id_string . urlencode(str_replace('\'', '', $property->name)) . '-' . Hashids::encode($property->id)));
        if(!empty($uploadedResult)){
            $property->photos = $uploadedResult;
        }
        $property->save();
        return redirect('admin/properties')->with('success', 'Property Saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        echo "woops";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $isSuperAdmin = Auth::user()->isSuperAdmin();
        if(Auth::user()->isAdmin()){
            $property = Property::findOrFail($id);
        } else {
            $property = Property::where('user_id', Auth::user()->id)->findOrFail($id);
        }

        $state = State::find($this->state->id);
        $locations = Location::where('state_id', $this->state->id)->lists('name', 'id');
        $regions = Region::where('state_id', $this->state->id)->get();
        $propertySettings = PropertySettings::all();
        $propertyStyles = PropertyStyles::all();
        $categories = AmenityCategory::all();
        foreach($categories as $category){
            $filtered = $category->amenities->filter(function($value, $key){
                return $value->states()->where('id', $this->state->id)->exists();   
            });
            $category->amenities = $filtered;
        }
        $userRegions = $property->regions()->lists('region_id')->toArray();
        $resorts = Resort::pluck('name','id')->all();
        $resorts = [null => 'Please Select'] + $resorts;
        $photos = PropertyPhoto::where('property_id',$id)->orderBy('photo_order')->get();
        $owners = User::where('role_id', 3)->where('state_id', $this->state->id)->lists('name', 'id');
        $property->save();

        return view('admin.property-form', compact('owners', 'resorts', 'locations', 'property', 'regions', 'photos', 'userRegions','categories', 'amenities',
            'propertySettings', 'propertyStyles', 'isSuperAdmin'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
        switch($request->submitbutton) {
            case 'Save':
                $this->validate($request, $this->validationRules, $this->messages);

                $property = Property::findOrfail($id);
                $regions = [];
                if(!empty($request->input('regions'))){
                    $regions = $request->input('regions');
                }
                $amenities = [];
                if(!empty($request->input('amenities'))){
                    $amenities = $request->input('amenities');
                }
                $settings = [];
                if(!empty($request->input('settings'))){
                    $settings = $request->input('settings');
                }
                $styles = [];
                if(!empty($request->input('styles'))){
                    $styles = $request->input('styles');
                }

                //Need to create a Location entry if the city in the address isn't something we already track.
                //TODO: avoid the 'address_location_id' with a pivot table synced like regions.
                if($request->input('city') != ''){
                    $cityName = $request->input('city');

                    $addressLocation = Location::where('name', ucwords(strtolower($cityName)))->first();
                    if($addressLocation == null){
                        $newLocation = Location::create(['name' => ucwords(strtolower($cityName)), 
                                                        'region_id' => reset($regions), 
                                                        'slug'=> strtolower(str_replace(' ', '-', $cityName).'-'.$this->state->abbr), 
                                                        'state_id' => $this->state->id]);
                        $addressLocation = $newLocation;
                    } 
                    $property->address_location_id = $addressLocation->id;
                } 
                //Form returns empty when it's an owner, so default to existing city if it's empty.
                else {
                    array_push($this->exceptFields, 'city', 'zip', 'state_id');
                }

                //Featured photo upload
                $featImage = $request->file('featured_photo');
                if(!is_null($featImage))
                {
                    if($property->featured_photo)
                    {
                        $path = str_replace(env('S3_IMAGE_PATH'), '', $property->featured_photo);
                        $path = 'images/'.$path;
                        $s3 = \Storage::disk('s3');
                        if($s3->exists($path)) {
                            $s3->delete($path);
                        }
                    }

                    $featName = str_replace(' ', '', $featImage->getClientOriginalName());
                    $featImageFilename = time() . '.' . $featName;
                    $s3Ref = \Storage::disk('s3');
                    $featFilepath = '/images/'.$this->state->name.'/'.$property->name.'/featured_photos/'.$featImageFilename;
                    $s3Ref->put($featFilepath, fopen($featImage, 'r+'), 'public');
                    $property->featured_photo = env('S3_IMAGE_PATH') . $this->state->name.'/'.$property->name.'/featured_photos/'. $featImageFilename;
                }


                //handle uploaded photos
                $imageNames = $this->uploadFileToS3($request);
                $photosToInsert = array();
                $photoOrder = PropertyPhoto::where('property_id', $id)->max('photo_order');
                $photoOrder++;
                if(count($imageNames) > 0) {
                    foreach($imageNames as $key => $imageName){
                        $photoLocation =  env('S3_IMAGE_PATH') . $this->state->name.'/'.$property->name.'/'.$imageName;
                        array_push($photosToInsert, ['property_id' => $id, 'image_path' => $photoLocation, 'filename'=> $imageName, 'description' => $request->newImageDescription[$key], 'photo_order' => $photoOrder]);
                        $photoOrder++;
                    }
                }
                
                if(count($request->existingDescription) > 0){
                    foreach($request->existingDescription as $key => $description){
                        $photo = PropertyPhoto::FindOrFail($key);
                        $photo->description = $description;
                        $photo->save();
                    }
                }
                        
                if(count($request->order) > 0){
                    foreach($request->order as $key => $newOrder){
                        $photo = PropertyPhoto::FindOrFail($key);
                        $photo->photo_order = $newOrder;
                        $photo->save();
                    }
                }
                PropertyPhoto::insert($photosToInsert);

                if($request->input('location_id') == ''){
                    $request['location_id'] = null;
                }

                if($request->input('resort_id') === ''){
                    $request['resort_id'] = null;
                }

                $property->fill($request->except($this->exceptFields));
                $property->regions()->sync($regions);
                $property->amenities()->sync($amenities);
                $property->settings()->sync($settings);
                $property->styles()->sync($styles);

                if(!empty($request->input('blocked_dates'))) {
                    $blocked_dates = json_decode($request->input('blocked_dates'));
                    $this->updateDates($property, $blocked_dates);
                }

                if(empty($property->public_id)){
                    $property->public_id = Hashids::encode($property->id);
                }
                $property->save();
                return redirect('admin/properties')->with('success', 'Property Saved');
            default:
                if(isset($request->submitbutton)) {
                    $deletedPhotoOrder = PropertyPhoto::FindOrFail($request->submitbutton)->photo_order;
                    $property = Property::findOrfail($id);
                    $this->destroyPhoto($property->name, $request->submitbutton);
                    $updateOrders = PropertyPhoto::where([
                        ['property_id', '=', $id],
                        ['photo_order', '>', $deletedPhotoOrder]
                        ])->get();
                    if(count($updateOrders) > 0){
                        foreach ($updateOrders as $photoToUpdate) {
                            $photoToUpdate->photo_order -= 1;
                            $photoToUpdate->save();
                        }
                    }
                    return redirect('admin/properties/'.$id.'/edit')->with('success', 'Photo Deleted!');
                }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $property = Property::find($id);
        $property['location_id'] = NULL;
        $property['address_location_id'] = NULL;
        $property->save();
        $property->delete();
        return redirect('admin/properties')->with('success', 'Property Deleted!');
    }

    public function uploadFileToS3(Request $request){
        $images = $request->file('image');
        $filenames = array();
        foreach($images as $image){
            if(!is_null($image)) {
                $name = str_replace(' ', '', $image->getClientOriginalName());
                $imageFilename = time() . '.' . $name;
                array_push($filenames, $imageFilename);
                $s3 = \Storage::disk('s3');
                $filepath = '/images/'.$this->state->name.'/'.$request->name.'/'.$imageFilename;
                $s3->put($filepath, fopen($image, 'r+'), 'public');
            }
        }

        return $filenames;
    }

    public function destroyPhoto($propertyName, $id){
        $propertyPhoto = PropertyPhoto::find($id);
        $propertyId = $propertyPhoto->property_id;
        $path = '/images/'.$this->state->name.'/'.$propertyName.'/'.$propertyPhoto->filename;
        $s3 = \Storage::disk('s3');
        if($s3->exists($path)) {
            $s3->delete($path);
        }

        $propertyPhoto->delete();
    }

    protected function updateDates(Property $property, $dates = []){
        $dates = array_unique($dates);
        if($dates != $property->blocked_dates->lists('blocked_date')->toArray()){
            $new_dates = [];
            $yesterday = Carbon::yesterday(new \DateTimeZone('America/Chicago'));
            foreach($dates as $blocked_date){
                $newDate = Carbon::createFromFormat('Y-m-d', $blocked_date);
                if($newDate > $yesterday){
                    $new_dates[] = new PropertyDate(['blocked_date' => $newDate]);
                } else {

                }
            }
            $property->blocked_dates()->delete();
            $property->blocked_dates()->saveMany($new_dates);
        }
    }

    //Deprecated!
    protected function uploadPhotos($fileArray, $photoDescArray, $currentPhotos = []){
        if(empty($fileArray)) {
            return false;
        }
        $photosArray = [];
        $destinationPath = public_path().'/uploads/';
        for($i=1; $i <= count($fileArray); $i++) {
            if(!empty($fileArray[$i])) {
                $filename = uniqid().'.'.$fileArray[$i]->guessExtension();
                $uploadSuccess = $fileArray[$i]->move($destinationPath, $filename);
                if($uploadSuccess){
                    $image = Image::make($destinationPath.$filename);
                    $image->resize(800, 600);
                    $image->save($destinationPath.$filename);
                    $photosArray[$i] = [
                        'name' => $filename,
                        'description' => $photoDescArray[$i],
                    ];
                }
            } else {
                if(!empty($currentPhotos[$i])) {
                    $photosArray[$i] = $currentPhotos[$i];
                }
            }
        }

        return count($photosArray) > 0 ? $photosArray : false;
    }
}
