<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\PropertySettings;

class PropertySettingsController extends Controller
{
    //
    public function __construct(Request $request){
        parent::__construct($request);
        $this->middleware('admin');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'setting' => 'required'
        ]);

        PropertySettings::create($request->all());

        return redirect('admin/options')->with('success', 'Property Setting Created');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $setting = PropertySettings::find($id);
        $setting->delete();
        return redirect('admin/options')->with('success', 'Property Setting Deleted');
    }
}
