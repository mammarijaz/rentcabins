<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\PropertyStyles;

class PropertyStylesController extends Controller
{
    //
    public function __construct(Request $request){
        parent::__construct($request);
        $this->middleware('admin');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'style' => 'required'
        ]);

        PropertyStyles::create($request->all());

        return redirect('admin/options')->with('success', 'Property Style Created');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $style = PropertyStyles::find($id);
        $style->delete();
        return redirect('admin/options')->with('success', 'Property Style Deleted');
    }
}
