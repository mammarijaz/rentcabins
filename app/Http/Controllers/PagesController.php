<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Page;
use App\Models\Location;
use App\Models\Amenity;
use App\Models\Region;
use App\Models\Settings;
use App\Models\Property;
use App\Models\PropertySettings;
use App\Models\PropertyStyles;
use App\Models\State;
use Mail;
use Config;

class PagesController extends Controller
{
    public function getIndex(){
        $templateClass = 'search';
        $state = State::find($this->state->id);
        $locations = Location::where('state_id', $this->state->id)->lists('name', 'id');

        //Hack to get search form data for Joe that returns user entered cities as well as established location data.
        $filterArray = array();
        foreach($locations as $id => $location) {
            array_push($filterArray, $location);
        }

        $amenities = $state->amenities()->where('is_searchable', 1)->lists('title', 'id');
        $propertyStyles = $state->propertyStyles()->where('is_searchable', 1)->lists('style', 'id');
        $propertySettings = $state->propertySettings()->where('is_searchable', 1)->lists('setting', 'id');
        $regions = Region::where('state_id', $this->state->id)->get();
        $settings = Settings::where('state_id', $this->state->id)->firstOrFail();
        $featuredProperty = Property::where('state_id', $this->state->id)->where('featured', '1')->orderByRaw("RAND()")->first();
        $featuredPages = Page::where('state_id', $this->state->id)->where('featured', '1')->orderByRaw("RAND()")->limit(4)->get();
    	return view('home', compact('templateClass', 'settings', 'regions', 'locations', 'filterArray', 'ownerLocations', 'amenities',
            'featuredProperty', 'featuredPages', 'propertyStyles', 'propertySettings'));
    }

    public function getPage($slug){
        $page = Page::where('state_id', $this->state->id)->where('slug', $slug)->firstOrFail();
        $featuredProperty = Property::where('state_id', $this->state->id)->where('featured', '1')->orderByRaw("RAND()")->first();
        $settings = Settings::where('state_id', $this->state->id)->first();
        return view('full-width', compact('page', 'settings', 'featuredProperty'));
    }

    public function getThanks(){
        $featuredProperty = Property::where('state_id', $this->state->id)->where('featured', '1')->orderByRaw("RAND()")->first();
        $settings = Settings::where('state_id', $this->state->id)->first();
        return view('thanks', compact('settings', 'featuredProperty'));
    }

    public function getRentalThanks(){
        $featuredProperty = Property::where('state_id', $this->state->id)->where('featured', '1')->orderByRaw("RAND()")->first();
        $settings = Settings::where('state_id', $this->state->id)->first();
        return view('rental-thanks', compact('settings', 'featuredProperty'));
    }

    public function getListInquiry(){
        $featuredProperty = Property::where('state_id', $this->state->id)->where('featured', '1')->orderByRaw("RAND()")->first();
        $settings = Settings::where('state_id', $this->state->id)->first();
        return view('list-rentals', compact('settings', 'featuredProperty'));
    }

    public function postRentalInquiry(Request $request){
        $emailDomain = str_replace("www.", "", "info@".$this->state->domain);
        $this->validate($request, [
            'email' => 'required',
            'name' => 'required',
        ]);

        $name = $request->input('name');
        if($request->has('Lname')){
            $name .= ' '.$request->input('Lname');
        }

        $settings = Settings::where('state_id', $this->state->id)->first();
        $message_info = "
            <hr />
            <p><strong>Name </strong><br />".$request->input('name')."</p>
            <p><strong>State </strong><br />".$request->input('state')."</p>
            <p><strong>Phone </strong><br />".$request->input('phone')."</p>
            <p><strong>Email </strong><br />".$request->input('email')."</p>
            <p><strong>Mobile Info </strong><br />".$request->input('mobile')."</p>
            <hr />
            <p><strong>Owner Website </strong><br />".$request->input('website')."</p>
            <p><strong>Number of Rentals </strong><br />".$request->input('numRentals')."</p>
            <p><strong>AVROA Member </strong><br />".($request->input('avroa') == "yes" ? "Yes" : "No")."</p>
            <hr />
            <p><strong>Personalization</strong><br />".$request->input('personalization')."</p>
            <hr />";

        // Send to Owner
        $to = $settings->from_email;
        $from = $this->state->seo_name.' <'.$emailDomain.'>';
        $subject = $this->state->seo_name.': New Listing Inquiry!';
        $joe_message = $message_info;

        $data = [
            'heading' => "New Inquiry!",
            'message_body' => $joe_message,
        ];

        Mail::send('emails.inquiry-email', $data, function($m) use ($to, $settings, $subject, $emailDomain){
            $m->to($settings->from_email);
            $m->from($emailDomain);
            $m->subject($subject);
        });
        return redirect('rental-thanks');
    }
}
