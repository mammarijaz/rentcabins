<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class SitemapController extends Controller
{
    public function index()
	{
		$routes = [
            ['route' => '', 'priority'=> 1.00],
            ['route' => 'list-your-rental', 'priority'=> 0.80],
            ['route' => 'inquiry', 'priority'=> 0.7],
            ['route' => 'rental-inquiry', 'priority'=> 0.7],
            ['route' => 'login', 'priority'=> 0.5],
        ];
		return response()->view('sitemap.index', [
			'routes' => $routes
		])->header('Content-Type', 'text/xml');
	}
}