<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Property;
use App\Models\Page;
use App\Models\Settings;
use App\Models\State;
use App\Helpers\Calendar;
use App\Models\Lead;
use App\Models\AmenityCategory;

use Mail;
use Config;

class PropertyController extends Controller
{
    //

    public function getProperty($publicId){
    	$page = new Page;
    	$page->title = 'Property Details';
    	$property = Property::where('state_id', $this->state->id)->where('public_id', $publicId)->firstOrFail();
        $amenityCategoriesToFind = $property->amenities->unique('amenity_category_id');
        $idArray = array();
        foreach($amenityCategoriesToFind as $amenity){
            array_push($idArray, $amenity->amenity_category_id);
        }
        $amenityCategories = AmenityCategory::whereIn('id', $idArray)->orderBy('category_order')->get();
    	$settings = Settings::where('state_id', $this->state->id)->first();
        $state = State::where('id', $this->state->id)->first();

        $cal = new Calendar(date('Y'));
        $isOdd = true;
        $calHtml = '';
        for($month=0;$month<12;$month++) {
            $calHtml .='<div class="col-sm-6">';
            $monthfuture = mktime(date('h'),date('i'),date('s'),date('m')+$month,01,date('Y'));
            $monthname = date('F', $monthfuture);
            if(date('m') > date('m', $monthfuture)) {
             $calHtml .='<h3>'.$monthname.' '.(date('Y')+1).'</h3>';
            } else {
             $calHtml .='<h3>'.$monthname.' '.(date('Y')).'</h3>';
            }

            $calHtml .= $cal->getMonthHTML(date('m', $monthfuture),'calendar', $property->blocked_dates()->lists('blocked_date')->toArray()).'</div>';

            if(!$isOdd) $calHtml .='<div class="clearfix hidden-xs-block"></div>';
            $isOdd = ! $isOdd;
        }
    	return view('property-details', compact('property', 'page', 'settings', 'calHtml', 'amenityCategories'));
    }

    public function postInquiry(Request $request){
        $emailDomain = str_replace("www.", "", "info@".$this->state->domain);
        $this->validate($request, [
            'email' => 'required',
            'name' => 'required',
        ]);

        $name = $request->input('name');
        if($request->has('Lname')){
            $name .= ' '.$request->input('Lname');
        }
        $selectedProperties = explode(',', $request->input('selected_properties'));
        foreach($selectedProperties as $propertyID)
        $lead = Lead::create([
            'name' => $name,
            'property_id' => $propertyID,
            'email' => $request->input('email'),
            'state' => ($request->has('state') ? $request->input('state') : ''),
            'phone' => ($request->has('phone') ? $request->input('phone') : ''),
            'email_optin' => ($request->has('email_optin') ? $request->input('email_optin') : ''),
            'personalization' => ($request->has('personalization') ? $request->input('personalization') : ''),
            'search_info' => json_encode($request->except(['name', 'Lname', 'state', 'phone', 'email_optin', 'personalization'])),
        ]);

        $lead->properties()->sync($selectedProperties);
        $lead->save();

        $settings = Settings::where('state_id', $this->state->id)->first();
        $message_to_searcher = "Thank you for searching on ".$this->state->seo_name;
        $message_to_owner = "CLICK your email Reply button and follow up with this lead from ".$this->state->seo_name.": ";
        if(! $request->has('is_search')){
            $message_search_info = "
                <hr />
                <p><strong>Name </strong><br />".$request->input('name')."</p>
                <p><strong>State </strong><br />".$request->input('state')."</p>
                <p><strong>Phone </strong><br />".$request->input('phone')."</p>
                <p><strong>Email </strong><br />".$request->input('email')."</p>
                <hr />
                <p><strong>Rental</strong> from ".$request->input('arrival_date')." to ".$request->input('departure_date')."</p>
                <p><strong>Number of Adults</strong><br />".$request->input('adults')."</p>
                <p><strong>Number of Children</strong><br />".$request->input('children')."</p>
                <hr />
                <p><strong>Personalization</strong><br />".$request->input('personalization')."</p>
                <hr />
                <h3><em><b>RENTERS, WANT A FREE $100 GAS CARD? </b></em></br></h3><p><em><b>Keep this email and Reply to
                    it IF you end up booking this rental. You&rsquo;ll automatically be eligible TO WIN A FREE $100
                    GAS CARD.</b> (4) chances to win every year: Drawings are April 1, July 1, October 1, December 31.
                    You&rsquo;ll be eligible for the 4 drawings that take place after your check in date. This is a ".
                    $this->state->seo_name." promotion.</em></p>";
        } else {
            $message_search_info = "
                <hr />
                <p><strong>Name </strong><br />".$request->input('name')."</p>
                <p><strong>State </strong><br />".$request->input('state')."</p>
                <p><strong>Phone </strong><br />".$request->input('phone')."</p>
                <p><strong>Email </strong><br />".$request->input('email')."</p>
                <hr />
                <p><strong>Rental</strong> from ".$request->input('arrival_date')." to ".$request->input('departure_date')."</p>
                <p><strong>Destination</strong><br />".$request->input('locations')."</p>
                <p><strong>Regions</strong><br />".$request->input('regions')."</p>
                <p><strong>Settings</strong><br />".$request->input('settings')."</p>
                <p><strong>Styles</strong><br />".$request->input('styles')."</p>
                <p><strong>Daily Budget</strong><br />".$request->input('max_rates_daily')."</p>
                <p><strong>Weekly Budget</strong><br />".$request->input('max_rates_weekly')."</p>
                <p><strong>Bedrooms</strong><br />".$request->input('bedrooms')."</p>
                <p><strong>Number of People</strong><br />".$request->input('max_capacity')."</p>
                <p><strong>Number of Adults</strong><br />".$request->input('adults')."</p>
                <p><strong>Number of Children</strong><br />".$request->input('children')."</p>
                <p><strong>Amenities</strong><br />".$request->input('features')."</p>
                <hr />
                <p><strong>Personalization</strong><br />".$request->input('personalization')."</p>
                <hr />
                <h3><em><b>RENTERS, WANT A FREE $100 GAS CARD? </b></em></br></h3><p><em><b>Keep this email and Reply to
                it IF you end up booking this rental. You&rsquo;ll automatically be eligible TO WIN A FREE $100 GAS
                CARD.</b> (4) chances to win every year: Drawings are April 1, July 1, October 1, December 31.
                You&rsquo;ll be eligible for the 4 drawings that take place after your check in date. This is a ".
                $this->state->seo_name." promotion.</em></p>";
        }

        $message_property_searcher = "";

        foreach($selectedProperties as $id) {
            $property = Property::find($id);

            $resortName = "Placeholder";
            if($property->resort){
                $resortName = $property->resort->name;
            } else {
                $resortName = "";
            }

            $message_property_owner = "<p><a href='http://".$this->state->domain."/rental/".$property->public_id."'>".
                $resortName." ".$property->name."</a> in ".$property->city." - Phone: ".$property->phone."</p>";

            $message_property_searcher .= $message_property_owner;

            // Send to Owner
            $to = ( !empty( $property->email ) ? $property->email : $property->user->email );
            $from = $this->state->seo_name.' <'.$emailDomain.'>';
            $subject = $this->state->seo_name.': ' . $property->name . ' Rental Lead';
            $owner_message = $message_property_owner.$message_search_info;

            $data = [
                'heading' => $message_to_owner,
                'message_body' => $owner_message,
            ];

            Mail::send('emails.inquiry-email', $data, function($m) use ($to, $settings, $subject, $emailDomain){
                $m->to($to);
                $m->from($emailDomain);
                $m->cc($settings->from_email);
                $m->subject($subject);
            });
            // TXT Alert
            // if(trim($property['phone_cell_carrier']) !== '' && trim($property['phone_cell']) !== '') {
            //     $transport = Swift_SendmailTransport::newInstance('/usr/sbin/sendmail -bs');
            //     $mailer = Swift_Mailer::newInstance($transport);
            //     $message = Swift_Message::newInstance()
            //         ->setSubject('$this->state->seo_name: '.$property['name'].' Rental Lead')
            //         ->setFrom(array('updates@$this->state->seo_name' => '$this->state->seo_name Updates'))
            //         ->setTo(array(str_replace(array('-','.','x',' ','_','#'),'',$property['phone_cell']).$property['phone_cell_carrier']))
            //         ->addPart('Check your email for a new Rental Inquiry from $this->state->seo_name!', 'text/plain')
            //     ;
            //     $mailer->send($message);
            // }

        }

        // Send to Searcher

        $to = $request->input('email');
        $from = $emailDomain;
        $subject = $this->state->seo_name.': A copy of your inquiry. $100 gas card promotion.';
        $message = $message_property_searcher.$message_search_info;
        $data = array(
            'heading' => $message_to_searcher,
            'message_body' => $message,
        );
        Mail::send('emails.inquiry-email', $data, function($m) use ($to, $settings, $subject){
            $m->to($to);
            $m->from($settings->from_email);
            $m->subject($subject);
        });
        return redirect('thanks');
    }
}
