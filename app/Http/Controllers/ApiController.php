<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Property;
use App\Models\Location;
use App\Models\PropertyDate;
use App\Models\PropertyPhoto;
use App\Models\Resort;
use App\Models\AmenityCategory;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use DB;
use App\Models\Lead;

class ApiController extends Controller
{
    public function getPropertyPhotos(Property $property) {
        return PropertyPhoto::where('property_id', $property->id)->get();
    }

    //
    public function getAllProperties(Request $request){
    	if(! $request->ajax()){
    		return false;
    	}

        if($request->has('resort_id')){
            $properties = Property::where('resort_id', $request->input('resort_id'))
            ->where('state_id', $this->state->id)
            ->with('amenities')->with('location')->with('resort')->get();;
            foreach($properties as $property){
                //Not a fan of this but it gets results :(
                $petPolicy = "NO Pets Allowed";
                foreach($property->amenities as $amenity){
                    if($amenity->title == "Pets Considered"){
                        $petPolicy = "Pets Considered";
                        break;
                    }
                }
                $property->{"pet_policy"} = $petPolicy;
            }
        } else {
            $properties = Property::where('state_id', $this->state->id)->with('amenities')->with('location')->with('resort')->get();
        }
        
        foreach($properties as $property) {
            $property->photos = $this->getPropertyPhotos($property);
            $property->description = substr($property->description, 0, 120) . '...';
        }

    	return response()->json($properties);
    }

    public function getProperties(Request $request){
        // DB::enableQueryLog();
    	$properties = (new Property)->newQuery();
    	$properties->where('state_id', $this->state->id);

    	if($request->has('locations')){            
            $properties->whereIn('location_id', $request->input('locations'))
                       ->orWhereIn('address_location_id', $request->input('locations'));
        }

        if($request->has('resort_id')){
            $properties->whereIn('resort_id', $request->input('resort_id'));
        }

        if($request->has('regions')){
            $regions = $request->input('regions');
            $properties->whereHas('regions', function($q) use($regions){
                $q->whereIn('region_id', $regions);
            });
    	}

        if($request->has('arrival_date') && $request->has('departure_date')){
            $ad = new Carbon($request->input('arrival_date'), 'America/Chicago');
            $dd = new Carbon($request->input('departure_date'), 'America/Chicago');
            $blocked_dates = PropertyDate::whereBetween('blocked_date', [$ad->toDateString(), $dd->toDateString()])
                ->groupBy('property_id')->lists('property_id');
                // ->get();
            // dump($blocked_dates);
            $properties->whereHas('blocked_dates', function($q)use($blocked_dates){
                $q->whereNotIn('property_id', $blocked_dates);
            });
        }

        if($request->has('settings')){
            $settings = $request->input('settings');
            $properties->whereHas('settings', function($q) use($settings){
                $q->whereIn('property_settings_id', $settings);
            });
        }

        if($request->has('styles')){
            $styles = $request->input('styles');
            $properties->whereHas('styles', function($q) use($styles){
                $q->whereIn('property_styles_id', $styles);
            });
        }

        //default values
        if($request->input('max_rates_daily') !== '$0 - $450+'){
            $max_rates_daily = explode(' - ', str_ireplace('$','',$request->input('max_rates_daily')));
            $max_rates_daily[1] = preg_replace('/\D/', '', $max_rates_daily[1]);
            if($max_rates_daily[1] === '450') $max_rates_daily[1] = '10000';
            if($max_rates_daily[0] === '0') {
                $properties->where(function($q) use($max_rates_daily){
                    $q->where(function($q2)use($max_rates_daily){
                        $q2->where('min_rates_daily', '>=', $max_rates_daily[0]);
                        $q2->orWhereNull('min_rates_daily');
                    });
                    $q->where('max_rates_daily', '<=', $max_rates_daily[1]);
                });
            } else {
                $properties->where(function($q) use($max_rates_daily){
                    $q->where('min_rates_daily', '>=', $max_rates_daily[0]);
                    $q->whereNotNull('min_rates_daily');
                    $q->where('max_rates_daily', '<=', $max_rates_daily[1]);
                });
            }
        }

        //default values
        if($request->input('max_rates_weekly') !== '$0 - $3000+'){
            $max_rates_weekly = explode(' - ', str_ireplace('$','',$request->input('max_rates_weekly')));
            $max_rates_weekly[1] = preg_replace('/\D/', '', $max_rates_weekly[1]);
            if($max_rates_weekly[1] === '3000') $max_rates_weekly[1] = '10000';
            // dump($max_rates_weekly);
            if($max_rates_weekly[0] === '0') {
                $properties->where(function($q) use($max_rates_weekly){
                    $q->where(function($q2)use($max_rates_weekly){
                        $q2->where('min_rates_daily', '>=', $max_rates_weekly[0]);
                        $q2->orWhereNull('min_rates_daily');
                    });
                    $q->where('max_rates_weekly', '<=', $max_rates_weekly[1]);
                });
            } else {
                $properties->where(function($q) use($max_rates_weekly){
                    $q->where('min_rates_daily', '>=', $max_rates_weekly[0]);
                    $q->whereNotNull('min_rates_daily');
                    $q->where('max_rates_weekly', '<=', $max_rates_weekly[1]);
                });
            }
        }

        if($request->has('bedrooms')){
            $properties->where('bedrooms', '>=', $request->input('bedrooms'));
        }

        if($request->has('max_capacity')){
            $properties->where('max_capacity', '>=', $request->input('max_capacity'));
        }

        if($request->has('amenities')){
            $amenities = $request->input('amenities');
            $properties->whereHas('amenities', function($q) use($amenities){
                $q->whereIn('amenity_id', $amenities);
            });
        }

        $result = $properties->with('amenities')->with('location')->get();
        // dump($properties->toSql());
        foreach($result as $property) {
            $property->photos = $this->getPropertyPhotos($property);
            $property->description = substr($property->description, 0, 120) . '...';
        }

     //    dump(DB::getQueryLog());
    	// dd($result);
    	return response()->json($result);
    }
}
