<?php
namespace App\Helpers;
/**
 * this is Calendar class
 */

 class Calendar {

     public $year = '';
     public $month = '';
     public $intToDay = array(
		0=>'Sun',
		1=>'Mon',
		2=>'Tue',
		3=>'Wed',
		4=>'Thu' ,
		5=>'Fri' ,
		6=>'Sat'
	);
	public $intToMonth = array(
		1=>'January',
		2=>'February',
		3=>'March',
		4=>'April' ,
		5=>'May' ,
		6=>'June' ,
		7=>'July',
		8=>'August',
		9=>'September',
		10=>'October',
		11=>'November',
		12=>'December'
	);

     function __construct() {
         $this->year = date('Y');
     }

	 public function getMonthHTML($month, $class='calendar', $events=array(), $eventclass='calendar-event', $eventclassAm = 'calendar-event-am', $eventclassPm = 'calendar-event-pm')
	 {
		 $this->month = $month;

		 if(date('m') > $month)
		 {
			 $time = mktime(0,0,0,$month,1,$this->year+1);
			 $year = $this->year + 1;
		 }
		 else
		 {
			 $time = mktime(0,0,0,$month,1,$this->year);
			 $year = $this->year;
		 }
		 $startingDay = date("D",$time);

		 $returnCode = '<table class="table table-condensed '.$class.'"><thead><tr>';
		 for($count = 0; $count < 7; $count++)
		 {
			 $returnCode .= '<th>' . $this->intToDay[$count] . '</th>';
		 }
		 $returnCode .= '</tr></thead><tr>';

		 for ($count = 0; $count < array_search($startingDay,$this->intToDay); $count++)
		 {
			 $returnCode .= '<td>&nbsp;</td>';
		 }

		 for ($counter = 1; $counter <= 31; $counter++,$count++)
		 {
			 if (($count % 7) == 0)
			 {
				 $returnCode .= '</tr><tr>';
			 }

			 $date = $year.'-'.$this->month.'-'.str_pad($counter,2,'0',STR_PAD_LEFT);
			 if(in_array($date, $events))
			 {
				 $returnCode .= '<td class="'.$eventclass.'">' . $counter . ' </td>';
			 }
			 elseif(in_array($date."am", $events))
			 {
				 $returnCode .= '<td class="'.$eventclassAm.'">' . $counter . ' </td>';
			 }
			 elseif(in_array($date."pm", $events))
			 {
				 $returnCode .= '<td class="'.$eventclassPm.'">' . $counter . ' </td>';
			 }
			 elseif($counter <= date('t', $time))
			 {
				 $returnCode .= '<td>' . $counter . '</td>';
			 }
			 else
			 {
				 $returnCode .= '<td>&nbsp;</td>';
			 }
		 }
		 $returnCode .= '</table>';

		 return $returnCode;
	 }

 }
