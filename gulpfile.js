var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
 elixir.config.publicPath = "html";

elixir(function(mix) {
    mix.less(['lesshat.less', 'print.less', 'main.less']);
    mix.less('admin.less');
    mix.scripts(['plugins.js', 'script.js']);
    mix.scripts('admin.js');
    // mix.babel('Datepicker.js');
    mix.version(['css/app.css', 'js/all.js']);
    // mix.browserSync({
    // 	proxy: 'rentwisconsincabins2.local',
    // });
});
